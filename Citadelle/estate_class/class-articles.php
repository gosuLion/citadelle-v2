<?php


    class Articles  {
        
        function displaySearchArticles()
        {
            
    $category = get_the_category();
    $catId = $category[0]->cat_ID;
           query_posts('cat=$catId'); while (have_posts()) : the_post();
            if($i <=2 ) :                
            
             $id = get_the_ID();
             $image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ) , 'full' );
            $content = get_the_content();
            $splitContent = str_split($content,190);
            
              //Get the currency
    $currency = $_SESSION['currency'];
               $numberOfDecimals = 0;
            //Get regular price
    $price = get_post_meta( get_the_ID(), '_regular_price', true);
    
    //Get sale price
    $salePrice = get_post_meta( get_the_ID(), '_sale_price', true);
            ?>
              
            <a href="<?php the_permalink(); ?>">
              <div class="item">
                <img src="<?php echo $image[0]; ?>" class="img-responsive" alt="">
                <div class="ribbon">
                  <p>
                    <?php the_title(); ?>
                  </p>
                  
                </div>
              </div>
            </a>    
              
           
		<?php $i++ ; 
		endif; 
	endwhile; 
        }
    
        function displayCategoryArticles()
        {

    $category = get_the_category();
    $catId = $category[0]->cat_ID;
    
     ?>
          
              
              <!-- similar : START -->
          <div class="container latest">
            <div class="row">
              <div class="col-xs-12 title">
                <h3><a href="#"><b><?php _e('Latest news' , 'real-estate') ; ?></b></a></h3>
                <p>
                 <?php _e('Find the latest news in home design and decorating, house sales and rental, <br class="mobile-hide">
                  and get in touch with the best agents on the market.','real-estate') ?>
                </p>
                <div class="line"></div>
              </div>
            </div>
            <div class="row">
            <?php $i = 0 ; ?>
	<?php query_posts('cat=$catId'); while (have_posts()) : the_post(); 
            if($i <=2 ) : 
             $id = get_the_ID();
            $image = wp_get_attachment_image_src( get_post_thumbnail_id( $id ) , 'full' );
            
            $content = get_the_content();
            $splitContent = str_split($content,190);
            ?>
              <div class="col-xs-12 col-sm-6 col-md-4">
                <img src="<?php echo $image[0]; ?>" class="img-responsive" alt="" style="width:380px; height:250px;">
                <div class="pad">
                  <h4><b><?php the_title(); ?><br>&nbsp;</b></h4>
                  <p class="line">
                 Published on <?php the_date(); ?>
                  </p>
                  <div class="prieview-text" style="height:100px;">
                    <p>
                      <?php 
                        echo $splitContent[0];
            ?>
                      
                    </p>
                  </div>
                </div>
                <a href="<?php the_permalink() ?>" class="btn"><?php _e('READ MORE' , 'real-estate');?></a>
              </div>      
              
           
			<?php endif; 
            $i ++ ; 
            endwhile; ?>

	 </div>
          
          
          <!-- similar : END -->
	</div>
	<?php wp_reset_query(); 
        }
    }