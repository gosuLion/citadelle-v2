<?php

class RealEstate
{
//define Variables
    
      public $UserId;
      public $numberOfProducts;
      public $bathroomName;
      public $bedroomName;
      public $rentName;
      public $saleName;
      public $soldName;

    
    public function __construct()
    {
        global $wpdb;
        global $redux_demo;
			
//Get default variables from Theme options
			
      $this->bathroomName = $redux_demo['product-bathroom']; 
      $this->bedroomName  = $redux_demo['product-bedrooms']; 
      $this->rentName     = $redux_demo['product-rent'];
      $this->saleName     = $redux_demo['product-sale'];
      $this->soldName     = $redux_demo['product-sold'];

        
        
      $this->numberOfProducts = $redux_demo['number-of-products'];
        
          if( get_current_user_id())
     {
        $this->UserId = get_current_user_id();
     }
        else
        {
            $cookie = $_COOKIE;
            $this->UserId = session_id();
           
            
        }
    }
    
//Display the products on the Front Page
    
    function displayPropertyListings()
    {            
        
//Insert the product ID to the database based on user ID.
        
        if(isset($_POST['favorite'])){
        global $wpdb;
           $prodid = $_POST['prodid']; 
        $wpdb->insert($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
        
//Delete the product id from the database
			
        if(isset($_POST['deletefavorite'])){
        global $wpdb;
        $prodid = $_POST['prodid']; 
        $wpdb->delete($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
?>
    <div class="container property-listings">
        <div class="row">
            <div class="col-xs-12 title">
                <h2 class="text-center"><a href="#"><b><?php _e('Property Listings', 'real-estate'); ?></b></a></h2>
                 <div class="line"></div>
            
            </div>
            <?php
			
// Set the arguments for getting the products
       
            $params = array(
                'post_per_page' => '1',
                'post_type' => 'product'
													);
      
// Get the last X added products and form the loop
			
           $loop = new WP_Query($params);
//Define a custom $i variable
			
                $i = 1;
                if($this->numberOfProducts == ""){
                    $this->numberOfProducts = 6;
                }
             while ( $loop->have_posts() && $i <= $this->numberOfProducts ) : $loop->the_post(); 
    							global $product; 
			
// Set the variables that will be used to display the product
    
//Get image URL
     $attachment_ids[0] = get_post_thumbnail_id( $product->id );
     $attachment = wp_get_attachment_image_src($attachment_ids[0], 'full' );
    
//Get and split the short description to 190 characters	
    $shortDescription = str_split(get_the_excerpt(),190);
    
//Get the currency	
    $currency = $_SESSION['currency'];
    $numberOfDecimals = 0;
			
//Get regular price	
    $price = get_post_meta( get_the_ID(), '_regular_price', true);
    $price = $price  * $_SESSION['price'];
			
//Get sale price
    $salePrice = get_post_meta( get_the_ID(), '_sale_price', true);
    $salePrice = $salePrice  * $_SESSION['price'];
			
//Product SKU
    $sku = $product->get_sku();
    
//Product Attributes
    $bathrooms = $product->get_attribute($this->bathroomName);
    $bedrooms = $product->get_attribute($this->bedroomName);
    $rent = $product->get_attribute($this->rentName);
    $sale = $product->get_attribute($this->saleName);
    $sold = $product->get_attribute($this->soldName);
        
//Get image alt by url
        $imageAlt =  wp_prepare_attachment_for_js($attachment_ids[0]);
        ?>
<!-- apartment list -> 3 in a row -->
                <div class="col-xs-12 col-sm-4 item">
                 
<!-- START favorites icon -->
                 
                        <?php
// Get products and display if in favorites or not
                        global $wpdb;
        $table = $wpdb->prefix . 'citadelle_favorites'; 
        $user = $this->UserId;
       $result = $wpdb->get_results( "SELECT * FROM $table WHERE userid = '".$user."' " );
        for ($a = 0; $a < count($result) ; $a++){
        if($result[$a]->productid == get_the_ID())
        {
            
            $ico = "animated favorite bounceIn";
            $subname = "deletefavorite";
           
        }   
        																			}
                    ?>
                     <form method="post">
                        <input type="hidden" name="prodid" value="<?php echo get_the_ID(); ?>">
              <button style="background:transparent" name="<?php if($subname != "0"){echo $subname;}else{echo "favorite";}?>" type="submit" class="fa fa-heart-o <?php if($ico != "0") {
                    echo $ico;
                } ?>" aria-hidden="true"></button></form>
                 
            <?php
            $ico = 0;
            $subname = 0;
        
        ?>
<!-- END favorites icon -->
                   
                    <div class="ribbon <?php 
                    if($rent){
                        echo " rent ";
                        $ribbon = __('For Rent', 'real-estate');
                    }
                else if($sold){
                    echo "sold ";
                    $ribbon = __('Sold', 'real-estate');
                }
                else{
                    echo "sale ";
                    $ribbon = __('Sale', 'real-estate');
                } ?>
                ">
                        <?php echo $ribbon; ?>
                    </div>
                    <!-- ribbon -->
                    <img src="<?php echo $attachment[0]; ?>" class="img-responsive" alt="<?php echo $imageAlt['alt']; ?>">
                    <!-- product image -->
                    <div class="facility">
                        <!-- bedrooms and bathrooms -->
                        <div class="col-xs-6">
                            <span class="sprite-load bedrooms"></span>
                            <?php echo $bedrooms; ?> bedrooms
                        </div>
                        <div class="col-xs-6">
                            <span class="sprite-load bathrooms"></span>
                            <?php echo $bathrooms; ?> bathrooms
                        </div>
                    </div>
                    <div class="short-description">
                        <!-- short description -->
                        <div class="col-xs-12">
                            <h3>
                  <a href="<?php echo get_permalink() ; ?>"><b><?php echo get_the_title(); ?></b></a>
                </h3>
                            <p>
                                <?php echo $shortDescription[0]; ?><a href="<?php echo get_permalink() ; ?>" class="stripped-more"> [...]</a>
                            </p>
                        </div>
                        <div class="separator"></div>
                        <div class="buy-details clearfix">
                            <div class="col-xs-5 prod-id">
                                ID:
                                <?php echo $sku ; ?>
                            </div>
                            <div class="col-xs-7 price">
                                <b><?php echo $currency ; ?> <?php if($salePrice){
                        echo number_format($salePrice, $numberOfDecimals,".",",");
                    }
                      else {
                      echo number_format($price, $numberOfDecimals,".",",");
                      }
                      ?></b>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- end 3 in a row -->
                    <?php if($i % 3 == 0 && $i < $this->numberOfProducts):?>
                    <div class="col-xs-12 title">
          <div class="line"></div>
        </div>
               <?php endif; 
//Increment the $i value
			
    $i++;
    endwhile; 
    wp_reset_query(); 
            ?>
        </div>
    </div>
    <?php
    }
    
    
//Display the products from the Recently Added Module
     function displayRecently()
    {
    
//Insert the product ID to the database based on user ID.
        
        if(isset($_POST['favorite'])){
        global $wpdb;
        $prodid = $_POST['prodid']; 
        $wpdb->insert($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
        
        //Delete the product id from the database
         if(isset($_POST['deletefavorite'])){
        global $wpdb;
        $prodid = $_POST['prodid']; 
        $wpdb->delete($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
      ?>
      
    <div class="recently-added">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 title">
                    <h2><b><?php _e('Recently added', 'real-estate'); ?></b></h2>
                    <div class="line"></div>
                </div>
            </div>
<!-- recently carousel: START -->
            <div class="jcarousel-wrapper">
                <div class="jcarousel">
                    <ul>
                      <?php
// Set the arguments for getting the products
       
            $params = array(
                'post_per_page' => '1',
                'post_type' => 'product'
            );
      
// Get the last X added products and form the loop
			 
           $loop = new WP_Query($params);
//Define a custom $i variable
                    $i = 1;
    
    while ( $loop->have_posts() && $i <= $this->numberOfProducts ) : $loop->the_post(); 
    global $product; 
			 
// Set the variables that will be used to display the product
    
//Get image URL
     $attachment_ids[0] = get_post_thumbnail_id( $product->id );
     $attachment = wp_get_attachment_image_src($attachment_ids[0], 'full' );
    
//Get and split the short description to 190 characters
    $shortDescription = str_split(get_the_excerpt(),190);
    
//Get the currency
    $currency = $_SESSION['currency'];
    $numberOfDecimals = 0;
			 
//Get regular price
    $price = get_post_meta( get_the_ID(), '_regular_price', true);
    $price = $price  * $_SESSION['price'];
			 
//Get sale price
    $salePrice = get_post_meta( get_the_ID(), '_sale_price', true);
         $salePrice = $salePrice  * $_SESSION['price'];
			 
//Product SKU
    $sku = $product->get_sku();
    
//Product Attributes
    $bathrooms = $product->get_attribute($this->bathroomName);
    $bedrooms = $product->get_attribute($this->bedroomName);
    $rent = $product->get_attribute($this->rentName);
    $sale = $product->get_attribute($this->saleName);
    $sold = $product->get_attribute($this->soldName);
        
//Get image alt by url
        $imageAlt =  wp_prepare_attachment_for_js($attachment_ids[0]);  
        ?>
<!-- apartment list -> 3 in a row -->
                <li class="item">
<!-- START favorites icon -->
                 
                        <?php
// Get products and display if in favorites or not
			 
        global $wpdb;
        $table = $wpdb->prefix . 'citadelle_favorites'; 
        $user = $this->UserId;
        $result = $wpdb->get_results( "SELECT * FROM $table WHERE userid = '".$user."' " );
        for ($a = 0; $a < count($result) ; $a++){
        if($result[$a]->productid == get_the_ID())
        {
            $ico = "animated favorite bounceIn";
            $subname = "deletefavorite";
        }   
        }
                    ?>
                     <form method="post">
                        <input type="hidden" name="prodid" value="<?php echo get_the_ID(); ?>">
              <button style="background:transparent" name="<?php if($subname != "0"){echo $subname;}else{echo "favorite";}?>" type="submit" class="fa fa-heart-o <?php if($ico != "0") {
                    echo $ico;
                } ?>" aria-hidden="true"></button></form>
                 
            <?php
            $ico = 0;
            $subname = 0;
        
        ?>
                       

                    
<!-- END favorites icon -->
                    <div class="ribbon <?php 
                    if($rent){
                        echo " rent ";
                        $ribbon = __('For Rent', 'real-estate');
                    }
                else if($sold){
                    echo "sold ";
                    $ribbon = __('Sold', 'real-estate');
                }
                else{
                    echo "sale ";
                    $ribbon = __('Sale', 'real-estate');
                } ?>
                ">
                        <?php echo $ribbon; ?>
                    </div>
<!-- ribbon -->
                    <img src="<?php echo $attachment[0]; ?>" class="img-responsive" alt="<?php echo $imageAlt['alt']; ?>">
<!-- product image -->
                    <div class="facility">
<!-- bedrooms and bathrooms -->
                        <div class="col-xs-6">
                            <span class="sprite-load bedrooms"></span>
                            <?php echo $bedrooms; ?> bedrooms
                        </div>
                        <div class="col-xs-6">
                            <span class="sprite-load bathrooms"></span>
                            <?php echo $bathrooms; ?> bathrooms
                        </div>
                    </div>
                    <div class="short-description">
<!-- short description -->
                        <div class="col-xs-12">
                            <h3>
                  <a href="<?php echo get_permalink() ; ?>"><b><?php echo get_the_title(); ?></b></a>
                </h3>
                            <p>
                                <?php echo $shortDescription[0]; ?><a href="<?php echo get_permalink() ; ?>"> [...]</a>
                            </p>
                        </div>
                        <div class="separator"></div>
                        <div class="buy-details clearfix">
                            <div class="col-xs-5 prod-id">
                                ID:
                                <?php echo $sku ; ?>
                            </div>
                            <div class="col-xs-7 price">
                                <b><?php echo $currency ; ?> <?php if($salePrice){
                        echo number_format($salePrice, $numberOfDecimals,".",",");
                    }
                      else {
                      echo number_format($price, $numberOfDecimals,".",",");
                      }
                      ?></b>
                            </div>
                        </div>
                    </div>

<!-- end 3 in a row -->
                        </li>
                <?php
//Increment the $i value
    $i++;
    endwhile; 


    wp_reset_query(); 
            ?>
                    </ul>
                </div>
                <!-- slider controls -->
                <div class="mobile-show">
                    <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                    <a href="#" class="jcarousel-control-next">&rsaquo;</a>
                </div>
                <p class="jcarousel-pagination"></p>
            </div>
        </div>
    </div>
    <?php
    }
        
//Display Products Based on a category
    
    
     function displayCategoryProducts($categorie)
    {
          
//Insert the product ID to the database based on user ID.
        
        if(isset($_POST['favorite'])){
        global $wpdb;
        $prodid = $_POST['prodid']; 
        $wpdb->insert($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
        
//Delete the product id from the database
        if(isset($_POST['deletefavorite'])){
        global $wpdb;
        $prodid = $_POST['prodid']; 
        $wpdb->delete($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
    ?>


    <div class="container property-listings">
        <div class="row">
            <div class="col-xs-12 title">
                <h2 class="text-center"><a href="#"><b><?php echo $categorie ?></b></a></h2>
                <p class="text-center">
                
                </p>
                <div class="line"></div>
            </div>
            <div class="paginate"></div>
            <div class="content">
            <?php
// Set the arguments for getting the products
       
            $params = array(
                'post_per_page' => '1',
                'post_type' => 'product',
                'product_cat' => $categorie
            );
      
// Get the last X added products and form the loop
           $loop = new WP_Query($params);
			 
//Define a custom $i variable
                    $i = 1;
    
    while ( $loop->have_posts() ) : $loop->the_post(); 
    global $product; 
			 
// Set the variables that will be used to display the product
    
//Get image URL
     $attachment_ids[0] = get_post_thumbnail_id( $product->id );
     $attachment = wp_get_attachment_image_src($attachment_ids[0], 'full' );
    
//Get and split the short description to 190 characters
    $shortDescription = str_split(get_the_excerpt(),190);
    
//Get the currency
    $currency = $_SESSION['currency'];
    $numberOfDecimals = 0;
			 
//Get regular price
    $price = get_post_meta( get_the_ID(), '_regular_price', true);
    $price = $price  * $_SESSION['price'];
			 
//Get sale price
    $salePrice = get_post_meta( get_the_ID(), '_sale_price', true);
    $salePrice = $salePrice  * $_SESSION['price'];
			 
//Product SKU
    $sku = $product->get_sku();
    
//Product Attributes
    $bathrooms = $product->get_attribute($this->bathroomName);
    $bedrooms = $product->get_attribute($this->bedroomName);
    $rent = $product->get_attribute($this->rentName);
    $sale = $product->get_attribute($this->saleName);
    $sold = $product->get_attribute($this->soldName);
        
//Get image alt by url
    $imageAlt =  wp_prepare_attachment_for_js($attachment_ids[0]);

        ?>
<!-- apartment list -> 3 in a row -->
                <div class="col-xs-12 col-sm-4 item">
<!-- START favorites icon -->
                 
                        <?php
// Get products and display if in favorites or not
        global $wpdb;
        $table = $wpdb->prefix . 'citadelle_favorites'; 
        $user = $this->UserId;
        $result = $wpdb->get_results( "SELECT * FROM $table WHERE userid = '".$user."' " );
        for ($a = 0; $a < count($result) ; $a++){
        if($result[$a]->productid == get_the_ID())
        {
            $ico = "animated favorite bounceIn";
            $subname = "deletefavorite";
        }
            
        }
 ?>
                     <form method="post">
                        <input type="hidden" name="prodid" value="<?php echo get_the_ID(); ?>">
              <button style="background:transparent" name="<?php if($subname != "0"){echo $subname;}else{echo "favorite";}?>" type="submit" class="fa fa-heart-o <?php if($ico != "0") {
                    echo $ico;
                } ?>" aria-hidden="true"></button></form>
                 
            <?php
            $ico = 0;
            $subname = 0;
        
        ?>
                       
                
                    
<!-- END favorites icon -->
                    <div class="ribbon <?php 
                    if($rent){
                        echo " rent ";
                        $ribbon = __('For Rent', 'real-estate');
                    }
                else if($sold){
                    echo "sold ";
                    $ribbon = __('Sold', 'real-estate');
                }
                else{
                    echo "sale ";
                    $ribbon = __('Sale', 'real-estate');
                } ?>
                ">
                        <?php echo $ribbon; ?>
                    </div>
<!-- ribbon -->
                    <img src="<?php echo $attachment[0]; ?>" class="img-responsive" alt="<?php echo $imageAlt['alt']; ?>">
                    <!-- product image -->
                    <div class="facility">
<!-- bedrooms and bathrooms -->
                        <div class="col-xs-6">
                            <span class="sprite-load bedrooms"></span>
                            <?php echo $bedrooms; ?> bedrooms
                        </div>
                        <div class="col-xs-6">
                            <span class="sprite-load bathrooms"></span>
                            <?php echo $bathrooms; ?> bathrooms
                        </div>
                    </div>
                    <div class="short-description">
<!-- short description -->
                        <div class="col-xs-12">
                            <h3>
                  <a href="<?php echo get_permalink() ; ?>"><b><?php echo get_the_title(); ?></b></a>
                </h3>
                            <p>
                                <?php echo $shortDescription[0]; ?><a href="<?php echo get_permalink() ; ?>"> [...]</a>
                            </p>
                        </div>
                        <div class="separator"></div>
                        <div class="buy-details clearfix">
                            <div class="col-xs-5 prod-id">
                                ID:
                                <?php echo $sku ; ?>
                            </div>
                            <div class="col-xs-7 price">
                                <b><?php echo $currency ; ?> <?php if($salePrice){
                        echo number_format($salePrice, $numberOfDecimals,".",",");
                    }
                      else {
                      echo number_format($price, $numberOfDecimals,".",",");
                      }
                      ?></b>
                            </div>
                        </div>
                    </div>
                </div>
            
<!-- end 3 in a row -->
                    <?php if($i % 3 == 0 ):?>
                    <div class="col-xs-12 title">
          <div class="line"></div>
        </div>
              </div>
               <?php endif;
			 
//Increment the $i value
    $i++;
			 
    endwhile; 
    wp_reset_query(); 
            ?>
        </div>
    </div>
    <?php
    }
}