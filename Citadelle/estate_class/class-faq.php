<?php

class Faq {

    function displayHeader($image) {
       ?> 
       <div class="jcarousel-wrapper">
      <div class="jcarousel">
        <ul>
          <!-- slide item -->
          <li class="slide-1" style="background-image:url('<?php echo $image['url']; ?>')">
            <div class="title">
              <h2><b>Frequently asked questions</b></h2>
              <p>
                  
                <b>Use the serach bar  to find answers</b>
              </p>
              <!-- search: START -->
              <form class="header-form">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Search questions...">
                  <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
                </div>
              </form>
<!-- search: END -->
              <p>
                <b>or select one of the frequently asked questions below to learn more about <br class="mobile-hide"> buying, selling, and renting real estate. </b>
              </p>
              <a href="#toScroll" class="scroll-down"><i class="fa fa-angle-down" aria-hidden="true"></i></a>
            </div>
          </li>
        </ul>
      </div>
    </div>
       
     <?php
    }
    
    function displayQuestions() {
        ?>
<?php if( have_rows('questions_answers') ): ?>

        <?php $number = 1;?>
	<?php while( have_rows('questions_answers') ): the_row(); 

		// vars
		$question = get_sub_field('question');
		$answer = get_sub_field('answer');
		?>
         <div class="faq-expand expand">
          <h2 data-toggle="collapse" class="btn-expand" data-target="#expand-<?php echo $number; ?>">
            <i class="fa fa-plus"></i>
            <b><?php echo $number;?>. <?php echo $question; ?></b>
          </h2>
          <div id="expand-<?php echo $number;?>" aria-expanded class="collapse expand-content">
            <p> 
             <?php echo $answer;?>
            </p>
          </div>
        </div>


    <?php $number++; ?>
	<?php endwhile; ?>

<?php endif; ?>
<?php
    }

}