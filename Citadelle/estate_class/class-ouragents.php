<?php

class OurAgents 
{

    function displayOurAgents($role)
    {
			
    $i=0;
    $users = get_users(array('role' => 'Agency'));
   
      
    foreach($users as $user):
			
/**
* Get user information
*/
      
        $facebook = get_field('user_facebook', 'user_'.$users[$i]->ID );
        $twitter = get_field('user_twitter', 'user_'.$users[$i]->ID );
        $linkedin = get_field('user_linkedin', 'user_'.$users[$i]->ID );
      	$avatar = get_avatar_url2($users[$i]->ID,256);
        $name = $users[$i]->display_name;
        $bio = get_the_author_meta('description',$users[$i]->ID);
        $splitBio = str_split($bio , 190);
        $email = $users[$i]->user_email;
    		$phone = get_user_meta( $users[$i]->ID, 'billing_phone', true );
   
        ?>
	<div class="item clearfix">
		<!-- item: START -->
		<div class="image">
			<a href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
			<?php print_r($avatar);?>
		</div>
		<div class="details">
			<div class="title">
				<h4>
                  <b> <?php echo $name; ?></b><br class="mobile-show">
                  <a href="<?php if($linkedin){
            echo $linkedin;
        }else{
            echo "#";
        } ;?>">
                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                  </a>
                  <a href="<?php if($facebook){
            echo $facebook;
        }else{
            echo "#";
        } ;?>">
                    <i class="fa fa-facebook" aria-hidden="true"></i>
                  </a>
                  <a href="<?php if($twitter){
            echo $twitter;
        }else{
            echo "#";
        } ;?>">
                    <i class="fa fa-twitter" aria-hidden="true"></i>
                  </a>
                </h4>
				<div class="line"></div>
				<p class="four-lines-text">
					<?php echo $splitBio[0]; ?>
				</p>
			</div>

			<div class="contact">
				<div>
					<a href="tel:<?php echo $phone; ?>">
						<i class="fa fa-phone" aria-hidden="true"></i>
						<?php echo $phone; ?>
					</a>

				</div>

				<div>
					<a href="mailto:<?php echo $email; ?>">
						<i class="fa fa-envelope-o" aria-hidden="true"></i>
						<?php echo $email; ?>
					</a>
				</div>

				<div class="listings">
					<a href="listings-page/?id=<?php echo $users[$i]->ID;?>" class="btn-secondary">
                    MY LISTINGS
                  </a>
				</div>
			</div>
		</div>
	</div>
	<!-- item: END -->

	<?php 
		$i++;
    endforeach; 
    }
    
    //function to display all agents on the front page
    
    function displayAgents()
    {
    ?>
		<div class="agents">
			<div class="container">
				<div class="row">

					<div class="col-xs-12 title">
						<h3><a href="#"><b><?php _e('Our agents','real-estate'); ?></b></a></h3>
						<div class="line"></div>
					</div>
					<!-- start agents carousel -->
					<div class="jcarousel-wrapper ">
						<div class="jcarousel clearfix">
							<ul>
								<?php
    $i=0;
    $users = get_users(array('role' => 'Agent'));
 			if(count($users) == 0){
            $users = get_users(array('role' => 'administrator'));
        
    }
    		foreach($users as $user):
/**
* Get user information
*/
			
        $avatar = get_avatar_url2($users[$i]->ID,256);
        $name = $users[$i]->display_name;
        $bio = get_the_author_meta('description',$users[$i]->ID);
        $splitBio = str_split($bio , 190);
        $email = $users[$i]->user_email;
        $phone = get_user_meta( $users[$i]->ID, 'billing_phone', true );
   
        ?>

									<li>
										<?php print_r($avatar);?>
											<div class="agent">
												<h4 class="agent-name">
                  <?php echo $name; ?>
                </h4>
												<div class="line"></div>
												<p class="text-center">
													<?php echo $splitBio[0]; ?>
												</p>
											</div>
											<div class="contact">
												<a href="tel:<?php echo $phone; ?>" class="phone-call"><i class="fa fa-phone" aria-hidden="true"></i><?php echo get_field('call_agent',$id) ;?></a>
												<div class="phone-no">
													<?php echo $phone; ?>
												</div>
											</div>
											<div class="contact">
												<a href="mailto:<?php echo $email; ?>"><i class="fa fa-envelope-o" aria-hidden="true"></i><?php echo get_field('send_mail',$id) ;?></a>
											</div>

											<a href="listings-page/?id=<?php echo $users[$i]->ID; ?>" class="btn-secondary hvr-bounce-to-right">
												<?php _e('MY LISTINGS','real-estate') ;?>
											</a>
									</li>
									<?php 
			$i++; 
			endforeach; ?>

							</ul>
						</div>

						<div class="mobile-show">
							<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
							<a href="#" class="jcarousel-control-next">&rsaquo;</a>
						</div>
						<p class="jcarousel-pagination"></p>
					</div>

				</div>
			</div>
		</div>
		<?php
    }
/**
* Display agent for specific product
*/
    
    function displayAgent()
    {
    
/**
 * Grabbing all the variables needed for the agent
 */
    global $post;

    $author_id=$post->post_author;
    $avatar = get_avatar_url2($author_id,256);
    $email = get_the_author_meta( 'email',$author_id);
    $description = get_the_author_meta( 'description',$author_id);
    $phone = get_the_author_meta( 'billing_phone',$author_id);
    $name = get_the_author_meta( 'display_name',$author_id);
if(isset($_POST['contact'])){
    $subject = "New Request";
    $message = $_POST['message'];
    $emailUser= $_POST['email'];
    $headers[] = 'From: '.$name.' <'.$emailUser.'>';
    wp_mail( $email, $subject, $message, $headers, $attachments );
        										}
        ?>
			<div class="col-xs-12 col-md-4">
				<!-- form start -->
				<form id="agent-form" class="form-agent" method="post">
					<div class="agent-form">
						<div class="row">
							<div class="col-xs-5">
								<?php echo $avatar ;?>
							</div>
							<div class="col-xs-7">
								<h3><b><?php echo $name ;?></b></h3>
								<div class="line"></div>
								<p>
									<a href="mailto:<?php echo $email ;?>">
										<?php echo $email ;?>
									</a>
								</p>
								<p>
									<a href="tel:<?php echo $phone ;?>">
										<?php echo $phone ;?>
									</a>
								</p>
								<p>
									<u><a href="listings-page/?id=<?php echo $author_id; ?>"><?php _e('My listings', 'real-estate'); ?></a></u>
								</p>
							</div>
						</div>
						<input type="text" id="name" placeholder="Your name *" name="name" required>
						<input type="text" id="phone" placeholder="Phone number *" name="phone" required>
						<input type="email" id="email" placeholder="E-mail*" name="email" required>
						<textarea name="message" id="" cols="30" rows="5" placeholder="Write your message here*" required></textarea>
					</div>
					<button type="submit" name="contact" class="btn-secondary">
						<?php _e('CONTACT AGENT' ,'real-estate'); ?>
					</button>
				</form>
				<!-- form end -->
			</div>

			<?php
    }

}