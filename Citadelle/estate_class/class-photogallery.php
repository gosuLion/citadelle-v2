<?php

class PhotoGallery
{
    function displayGallery()
    {
    ?>
    
    <div class="bg-f5f5f5 photo-gallery clearfix">
  <div class="container">
                <?php

			// Adding Breadcrumbs by Yoast


if ( function_exists('yoast_breadcrumb') && !is_front_page() ) {
yoast_breadcrumb('
<div class="breadcrumbs"><p>','</p></div>
');
}
?>
    <div class="row">
      <div class="col-xs-12 title">
        <h2 class="text-center"><a href="#"><b><?php _e('Photo gallery','real-estate'); ?></b></a></h2>
        <div class="line"></div>
      </div>
    </div>
  </div>
   <?php
			
    $images = get_field('add_photos');
			
			if( $images ): 
			foreach( $images as $image ): ?>
            
             <a href="<?php echo $image['url'] ; ?>" data-toggle="lightbox" data-gallery="multiimages" class="gallery-item">
    <img src="<?php echo $image['sizes']['large'] ; ?>" class="img-responsive">
    <div class="overlay">
    </div>
    <i class="fa fa-eye" aria-hidden="true"></i>
  </a>
        <?php endforeach; 
			endif; ?>
</div>
    
    <?php
    }
}