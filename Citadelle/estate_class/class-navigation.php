<?php

class Navigation
{

   
    function displayLogo()
    {
        global $redux_demo;
        
         $url = home_url();
        
//Check if a logo is defined in Theme Options
        
        if($redux_demo['desktop_logo']['url'] == ""){
            $uri = get_site_url();
            $desktopLogo = $uri."/wp-content/themes/Citadelle/assets/images/desktop-logo.jpg";
            $mobileLogo = $uri."/wp-content/themes/Citadelle/assets/images/mobile-logo.jpg"; 
        }
        else
        {
            
        $desktopLogo = $redux_demo['desktop_logo']['url'];
        $mobileLogo = $redux_demo['mobile_logo']['url'];
            
        }
        
        
        ?>
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only"><?php _e('Toggle navigation','real-estate'); ?></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<?php echo esc_url( $url ); ?>">
              <img src="<?php echo $desktopLogo;?>" class="img-responsive mobile-hide" alt="<?php echo get_field('desktop_logo','option')['alt'];?>">
              <img src="<?php echo $mobileLogo;?>" class="img-responsive mobile-show" alt="<?php echo get_field('desktop_logo','option')['alt'];?>">
            </a>
          </div>
          <?php
    }
    
    
    function displayFooterLogo()
    {
        global $redux_demo;
         $uri = get_site_url();
          $url = home_url();
        if($redux_demo['footer_logo']['url'] == ""){
            
                        $image= $uri."/wp-content/themes/Citadelle/assets/images/footer-logo.jpg";

        }
        else
        {
            $image = $redux_demo['footer_logo']['url'];
        }
        ?>
        <a href="<?php echo esc_url( $url ); ?>">
              <img src="<?php echo $image;?>" class="img-responsive footer-logo" alt="<?php echo get_field('footer_logo','option')['alt'];?>">
            </a>
            <?php
    }
    
    
    function displayContactInfo()
    {
        global $redux_demo;
        
        $location = get_field('homepage_map','option');
        ?>
             <div class="mobile-hide">
            <ul class="nav navbar-nav contact">
              <li>
                <a href="tel:<?php 
        
        if($redux_demo['phone-header'] == ""){
            echo "0800 800 800";
        }
        else
        {
        echo $redux_demo['phone-header']; 
        }?>">
                  <i class="fa fa-phone" aria-hidden="true"></i>
                  <?php 
        
        if($redux_demo['phone-header'] == ""){
            echo "0800 800 800";
        }
        else
        {
        echo $redux_demo['phone-header']; 
        }?>
                  
                </a>
              </li>
              <li class="border-right-black">
              <?php if($location != ""): ?>
                <a href="https://www.google.ro/maps/@<?php echo $redux_demo['maps-latitude'];?>,<?php echo $redux_demo['maps-longitude'];?>,15z?hl=en" target="_blank">
                 
                  <i class="fa fa-map-marker" aria-hidden="true"></i>
                  
                  <?php echo $redux_demo['maps-city'] ?>
                </a>
                <?php endif;?>
                    <?php if($location == ""): ?>
                <a href="https://www.google.ro/maps/place/Melbourne+VIC,+Australia/@-37.8106012,144.9623603,15z/data=!4m5!3m4!1s0x6ad646b5d2ba4df7:0x4045675218ccd90!8m2!3d-37.8136276!4d144.9630576" target="_blank">
                 
                  <i class="fa fa-map-marker" aria-hidden="true"></i>
                  
            
                  <?php echo "Melbourne"; ?>
                </a>
                <?php endif;?>
              </li>
            </ul>
      <?php  
    }
    
    function displaySocials(){
			
        global $redux_demo;
        
    ?>
       
        <ul class="nav navbar-nav contact socials">
	<?php if($redux_demo['social-facebook'] !=""):?>
                 <li>
                <a href="<?php echo $redux_demo['social-facebook']; ?>">
                  <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
              </li>
               <?php endif;?>
               
               <?php if($redux_demo['social-linkedin'] !=""): ?>
                <li>
                <a href="<?php echo $redux_demo['social-linkedin']; ?>">
                  <i class="fa fa-linkedin" aria-hidden="true"></i>
                </a>
              </li>
               <?php endif; ?>
               <?php if($redux_demo['social-twitter'] !=""): ?>
                <li>
                <a href="<?php echo $redux_demo['social-twitter']; ?>">
                  <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
              </li>
<?php endif;?>
            </ul>
    <?php
    }
    function displayFooterSocials(){
			
        global $redux_demo;
			
           ?>
       
       <p class="social">
              <b><?php _e('Follow us on:' , 'real-estate'); ?> </b> <br class="mobile-show">    
               
               	<?php if($redux_demo['social-facebook'] !=""):?>
           
                 
                <a href="<?php echo $redux_demo['social-facebook']; ?>">
                  <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
           
               <?php endif;?>
               
               <?php if($redux_demo['social-linkedin'] !=""): ?>
           
                <a href="<?php echo $redux_demo['social-linkedin']; ?>">
                  <i class="fa fa-linkedin" aria-hidden="true"></i>
                </a>
        
               <?php endif; ?>
               
               <?php if($redux_demo['social-twitter'] !=""): ?>
             
                <a href="<?php echo $redux_demo['social-twitter']; ?>">
                 <i class="fa fa-twitter" aria-hidden="true"></i>
                </a>
         
<?php endif;?>
               
                
                
                 </p>

          
    <?php
    }
    
}
















