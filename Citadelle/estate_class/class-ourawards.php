<?php

class OurAwards
{
    function displayAwards()
    {
    ?>
    <div class="container awards">
  <div class="row">
    <div class="col-xs-12 title">
      <h3><a href="#"><b><?php _e('Awards', 'real-estate');?></b></a></h3>
      <img src="wp-content/themes/Citadelle/assets/images/separator.png" class="img-responsive center-block" alt="">
    </div>
    <div class="content">
    <?php if( have_rows('add_awards') ): 
      while( have_rows('add_awards') ): the_row(); 
			
// Get variables
			
		$image = get_sub_field('awards_image');
		$description = get_sub_field('awards_description');
		?>
                        <div class="col-xs-12 col-md-3">
                            <div class="col-xs-6">
                                <img src="<?php echo $image['url']; ?>" class="img-responsive" alt="<?php echo $image['alt']; ?>">
                            </div>
                            <div class="col-xs-6 text">
                                <?php echo $description; ?>
                            </div>
                        </div>

<?php 
			endwhile;
			endif; ?>
    </div>
    <div class="col-xs-12">
      <div class="separator"></div>
      <div class="separator second"></div>
    </div>

  </div>
</div>
    <?php
    }
}