<?php
class Currency {    
//Set User Id Variable
    public $UserId;    
    
    public function __construct()
    {
        global $redux_demo;        
        if ( get_current_user_id())
     {
        $this->UserId = get_current_user_id();
     }
        else
        {
            $cookie = $_COOKIE;
            $this->UserId = session_id();
           
            
        }
    }
    
     function displaySwitch() 
     {
        
                if(!empty($_POST)){
                    global $wpdb;
                    $table = $wpdb->prefix . 'citadelle_currency';
                    $wpdb->insert($table,array('userid' => $this->UserId,'surface' =>  $_POST['switch']));
                }
                
             
       wp_reset_query();
    
       global $wpdb;
         
        $table = $wpdb->prefix . 'citadelle_currency'; 
        $a = $this->UserId;
        $result = $wpdb->get_row( "SELECT * FROM $table WHERE userid = '".$a."' ORDER BY id DESC" );
        
             if($result->surface == "on")
             {
                 $check = "checked='checked'";
                 $value = "off";
                  $_SESSION['check']= $check;
                  $_SESSION['value'] = "off";
                  $_SESSION['change'] = 1;
             }
              else{
                  $_SESSION['value'] = "on";
                  $value = "on";
                  $check = "";
                   $_SESSION['check']= $check;
                  $_SESSION['change'] = 10.7639;
              
              }
          wp_reset_query();
      ?>
    <div class="onoffswitch">

        <form id="form" method="post">

            <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" onchange="$('#form').submit();" id="myonoffswitch" <?php echo $_SESSION[ 'check'] ;?>>

            <label class="onoffswitch-label" for="myonoffswitch">
                <span class="onoffswitch-inner"></span>
                <span class="onoffswitch-switch"></span>
            </label>

            <input type="hidden" value="<?php echo $_SESSION['value'];?>" name="switch">
        </form>

    </div>

    <?php
    }
    
    function displayCurrency() 
    
    {
        
//Define Global Variables
        
         global $wpdb;
         global $redux_demo;
        
        
        $table = $wpdb->prefix . 'citadelle_currency';
    ?>
        <li class="dropdown language">
            <?php 
        if($_POST['language'] !=""){
          
                
  $wpdb->insert($wpdb->prefix . 'citadelle_currency',array('userid' => $this->UserId,'currency' => $_POST['language']) );
        }
    ?>
                <form method="post" style="background-color:transparent">
                    <!-- currency bar: START -->

                    <a href="#" id="monetary" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true">
                        <?php
        global $wpdb;
        $a = $this->UserId;
        $result = $wpdb->get_row( "SELECT * FROM $table WHERE userid = '".$a."' ORDER BY id DESC" );
            
//Define values for price exchange
// If the values in Theme Options are not set, take the default values
        
        if ($redux_demo['usd-value'] == "" && $redux_demo['gbp-value'] == "" && $redux_demo['euro-value'] == "")
        {
            $usd = 1;
            $gbp = 1;
            $eur = 1;
             
        }
        else
        {
            
            $usd = $redux_demo['usd-value'];
            $gbp = $redux_demo['gbp-value'];
            $eur = $redux_demo['euro-value'];
           
        }
    
            if($result->currency == "USD")
            {
                $_SESSION['currency'] = "$";
                $_SESSION['price'] = $usd;
                
            }
        else if ($result->currency == "GBP")
        {
            $_SESSION['currency'] = "£";
            $_SESSION['price'] = $gbp;
        }
        else
        {
           
            $_SESSION['currency'] = "€";
            $_SESSION['price'] = $eur;
        }
       if(! $result->currency){
           echo "EUR";
       }
        else {
            
       
        echo $result->currency;
             }
        ?>
                            <i class="fa fa-angle-down" aria-hidden="true"></i></a>

                    <ul class="dropdown-menu">
                        <li>
                            <button style="background-color:#f5f5f5;color:#333" type="submit" name="language" value="USD">USD</button>
                        </li>
                        <li>
                            <button style="background-color:#f5f5f5;color:#333" type="submit" name="language" value="GBP">GBP</button>
                        </li>
                        <li>
                            <button style="background-color:#f5f5f5;color:#333" type="submit" name="language" value="EUR">EUR</button>
                        </li>
                    </ul>
                    <!-- currency bar: END -->
                </form>
        </li>

        <?php
    }

}