<?php

    class Neighbourhoods {
    
        function displayCategoriesFeatured($parent_cat_NAME,$showNumber){
        ?>
        <?php
        $IDbyNAME = get_term_by('name', $parent_cat_NAME, 'product_cat');
  			$product_cat_ID = $IDbyNAME->term_id;
				$args = array(
       		'hierarchical' => 1,
       		'show_option_none' => '',
					'hide_empty' => 0,
       		'parent' => $product_cat_ID,
       		'taxonomy' => 'product_cat'
										);
					
  			$subcats = get_categories($args);
      $i = 1; 
      
   foreach ($subcats as $subcat ) :
        if($i < 4):
      
            $cat_thumb_id = get_woocommerce_term_meta( $subcat->term_id, 'thumbnail_id', true );
        		$cat_thumb_url = wp_get_attachment_image_src( $cat_thumb_id, 'shop_catalog' );
            $category_link = get_category_link( $subcat->term_id );
            $term = get_term(  $subcat->term_id, 'product_cat' ); 
            $value = $term->count;
            
       ;?>
         


      <div class="col-xs-12 col-sm-4">
        <div class="item">
          <img src="<?php if($cat_thumb_url[0] != 0)
            {
            echo $cat_thumb_url[0] ;
            }
            else
            {
            echo get_site_url()."/wp-content/themes/Citadelle/assets/images/cattumb.jpg";
            } ?>" width="100%" class="img-responsive" alt="">
          <div class="overlayer">
            <p class="name">
              <b><?php echo $subcat->name ; ?></b>
            </p>
            <p class="lisings">
              <?php if($value){echo $value;} ?> listings
            </p>
            <p>
              <a href="<?php echo $category_link; ?>" class="btn">See all listings</a>
            </p>
          </div>
        </div>
      </div>

    
  
     <?php $i++; 
					endif;
     endforeach;?>
   
        <?php
        }
        
        function displayOtherAreas($parent_cat_NAME){
            $totalPosts = 30;
            $IDbyNAME = get_term_by('name', $parent_cat_NAME, 'product_cat');
            $product_cat_ID = $IDbyNAME->term_id;
    				$args = array(
       				'hierarchical' => 1,
         			'posts_per_page' => 2,
       				'show_option_none' => '',
       				'hide_empty' => 0,
       				'parent' => $product_cat_ID,
							'taxonomy' => 'product_cat',
        			'nopaging' => true,  
    										);
					
					  $subcats = get_categories($args);
						$i = 1; 
      
    			foreach ($subcats as $subcat) :
      			if ($i <= $totalPosts) :
     
            	$cat_thumb_id = get_woocommerce_term_meta( $subcat->term_id, 'thumbnail_id', true );
        			$cat_thumb_url = wp_get_attachment_image_src( $cat_thumb_id, 'shop_catalog' );
              $category_link = get_category_link( $subcat->term_id );
       ;?>
         


      <div class="list clearfix ajax">  <!--item -->
      <div class="left">
        <img src="<?php  if($cat_thumb_url[0] != 0)
            {
            echo $cat_thumb_url[0] ;
            }
            else
            {
                echo get_site_url()."/wp-content/themes/Citadelle/assets/images/cattumb.jpg";
            }?>" width="100%" class="img-responsive center-block" alt="">
      </div>
      <div class="right">
        <div class="head clearfix">
          <div class="name">
            <b><?php echo $subcat->name ; ?></b>
          </div>
          <div class="">
            
<?php
            $listings = 0;
            $IDbyNAME = get_term_by('name', $subcat->name, 'product_cat');
            $product_cat_ID = $IDbyNAME->term_id;
    				$args = array(
								'hierarchical' => 1,
								'show_option_none' => '',
       					'hide_empty' => 0,
								'parent' => $product_cat_ID,
								'taxonomy' => 'product_cat',
    										);
					
						$subcats = get_categories($args);
            $numberOfNeighbour = count($subcats);
          
       foreach ($subcats as $subcat ) :
            
       $term = get_term(  $subcat->term_id, 'product_cat' ); 
       $value = $term->count;
       $listings = $listings + $value; 
             
        endforeach;
            
            
            echo $numberOfNeighbour; ?> neighborghoods, <?php echo $listings ;?> listings <br class="mobile-show">
            <a href="<?php echo $category_link; ?>" class="btn">See all <i class="fa fa-angle-right" aria-hidden="true"></i></a>
          </div>
        </div>
        <ul class="plain">
        <?php $row = 1; 
     foreach ($subcats as $subcat ) :   
     $category_link = get_category_link( $subcat->term_id );
         ?>
         
          <a href="<?php echo $category_link; ?>"><li><i class="fa fa-check" aria-hidden="true"></i> <?php echo $subcat->name;?></li></a>
          <?php if($row % 4 ==0):?>
          <ul class="plain">
          </ul>
          
          <?php endif;
            $row++ ; 
          endforeach;?>
          
        </ul>
      </div>
    </div>

    
  
<?php
					
	$i++; 
   endif; 
   endforeach;
        }
    
    }