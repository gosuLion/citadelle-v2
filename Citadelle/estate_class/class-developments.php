<?php
    class Developments{
    
        function displayNewDevelopments() {
                ?>
	<?php
//Insert the product ID to the database based on user ID.
        
        if(isset($_POST['favorite'])){
        global $wpdb;
						$prodid = $_POST['prodid']; 
        $wpdb->insert($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
        
//Delete the product id from the database
         if(isset($_POST['deletefavorite'])){
        global $wpdb;
           $prodid = $_POST['prodid']; 
        $wpdb->delete($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
        
        
                    ?>
		<div class="container">
			<?php

// Adding Breadcrumbs by Yoast


if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<div class="breadcrumbs"><p>','</p></div>
');
}
?>
				<div class="row">
					<div class="col-xs-12 title">
						<h2><b><?php echo get_field('header_title'); ?></b></h2>
						<p>
							<?php echo get_field('header_subtitle');?>
						</p>
						<div class="line"></div>
					</div>
				</div>
				<div class="row" id="six-per-page">
					<div class="col-xs-12 result-head">
<!-- sort: START -->
						<div class="col-xs-6 col-md-3 col-lg-2">
							Sort by:
							<span class="dropdown">
              <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><br class="mobile-show"> Default Order <i class="fa fa-angle-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu">
                <li><a href="javascript: void(0)">Price (Low to High)</a></li>
                <li><a href="javascript: void(0)">Price (High to Low)</a></li>
                <li><a href="javascript: void(0)">Featured</a></li>
                <li><a href="javascript: void(0)">Date Old to New</a></li>
                <li><a href="javascript: void(0)">Date New to Old</a></li>
              </ul>
            </span>
						</div>
<!-- sort: START -->
<!-- order: START -->
						<div class="col-xs-6 col-md-3 col-lg-2">
							Order by:
							<span class="dropdown">
              <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><br class="mobile-show"> Default Order <i class="fa fa-angle-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu">
                <li><a href="javascript: void(0)">Price (Low to High)</a></li>
                <li><a href="javascript: void(0)">Price (High to Low)</a></li>
                <li><a href="javascript: void(0)">Featured</a></li>
                <li><a href="javascript: void(0)">Date Old to New</a></li>
                <li><a href="javascript: void(0)">Date New to Old</a></li>
              </ul>
            </span>
						</div>
<!-- pagination:START -->
						<div class="col-xs-6 col-md-3 pull-right text-right">
							<div class="pagination page_navigation pagination-sm"></div>
						</div>
<!-- pagination:END -->
					</div>

					<!-- paginate container: START -->
					<div class="content col-xs-12">
						<?php if( have_rows('add_development') ): ?>


							<?php while( have_rows('add_development') ): the_row(); 

// vars
		$pageID = get_sub_field('page');
        $id = $pageID[0];
		?>

								<div class="item clearfix">
									<!-- new dev item -->
									<div class="mobile-separator"></div>
									<div class="col-xs-12 col-md-4">
										<!-- START favorites icon -->

										<?php
        // Get products and display if in favorites or not
                        global $wpdb;
        $table = $wpdb->prefix . 'citadelle_favorites'; 
        $user = $this->UserId;
       $result = $wpdb->get_results( "SELECT * FROM $table WHERE userid = '".$user."' " );
        for ($a = 0; $a < count($result) ; $a++){
        if($result[$a]->productid == get_the_ID())
        {
            
            $ico = "animated favorite bounceIn";
            $subname = "deletefavorite";
            ?>
											<?php
        }
            
        }
                ?>
												<?php
        
                    ?>
													<form method="post">
														<input type="hidden" name="prodid" value="<?php echo get_the_ID(); ?>">
														<button style="background:transparent" name="<?php if($subname != " 0 "){echo $subname;}else{echo "favorite ";}?>" type="submit" class="fa fa-heart-o <?php if($ico != " 0 ") {
                    echo $ico;
                } ?>" aria-hidden="true"></button>
													</form>

													<?php
            $ico = 0;
            $subname = 0;
        
        ?>



<!-- END favorites icon -->
														<img src="<?php echo get_field('development_image',$id)['url'];?>" class="img-responsive center-block img" alt="">
									</div>
									<div class="col-xs-12 col-md-8">
										<!-- tabbed content: START -->
										<div class="real-estate-tabbed">
											<div class="row">
												<div class="clearfix over-tab">
													<h2 class="pull-left"><b><?php echo get_the_title($id);?></b> </h2>
													<span class="pull-right"><a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>Share</a></span>
												</div>
<!-- tab head START -->
												<ul class="nav nav-pills">
													<li class="active" role="presentation"><a data-toggle="tab" href="#info">Info</a></li>
													<li class="" role="presentation"><a data-toggle="tab" href="#gallery">Gallery</a></li>
													<li class="" role="presentation"><a data-toggle="tab" href="#prices">Prices</a></li>
													<li class="" role="presentation"><a data-toggle="tab" href="#location">Location</a></li>
													<li class="" role="presentation"><a data-toggle="tab" href="#contact">Contact</a></li>
												</ul>
<!-- tab head END -->

												<!-- tabs content: START -->
												<div class="tab-content col-xs-12 clearfix">

													<div class="tab-pane info active clearfix" id="info">
														<p>
															<?php echo get_field('info_title',$id);?>
														</p>
														<p>
															<?php echo get_field('info_description',$id);?>
														</p>
													</div>

													<div class="tab-pane gallery" id="gallery">
														<?php  $images = get_field('gallery',$id); ?>
															<ul class="plain photo-gallery">
																<?php foreach( $images as $image ): ?>
																	<li>
																		<a href="<?php echo $image['url']; ?>" data-toggle="lightbox" data-gallery="multiimages" data-title="<?php echo $image['alt']; ?>" class="gallery-item">
                            <img src="<?php echo $image['sizes']['large']; ?>" class="img-responsive" width="100%" height="100%">
                            <div class="overlay">
                            </div>
                            <i class="fa fa-eye" aria-hidden="true"></i>
                          </a>
																	</li>

																	<?php endforeach; ?>
															</ul>

													</div>

													<div class="tab-pane prices" id="prices">
														<div class="apt-list">
															<p>FOR SALE AT 450 BROOME</p>
															<table width="100%">
																<tr>
																	<th>Unit</th>
																	<th>Bedrooms</th>
																	<th>Bathrooms</th>
																	<th>Price</th>
																	<th>Sq Ft</th>
																	<th>Date Listed</th>
																	<th>Common Charges*</th>
																	<th>Taxes*</th>
																</tr>
																<?php if( have_rows('add_prices',$id) ): ?>


																	<?php while( have_rows('add_prices',$id) ): the_row(); 

// vars
		$unitNumber = get_sub_field('unit_number',$id);
		$unitLink = get_sub_field('unit_link',$id);
		$bedroooms = get_sub_field('bedrooms',$id);
		$bathrooms = get_sub_field('bathrooms',$id);
		$price = get_sub_field('price',$id);
		$area = get_sub_field('area',$id);
		$date = get_sub_field('date_listed',$id);
		$common = get_sub_field('common_charges',$id);
		$taxes = get_sub_field('taxes',$id);
		

		?>
																		<tr>
																			<td><a href="<?php echo $unitLink;?>"><b><?php echo $unitNumber ;?></b></a></td>
																			<td>
																				<?php echo $bedroooms;?>
																			</td>
																			<td>
																				<?php echo $bathrooms;?>
																			</td>
																			<td>$
																				<?php echo $price;?>
																			</td>
																			<td>
																				<?php echo $area;?>
																			</td>
																			<td>
																				<?php echo $date;?>
																			</td>
																			<td>
																				<?php echo $common;?>
																			</td>
																			<td>
																				<?php echo $taxes;?>
																			</td>
																		</tr>
																		<?php endwhile; ?>
																			<?php endif; ?>

															</table>
														</div>
													</div>

													<div class="tab-pane location" id="location">
														<div class="row">
															<div class="col-xs-12 col-sm-6">
																<ul class="plain">
																	<li><b><?php _e('Address:', 'real-estate');?></b>
																		<?php echo get_field('location_address',$id);?>
																	</li>
																	<li><b><?php _e('City:' , 'real-estate');?></b>
																		<?php echo get_field('location_city',$id);?>
																	</li>
																	<li><b><?php _e('State/Country:', 'real-estate');?></b>
																		<?php echo get_field('location_state',$id);?>
																	</li>
																	<li><b><?php _e('Zip/Postal Code:', 'real-estate'); ?></b>
																		<?php echo get_field('location_zip',$id);?>
																	</li>
																	<li><b><?php _e('Neighborhood:', 'real-estate'); ?></b>
																		<?php echo get_field('location_neighbourhood',$id);?>
																	</li>
																	<li><b><?php _e('Country:', 'real-estate');?></b>
																		<?php echo get_field('location_country',$id);?>
																	</li>
																</ul>
															</div>
															<div class="col-xs-12 col-sm-6">
																<div class="location-map" data-address="<?php echo get_field('location_address',$id);?>"></div>
															</div>
														</div>
													</div>

													<div class="tab-pane contact" id="contact">
														<div class="row">
															<?php $agent1 = get_field('agent_1',$id); 
                            $phone1 = get_user_meta( $agent1['ID'], 'billing_phone', true );
                                $agent2 = get_field('agent_2',$id); 
                            $phone2 = get_user_meta( $agent2->ID, 'billing_phone', true );
            ?>
																<?php if($agent1['display_name'] !=""):?>
																	<div class="col-xs-12 col-sm-6 agent">
																		<div class="col-xs-4 pad-0">
																			<?php echo $agent1['user_avatar']; ?>
																		</div>

																		<div class="col-xs-8">
																			<h3><b><?php echo $agent1['display_name']; ?></b></h3>
																			<div class="line"></div>
																			<ul class="plain">
																				<li>
																					<a href="mailto:<?php echo $agent1['user_email'] ; ?>">
																						<?php echo $agent1['user_email'] ; ?>
																					</a>
																				</li>
																				<li>
																					<a href="tel:<?php echo $phone1;?>">
																						<?php echo $phone1;?>
																					</a>
																				</li>

																			</ul>
																		</div>
																		<a href="#" class="btn-secondary">
																			<?php _e('CONTACT AGENT','real-estate');?>
																		</a>
																	</div>
																	<?php endif;?>

																		<?php if($agent2['display_name'] !=""):?>
																			<div class="col-xs-12 col-sm-6 agent">
																				<div class="col-xs-4 pad-0">
																					<?php echo $agent2['user_avatar']; ?>
																				</div>
																				<div class="col-xs-8">
																					<h3><b><?php echo $agent2['display_name']; ?></b></h3>
																					<div class="line"></div>
																					<ul class="plain">
																						<li>
																							<a href="mailto:<?php echo $agent2['user_email'] ; ?>">
																								<?php echo $agent2['user_email'] ; ?>
																							</a>
																						</li>
																						<li>
																							<a href="tel:<?php echo $phone2;?>">
																								<?php echo $phone2;?>
																							</a>
																						</li>

																					</ul>
																				</div>
																				<a href="#" class="btn-secondary">CONTACT AGENT</a>
																			</div>
																			<?php endif;?>
														</div>
													</div>

												</div>
<!-- tabs content: END -->
											</div>
										</div>
<!-- tabbed content: END -->
									</div>

								</div>

								<?php endwhile; ?>

									<?php endif; ?>

					</div>
<!-- paginate container: END -->
				</div>
		</div>






		<?php
            }
    
    }