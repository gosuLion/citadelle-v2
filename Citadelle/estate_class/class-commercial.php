<?php

    class Commercial {
    
        function displayCategories($parent_cat_NAME)
        {
     
        $IDbyNAME = get_term_by('name', $parent_cat_NAME, 'product_cat');
  $product_cat_ID = $IDbyNAME->term_id;
    $args = array(
       'hierarchical' => 1,
       'show_option_none' => '',
       'hide_empty' => 0,
       'parent' => $product_cat_ID,
       'taxonomy' => 'product_cat'
    );
  $subcats = get_categories($args);
      
        ?>

        <div class="container">
            <?php

// Adding Breadcrumbs by Yoast


        if ( function_exists('yoast_breadcrumb') ) {
        yoast_breadcrumb('
        <div class="breadcrumbs"><p>','</p></div>
        ');
        }
        ?>
                <div class="row item-row">
<!-- item-row: START -->
                    <?php $i = 1; 
            foreach ($subcats as $subcat) :
            
            $cat_thumb_id = get_woocommerce_term_meta( $subcat->term_id, 'thumbnail_id', true );
        $cat_thumb_url = wp_get_attachment_image_src( $cat_thumb_id, 'shop_catalog' );
             $category_link = get_category_link( $subcat->term_id );
             $term = get_term(  $subcat->term_id, 'product_cat' ); 
            $value = $term->count;
       ;?>
                                <div class="col-xs-12 col-sm-4">
                                    <div class="item">
                                        <img src="<?php 
            if($cat_thumb_url[0] != 0)
            {
            echo $cat_thumb_url[0] ;
            }
            else
            {
                echo get_site_url()." /wp-content/themes/Citadelle/assets/images/cattumb.jpg ";
            }
                
                ?>" width="100%" class="img-responsive" alt="">
                                        <div class="overlayer">
                                            <p class="name">
                                                <b><?php echo $subcat->name ; ?></b>
                                            </p>
                                            <p class="lisings">
                                                <?php echo $value; ?> listings
                                            </p>
                                            <p>
                                                <a href="<?php echo $category_link; ?>" class="btn">See all listings</a>
                                            </p>
                                        </div>
                                    </div>
                                </div>

                                <?php if($i % 3 == 0):?>
                </div>
                <div class="row item-row">
                    <?php endif;
                    $i++; 
                    endforeach;?>



                </div>
<!-- item-row: END -->



        </div>

        <?php
        }
    
    }