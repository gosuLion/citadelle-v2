<?php

class Maps
{
    function displaySearchMap()
    {
    ?>
       <!-- full width map: START -->

    <!-- map pins: START -->

    <!-- to be placed anywhere inside DOM -->
    <!-- add an input as a marker with the address as value -->
    <!-- NOT OPTIONAL: center as an atribute to center map on it -->
    <!-- center ping is draggable and has a radius circle -->
    <!-- optional: data-map-pin attribute for custom map pin image -->
    <!-- optional: add a sibling hidden div as a pop-up on click -->
    <!-- .custom-popup class added to the parent gmaps infoView for custom styles -->
    <input class="address-marker" data-map-pin="assets/images/for-rent-pin-big.png" type="hidden" value="Ditmars Steinways">
    <div class="hide">
      <a href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
      <div class="options">
        <div class="image">
          <img src="assets/images/agent.png" class="img-responsive" alt="">
        </div>
        <div class="phone">
          <a href="tel:#"><i class="fa fa-phone" aria-hidden="true"></i></a>
        </div>
        <div class="email">
          <a href="mailto:#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
        </div>
      </div>
      <div class="top">
        <img src="assets/images/map-modal.png" class="img-responsive center-block" alt="">
      </div>
      <div class="bottom">
        <p class="address">157 West 57th Street, 83</p>
        <p class="price"><b>$ 7,999,000</b></p>
      </div>
    </div>

    <input class="address-marker" data-map-pin="assets/images/sold-pin-big.png" type="hidden" value="Bayside">
    <div class="hide">
      <a href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
      <div class="options">
        <div class="image">
          <img src="assets/images/agent.png" class="img-responsive" alt="">
        </div>
        <div class="phone">
          <a href="tel:#"><i class="fa fa-phone" aria-hidden="true"></i></a>
        </div>
        <div class="email">
          <a href="mailto:#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
        </div>
      </div>
      <div class="top">
        <img src="assets/images/map-modal.png" class="img-responsive center-block" alt="">
      </div>
      <div class="bottom">
        <p class="address">157 West 57th Street, 83</p>
        <p class="price"><b>$ 7,999,000</b></p>
      </div>
    </div>

    <input class="address-marker" data-map-pin="assets/images/for-rent-pin-big.png" type="hidden" value="North Corona">
    <div class="hide">
      <a href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
      <div class="options">
        <div class="image">
          <img src="assets/images/agent.png" class="img-responsive" alt="">
        </div>
        <div class="phone">
          <a href="tel:#"><i class="fa fa-phone" aria-hidden="true"></i></a>
        </div>
        <div class="email">
          <a href="mailto:#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
        </div>
      </div>
      <div class="top">
        <img src="assets/images/map-modal.png" class="img-responsive center-block" alt="">
      </div>
      <div class="bottom">
        <p class="address">157 West 57th Street, 83</p>
        <p class="price"><b>$ 7,999,000</b></p>
      </div>
    </div>

    <input class="address-marker" data-map-pin="assets/images/sold-pin-big.png" type="hidden" value="7202 Astoria Blvd
    East Elmhurst, NY 11370">
    <div class="hide">
      <a href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
      <div class="options">
        <div class="image">
          <img src="assets/images/agent.png" class="img-responsive" alt="">
        </div>
        <div class="phone">
          <a href="tel:#"><i class="fa fa-phone" aria-hidden="true"></i></a>
        </div>
        <div class="email">
          <a href="mailto:#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
        </div>
      </div>
      <div class="top">
        <img src="assets/images/map-modal.png" class="img-responsive center-block" alt="">
      </div>
      <div class="bottom">
        <p class="address">157 West 57th Street, 83</p>
        <p class="price"><b>$ 7,999,000</b></p>
      </div>
    </div>

    <input class="address-marker" data-map-pin="assets/images/for-sale-pin-big.png" type="hidden" value="Crocherona Avenue">
    <div class="hide">
      <a href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
      <div class="options">
        <div class="image">
          <img src="assets/images/agent.png" class="img-responsive" alt="">
        </div>
        <div class="phone">
          <a href="tel:#"><i class="fa fa-phone" aria-hidden="true"></i></a>
        </div>
        <div class="email">
          <a href="mailto:#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
        </div>
      </div>
      <div class="top">
        <img src="assets/images/map-modal.png" class="img-responsive center-block" alt="">
      </div>
      <div class="bottom">
        <p class="address">157 West 57th Street, 83</p>
        <p class="price"><b>$ 7,999,000</b></p>
      </div>
    </div>

    <input class="address-marker" data-map-pin="assets/images/for-sale-pin-big.png" type="hidden" value="Jackson Heights">
    <div class="hide">
      <a href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
      <div class="options">
        <div class="image">
          <img src="assets/images/agent.png" class="img-responsive" alt="">
        </div>
        <div class="phone">
          <a href="tel:#"><i class="fa fa-phone" aria-hidden="true"></i></a>
        </div>
        <div class="email">
          <a href="mailto:#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
        </div>
      </div>
      <div class="top">
        <img src="assets/images/map-modal.png" class="img-responsive center-block" alt="">
      </div>
      <div class="bottom">
        <p class="address">157 West 57th Street, 83</p>
        <p class="price"><b>$ 7,999,000</b></p>
      </div>
    </div>

    <input class="address-marker" data-map-pin="assets/images/for-sale-pin-big.png" center type="hidden" value="Flushing">
    <div class="hide">
      <a href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
      <div class="options">
        <div class="image">
          <img src="assets/images/agent.png" class="img-responsive" alt="">
        </div>
        <div class="phone">
          <a href="tel:#"><i class="fa fa-phone" aria-hidden="true"></i></a>
        </div>
        <div class="email">
          <a href="mailto:#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
        </div>
      </div>
      <div class="top">
        <img src="assets/images/map-modal.png" class="img-responsive center-block" alt="">
      </div>
      <div class="bottom">
        <p class="address">157 West 57th Street, 83</p>
        <p class="price"><b>$ 7,999,000</b></p>
      </div>
    </div>

    <input class="address-marker" data-map-pin="assets/images/sold-pin-big.png" type="hidden" value="Bay Terrace">
    <div class="hide">
      <a href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
      <div class="options">
        <div class="image">
          <img src="assets/images/agent.png" class="img-responsive" alt="">
        </div>
        <div class="phone">
          <a href="tel:#"><i class="fa fa-phone" aria-hidden="true"></i></a>
        </div>
        <div class="email">
          <a href="mailto:#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
        </div>
      </div>
      <div class="top">
        <img src="assets/images/map-modal.png" class="img-responsive center-block" alt="">
      </div>
      <div class="bottom">
        <p class="address">157 West 57th Street, 83</p>
        <p class="price"><b>$ 7,999,000</b></p>
      </div>
    </div>

    <input class="address-marker" data-map-pin="assets/images/sold-pin-big.png" type="hidden" value="East Elmhurst">
    <div class="hide">
      <a href="javascript:void(0)"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
      <div class="options">
        <div class="image">
          <img src="assets/images/agent.png" class="img-responsive" alt="">
        </div>
        <div class="phone">
          <a href="tel:#"><i class="fa fa-phone" aria-hidden="true"></i></a>
        </div>
        <div class="email">
          <a href="mailto:#"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
        </div>
      </div>
      <div class="top">
        <img src="assets/images/map-modal.png" class="img-responsive center-block" alt="">
      </div>
      <div class="bottom">
        <p class="address">157 West 57th Street, 83</p>
        <p class="price"><b>$ 7,999,000</b></p>
      </div>
    </div>
    <!-- map pins: END -->

    <!-- add the map container -->
    <div id="search-map"></div>
    <!-- full width map: END -->
    <?php
    }
    
    function displayContactMap() {
        ?>
<!-- location: START -->
<div class="acf-map mmap">
<?php
    global $redux_demo;
    ?>
	<div class="marker" data-lat="<?php echo $redux_demo['maps-latitude'] ; ?>" data-lng="<?php echo $redux_demo['maps-longitude'] ; ?>"></div>
</div>
<!-- location: END -->
        <?php
        
    }
    
    
}