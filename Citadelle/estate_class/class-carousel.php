<?php
class Carousel {
    
        function displayCarouselAgent($id){

//Get User Details
          $users = get_user_by('id',$id);
          $facebook = get_field('user_facebook', 'user_'.$users->ID );
          $twitter = get_field('user_twitter', 'user_'.$users->ID );
          $linkedin = get_field('user_linkedin', 'user_'.$users->ID );

          $avatar = get_avatar_url2($users->ID,196);
          $name = $users->display_name;
          $bio = get_the_author_meta('description',$users->ID);
          $splitBio = str_split($bio , 90);
          $email = $users->user_email;
          $phone = get_user_meta( $users->ID, 'billing_phone', true );
            ?>
    <div class="jcarousel-wrapper">
        <div class="jcarousel">
            <ul>
                <!-- slide item -->
                <li class="slide-1" style="background-image:url('<?php echo get_field('article_header','option')['url'];?>');">
                    <div class="title">
                        <?php print_r($avatar);?>
                            <h2><b><?php echo $name;?></b></h2>
                            <p>
                                <b><?php echo $splitBio[0]."<br>".$splitBio[1]; ?> </b>
                            </p>
                            <div class="line"></div>
                            <div class="socials">
                                <a href="tel:<?php echo $phone ;?>">
                                    <i class="fa fa-phone" aria-hidden="true"></i>
                                    <?php echo $phone ;?>
                                </a>
                                <a href="mailto:<?php echo $email ;?>">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    <?php echo $email ;?>
                                </a>
                                <br class="mobile-show"> Follow me on:
                                <a href="<?php if($linkedin){
            echo $linkedin;
        }else{
            echo " # ";
        } ;?>">
                                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                                </a>
                                <a href="<?php if($facebook){
            echo $facebook;
        }else{
            echo " # ";
        } ;?>">
                                    <i class="fa fa-facebook" aria-hidden="true"></i>
                                </a>
                                <a href="<?php if($twitter){
            echo $twitter;
        }else{
            echo " # ";
        } ;?>">
                                    <i class="fa fa-twitter" aria-hidden="true"></i>
                                </a>
                            </div>
                            <a href="#" data-toggle="modal" data-target="#agent-form-modal" class="btn-secondary">CONTACT AGENT</a>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <!-- Modal for contact-agent form: START -->
    <div class="modal fade" id="agent-form-modal">
        <form action="" id="agent-form" class="form-agent">
            <div class="agent-form">
                <div class="row">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                    <div class="col-xs-5">
                        <?php print_r($avatar);?>
                    </div>
                    <div class="col-xs-7">
                        <h3><b><?php echo $name;?></b></h3>
                        <div class="line"></div>
                        <p>
                            <a href="mailto:#">
                                <?php echo $email;?>
                            </a>
                        </p>
                        <p>
                            <a href="tel:<?php echo $phone;?>">
                                <?php echo $phone;?>
                            </a>
                        </p>
                    </div>
                </div>
                <input type="text" id="name" placeholder="Your name *" name="name">
                <input type="text" id="phone" placeholder="Phone number *" name="phone">
                <input type="text" id="email" placeholder="E-mail*" name="email">
                <textarea name="" id="" cols="30" rows="5" placeholder="Write your message here*"></textarea>
            </div>
            <button type="submit" class="btn-secondary">CONTACT AGENT</button>
        </form>
    </div>
    <?php
        }
    
    
    
        function displayCarousel()
        {
        ?>

        <?php if( have_rows('slider_carousel') ): ?>
            <div class="jcarousel-wrapper">
                <div class="jcarousel">
                    <ul>
                        <?php $i = 0; ?>
                            <?php while( have_rows('slider_carousel') ): the_row(); 
		$imageBackground = get_sub_field('slider_image');
		$sliderTitle = get_sub_field('carousel_title_heading');
        $buttonLink = get_sub_field('slider_button_link');
		?>

                                <?php if($imageBackground != ""):?>
                                    <li class="slide-1 <?php if($i==0 || $i==1){echo " active ";}?>" style=" background-image: url('<?php echo esc_url($imageBackground['url']); ?>');">
                                        <?php endif;?>
                                            <!-- slide content: START -->
                                            <div class="header-slide-text">
                                                <h2 class="text-center"><b><?php echo $sliderTitle; ?></b></h2>
                                                <?php if( have_rows('carousel_description') ): ?>
                                                    <p>
                                                        <?php while( have_rows('carousel_description') ): the_row(); 

		// vars
		$line = get_sub_field('description_line');
		
                echo $line."</br>";
		?>

                                                            <?php endwhile; ?>
                                                    </p>
                                                    <?php endif; ?>
                                                        <a href="<?php echo esc_url($buttonLink); ?>" class="btn-secondary">
                                                            <?php _e('SEE THIS OFFER', 'real-estate'); ?>
                                                        </a>
                                            </div>
                                            <!-- slide content: END -->
                                    </li>
                                    <?php $i++; ?>
                                        <?php endwhile; ?>
                    </ul>
                </div>

                <?php endif; ?>

                    <!-- slider controls -->
                    <div class="mobile-show">
                        <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                        <a href="#" class="jcarousel-control-next">&rsaquo;</a>
                    </div>
                    <!-- slider pagination -->
                    <p class="jcarousel-pagination"></p>
            </div>
            <?php
        }
    
     function displayCarouselGallery($image) 
    {
        ?>
                <div class="jcarousel-wrapper">
                    <div class="jcarousel">
                        <ul>
                            <!-- slide item -->
                            <li class="slide-1" style="background-image:url('<?php echo $image['url']; ?>')">

                            </li>
                        </ul>
                    </div>
                </div>
                <?php
        
    }
    
    function displayCarouselArticle($image,$icon,$description)
    {
        ?>

                    <div class="jcarousel-wrapper">
                        <div class="jcarousel">
                            <ul>
                                <!-- slide item -->
                                <li class="slide-1 active" style="background-image:url('<?php echo $image['url']; ?>')">
                                    <!-- slide content: START -->
                                    <div class="header-slide-text title">
                                        <i class="fa <?php echo $icon; ?>" aria-hidden="true"></i>
                                        <h2 class="text-center"><b><?php echo the_title(); ?></b></h2>
                                        <p>
                                            <b><?php echo $description; ?></b>
                                        </p>
                                        <div class="line"></div>
                                    </div>
                                    <!-- slide content: END -->
                                </li>

                            </ul>
                        </div>
                    </div>

                    <?php
    }
            function displayCarouselCommercial($image,$subtitle,$title)
            {
                ?>
                        <div class="jcarousel-wrapper">
                            <div class="jcarousel">
                                <ul>
                                    <!-- slide item -->
                                    <li class="slide-1" style="background-image:url('<?php echo $image['url']?>')">
                                        <div class="title">
                                            <h2><b><?php echo $title; ?></b></h2>
                                            <p>
                                                <?php echo $subtitle;?>
                                            </p>
                                            <div class="line"></div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <?php
            }
    }