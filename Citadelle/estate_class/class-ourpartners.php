<?php

class OurPartners
{
    function displayOurPartners()
    {
    ?>
    <div class="container partners">
  <div class="row">
    <div class="col-xs-12 col-md-4">
      <h2><b><?php _e('Our Partners', 'real-estate'); ?></b></h2>
    </div>
        <?php 

$images = get_field('add_partners');
if( $images ):
			foreach( $images as $image ): ?>
        <div class="col-xs-6 col-sm-3 col-md-2">
      <img src="<?php echo $image['sizes']['large']; ?>" class="img-responsive partner-img" alt="">
    </div>
<?php endforeach;
			endif; ?>
  </div>
</div>
    <?php
    }
}