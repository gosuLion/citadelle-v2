<?php

class Testimonials {

    function displayHeader($image,$imageTestimonial,$nameTestimonial,$positionTestimonial,$descriptionTestimonial)
    {
    ?>
    
        <div class="jcarousel-wrapper">
      <div class="jcarousel">
        <ul>
<!-- slide item -->
          <li class="slide-1" style="background-image:url('<?php echo $image['url']; ?>')">
            <div class="title">
              <div class="round">
                <img src="<?php echo $imageTestimonial['url']; ?>" class="" alt="">
              </div>
              <h2><b><?php echo $nameTestimonial; ?></b></h2>
              <p>
                <?php echo $positionTestimonial; ?>
              </p>
              <i class="sprite-load sprite-quote"></i>
              <p>
                <b><?php echo $descriptionTestimonial; ?></b>
                </p>
                <div class="line"></div>
              </div>
            </li>
          </ul>
        </div>
      </div>
    
    <?php
    }
    
    function displayTestimonials(){
			
     if( have_rows('add_testimonials') ): ?>

                <div class="testimonials-content clearfix">
      <div class="container">

	<?php while( have_rows('add_testimonials') ): the_row(); 

		// vars
		$image = get_sub_field('testimonial_image');
		$name = get_sub_field('testimonial_name');
		$position = get_sub_field('testimonial_name_position');
		$description= get_sub_field('testimonial_description');
		?>
		
		
        <div class="row"> <!-- testimonial item: START -->
          <div class="col-xs-12 col-md-2">
            <div class="round">
              <img src="<?php echo $image['url']; ?>" class="" alt="">
            </div>
            <i class="sprite-load sprite-quote"></i>
            <p class="date">
              
            </p>
          </div>
          <div class="col-xs-12 col-md-10">
            <div class="testimonial-text">
              <h3><b><?php echo $name;?></b></h3>
              <p class="occupation">
                <?php echo $position; ?>
              </p>
              <p class="italic">
               <?php echo $description; ?>
              </p>
            </div>
          </div>
        </div> <!-- testimonial item: END -->


	<?php endwhile; ?>
 </div>
    </div>
<?php endif;
    }
}