<?php

    class ListingsPage {
        
        
//define Variables
        
      public $numberOfProducts;
      public $bathroomName;
      public $bedroomName;
      public $rentName;
      public $saleName;
      public $soldName;
        
        public function __construct()
        {
      global $redux_demo;
					
//Get default variables from Theme options
					
      $this->bathroomName = $redux_demo['product-bathroom']; 
      $this->bedroomName  = $redux_demo['product-bedrooms']; 
      $this->rentName     = $redux_demo['product-rent'];
      $this->saleName     = $redux_demo['product-sale'];
      $this->soldName     = $redux_demo['product-sold'];
      $this->numberOfProducts = $redux_demo['number-of-products'];
        }
        
       function displayListings($id)
    {
 
// Set the arguments for getting the products
       
            $params = array(
                'post_per_page' => '1',
                'author' => $id,
                'post_type' => 'product'
            );
      
// Get the last X added products and form the loop
           $loop = new WP_Query($params);
				 
//Define a custom $i variable
				 
                    $i = 1;
        
//Insert the product ID to the database based on user ID.
        
        if(isset($_POST['favorite'])){
        global $wpdb;
           $prodid = $_POST['prodid']; 
        $wpdb->insert($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
        
//Delete the product id from the database
				 
        if(isset($_POST['deletefavorite'])){
            global $wpdb;
            $prodid = $_POST['prodid']; 
            $wpdb->delete($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
        
        
                    
    while ( $loop->have_posts() && $i <=  $this->numberOfProducts ) : $loop->the_post(); 
    global $product; 
				 
// Set the variables that will be used to display the product
    
//Get image URL
				 
     $attachment_ids[0] = get_post_thumbnail_id( $product->id );
     $attachment = wp_get_attachment_image_src($attachment_ids[0], 'full' );
    
//Get and split the short description to 190 characters
				 
    $shortDescription = str_split(get_the_excerpt(),190);
    
//Get the currency
				 
    $currency = $_SESSION['currency'];
    $numberOfDecimals = 0;
				 
//Get regular price
				 
    $price = get_post_meta( get_the_ID(), '_regular_price', true);
    
//Get sale price
				 
    $salePrice = get_post_meta( get_the_ID(), '_sale_price', true);
				 
//Product SKU
				 
    $sku = $product->get_sku();
    
//Product Attributes
				 
    $bathrooms = $product->get_attribute( $this->bathroomName);
    $bedrooms = $product->get_attribute($this->bedroomName);
    $rent = $product->get_attribute($this->rentName);
    $sale = $product->get_attribute($this->saleName);
    $sold = $product->get_attribute($this->soldName);
        
//Get image alt by url
				 
    $imageAlt =  wp_prepare_attachment_for_js($attachment_ids[0]); 
        ?>

	<div class="col-xs-12 result-item list">
		<!-- item -->
		<div class="col-xs-12 col-md-3 left">
			<img src="<?php echo $attachment[0]; ?>" class="img-responsive" alt="<?php echo $imageAlt['alt']; ?>">
			<div class="ribbon <?php 
                    if($rent){
                        echo " rent ";
                        $ribbon = __('For Rent', 'real-estate');
                    }
                else if($sold){
                    echo "sold ";
                    $ribbon = __('Sold', 'real-estate');
                }
                else{
                    echo "sale ";
                    $ribbon = __('Sale', 'real-estate');
                } ?>
                ">
				<?php echo $ribbon; ?>
			</div>
			<!-- START favorites icon -->

			<?php if(get_field('display_favorites', 'option')):
                      
// Get products and display if in favorites or not
				 
                     global $wpdb;
        $table = $wpdb->prefix . 'citadelle_favorites'; 
        $user = $this->UserId;
        $result = $wpdb->get_results( "SELECT * FROM $table WHERE userid = '".$user."' " );
        for ($a = 0; $a < count($result) ; $a++){
        if($result[$a]->productid == get_the_ID())
        {
            
            $ico = "animated favorite bounceIn";
            $subname = "deletefavorite";
          
                                            
        }
        																				}
        
                    ?>
				<form method="post">
					<input type="hidden" name="prodid" value="<?php echo get_the_ID(); ?>">
					<button style="background:transparent" name="<?php if($subname != " 0 "){echo $subname;}else{echo "favorite ";}?>" type="submit" class="fa fa-heart-o <?php if($ico != " 0 ") {
                    echo $ico;
                } ?>" aria-hidden="true"></button>
				</form>

				<?php
            $ico = 0;
            $subname = 0;
        
        ?>

					<?php endif;?>

						<!-- END favorites icon -->
		</div>
		<div class="col-xs-12 col-md-9 right">
			<div class="pad">
				<h4>
                  <b>
                    <a href="<?php echo get_permalink() ; ?>"><b><?php echo get_the_title(); ?></b></a>
                  </b>
                </h4>
				<p>
					<?php echo $shortDescription[0]; ?><a href="<?php echo get_permalink() ; ?>"> [...]</a>
				</p>
			</div>
			<div class="specs-ribbon clearfix">
				<div class="col-xs-4">
					<span class="sprite-load ico-bedroom"></span>
					<br class="mobile-show">
					<?php echo $bedrooms; ?> bedrooms
				</div>
				<div class="col-xs-4">
					<span class="sprite-load ico-bathroom"></span>
					<br class="mobile-show">
					<?php echo $bathrooms; ?> bathrooms
				</div>
			</div>
			<div class="pad clearfix">
				<div class="pull-left id">
					ID:
					<?php echo $sku ; ?>
				</div>
				<div class="pull-right price">
					<b><?php echo $currency ; ?> <?php if($salePrice){
                        echo number_format($salePrice, $numberOfDecimals,".",",");
                    }
                      else {
                      echo number_format($price, $numberOfDecimals,".",",");
                      }
                      ?></b>
				</div>
			</div>
		</div>
	</div>
	<?php
				 
//Increment the $i value
				 
    $i++;
    endwhile; 


    wp_reset_query(); 
    }
    
    }