<?php

class SearchModule 
{
    public $neighbour;
    public $surface;
    public $propertyType;
    public $bedrooms;
    public $bathrooms;
    public $rent;
    public $sale;
    public $sold;
    
    
    public function __construct(){
//Get variables to fill in the search form
        global $redux_demo;
        
        if($redux_demo['product-bathroom'] == "")
        {
            
        $this->neighbour = "pa_neighborhood";
        $this->surface = "pa_area";
        $this->propertyType = "pa_property-type";
        $this->bedrooms = "pa_bedrooms";
        $this->bathrooms = "pa_bathrooms";
        $this->rent = "pa_rent";
        $this->sold = "pa_sold";
        $this->sale = "pa_sale";
        $this->numberOfProducts = "6"; 
            
            
        }
        else {
            
        
        $this->neighbour = $redux_demo['product-neighbourhood'];
        $this->surface = $redux_demo['product-area'];
        $this->propertyType = $redux_demo['product-property'];
        $this->bedrooms = $redux_demo['product-bedrooms'];
        $this->bathrooms = $redux_demo['product-bathroom'];
        $this->rent = $redux_demo['product-rent'];
        $this->sold = $redux_demo['product-sold'];
        $this->sale = $redux_demo['product-sale'];
        $this->numberOfProducts = $redux_demo['number-of-products'];
        }
    }
    
    
    function displaySearchBySku($sku) {
			
        global $redux_demo;
        global $wpdb;
			
   $productSku = wc_get_product_id_by_sku( $sku );
   $userID = get_user_id_by_display_name($sku);
    $args = array(
    		'author' => $userID,
        'post_type' => 'product'
    						);
    $productsByUser = get_posts( $args );
  
//Insert the product ID to the database based on user ID.
        if(isset($_POST['favorite'])){
        global $wpdb;
        $prodid = $_POST['prodid']; 
        $wpdb->insert($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
        
//Delete the product id from the database
         if(isset($_POST['deletefavorite'])){
        global $wpdb;
        $prodid = $_POST['prodid']; 
        $wpdb->delete($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
?>
  
<!-- property listing: START -->
      <div class="content">
<?php
 
    if($productSku != ""){
            $posts = [];
            array_push($posts,$productSku);
    }
    else
    {
        $posts = [];
        
        for($i=0; $i < count($productsByUser); $i++)
        {
            array_push($posts,$productsByUser[$i]->ID);
        }
    }
            $params = array(
                'post_per_page' => '1',
                'post_type' => 'product',
                'post__in' => $posts
            );
        
           if($productSku == 0 && ! $userID){
            $params = array(
                'post_per_page' => '1',
                'post_type' => 'product',
                's' => $sku
            );
          
        }  
       
        
// Get the last X added products and form the loop
           $loop = new WP_Query($params);
			
//Define a custom $i variable
            $i = 1;
    
    while ( $loop->have_posts() && $i <= $this->numberOfProducts ) : $loop->the_post(); 
    global $product; 
    // Set the variables that will be used to display the product
    
//Get image URL
     $attachment_ids[0] = get_post_thumbnail_id( $product->id );
     $attachment = wp_get_attachment_image_src($attachment_ids[0], 'full' );
    
//Get and split the short description to 190 characters
    $shortDescription = str_split(get_the_excerpt(),190);
    
//Get the currency
    $currency = $_SESSION['currency'];
    $numberOfDecimals = 0;
			
//Get regular price
    $price = get_post_meta( get_the_ID(), '_regular_price', true);
    $price = $price  * $_SESSION['price'];
			
//Get sale price
    $salePrice = get_post_meta( get_the_ID(), '_sale_price', true);
    $salePrice = $salePrice  * $_SESSION['price'];
			
//Product SKU
    $sku = $product->get_sku();
    
//Product Attributes
    $bathroomsAtribute = $product->get_attribute($this->bathrooms);
    $bedroomsAttribute = $product->get_attribute($this->bedrooms);
    $rentAttribute = $product->get_attribute($this->rent);
    $saleAttribute = $product->get_attribute($this->sale);
    $soldAttribute = $product->get_attribute($this->sold);
    $neighbourAttribute = $product->get_attribute($this->neighbour);
    $areaSizeAttribute = $product->get_attribute($this->surface);
    global $post;

    $author_id=$post->post_author;
    $avatar = get_avatar_url2($author_id,64);
    $email = get_the_author_meta( 'email',$author_id);
    $phone = get_the_author_meta( 'billing_phone',$author_id);
    ?>                     

<!-- result-item: START -->
              <div class="col-xs-12 result-item list"> 
<!-- item -->
<!-- expand:START -->
                <div class="expand">
                  <a data-toggle="collapse" class="btn" data-target="#item-expand-<?php echo $i ; ?>">
                    <div class="triangle"></div>
                    <i class="fa fa-plus" aria-hidden="true"></i>
                  </a>
                  <div id="item-expand-<?php echo $i ; ?>" aria-expanded="false" class="collapse">
                    <div class="image">
                      <?php echo $avatar; ?>
                    </div>
                    <div class="phone">
                      <a href="tel:<?php echo $phone; ?>"><i class="fa fa-phone" aria-hidden="true"></i></a>
                    </div>
                    <div class="email">
                      <a href="mailto:<?php echo $email; ?>"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                    </div>
                  </div>
                </div>
<!-- expand: END -->
                <div class="col-xs-12 col-md-4 left">
                  <img src="<?php echo $attachment[0]; ?>" class="img-responsive" alt="">
                  <div class="ribbon <?php 
                    if($rentAttribute){
                        echo "rent";
                        $ribbon = _('For Rent' , 'real-estate');
                    }
                else if($soldAttribute){
                    echo "sold";
                    $ribbon = _('Sold' , 'real-estate');
                }
                else{
                    echo "sale";
                    $ribbon = _('For Sale' , 'real-estate');
                } ?>"><?php echo $ribbon; ?></div>
                    <!-- START favorites icon -->
                   <?php if(get_field('display_favorites', 'option')):
// Get products and display if in favorites or not
        global $wpdb;
        $table = $wpdb->prefix . 'citadelle_favorites'; 
        $user = $this->UserId;
        $result = $wpdb->get_results( "SELECT * FROM $table WHERE userid = '".$user."' " );
        for ($a = 0; $a < count($result) ; $a++){
        if($result[$a]->productid == get_the_ID())
        {
            
            $ico = "animated favorite bounceIn";
            $subname = "deletefavorite";
        }   
        }
									?>
              <form method="post">
              <input type="hidden" name="prodid" value="<?php echo get_the_ID(); ?>">
              <button style="background:transparent" name="<?php if($subname != "0"){echo $subname;}else{echo "favorite";}?>" type="submit" class="fa fa-heart-o <?php if($ico != "0") {
                    echo $ico;
                } ?>" aria-hidden="true"></button></form>
                 
            <?php
            $ico = 0;
            $subname = 0;
						endif;?>
<!-- END favorites icon -->
                </div>
                <div class="col-xs-12 col-md-8 right">
                  <div class="pad">
                   <a href="<?php echo get_permalink($product_id); ?>">
                    <h4>
                      <b>
                        <?php echo get_the_title(); ?>
                      </b>
                    </h4>
                      </a>
                    <p>
                     <?php echo $shortDescription[0]; ?>
                    </p>
                  </div>
                  <div class="specs-ribbon clearfix">
                    <div class="col-xs-4">
                      <span class="sprite-load ico-bedroom"></span> <br class="mobile-show">
                      <?php echo $bedroomsAttribute; ?> <?php _e('bedrooms' , 'real-estate'); ?>
                    </div>
                    <div class="col-xs-4">
                      <span class="sprite-load ico-bathroom"></span> <br class="mobile-show">
                      <?php echo $bathroomsAtribute; ?> <?php _e('bathrooms' , 'real-estate'); ?>
                    </div>
                    <div class="col-xs-4">
                      <span class="sprite-load ico-parking"></span> <br class="mobile-show">
                      <?php echo get_field('product_garage',$product->id);?> parking spaces
                    </div>
                  </div>
                  <div class="pad clearfix">
                    <div class="pull-left id">
                      <?php _e('ID:' , 'real-estate');?> <?php echo $sku ; ?>
                    </div>
                    <div class="pull-right price">
                      <b><?php echo $currency ; ?> <?php if($salePrice){
                        echo number_format($salePrice, $numberOfDecimals,".",",");
                    }
                      else {
                      echo number_format($price, $numberOfDecimals,".",",");
                      }
                      ?></b>
                    </div>
                  </div>
                </div>
              </div>
<!-- result-item: END -->
<!-- end 3 in a row -->
             
<?php
//Increment the $i value and reset the query
    $i++;
    endwhile; 
    wp_reset_query(); 
?>
    </div>
<!-- property listing: END -->
  <?php      
    }
    function displaySearchResults($neighbour,$type,$bedroom,$bathroom,$area,$price,$agentID=""){
    global $redux_demo;
			
//Get min and max area values
    $newArea = str_replace("sq ft","",$area);
    $arrayArea = explode(" ",$newArea);
    $minArea = $arrayArea[0];
    $maxArea = $arrayArea[1];

//GEt min and max price values
    $newPrice = str_replace("$","",$price);
    $arrayPrice = explode(" - ",$newPrice);
    
    $minPrice = $arrayPrice[0];
    $maxPrice = $arrayPrice[1];
    
        
     global $wpdb;
			
//Creating the sql for term_id
    $sql = "SELECT term_id FROM wp_terms WHERE name IN ('$neighbour','$type','$bedroom','$bathroom')";
    $myrows = $wpdb->get_results( $sql );
    $termIdString = "";
    $termIdString .= $myrows[0]->term_id.",";
    $termIdString .= $myrows[1]->term_id.",";
    $termIdString .= $myrows[2]->term_id.",";
    $termIdString .= $myrows[3]->term_id;
    
//Creating the sql for object_id
    $sql = "SELECT DISTINCT object_id FROM wp_term_relationships WHERE term_taxonomy_id IN($termIdString)";
    $myrows = $wpdb->get_results( $sql );
    $a = count($myrows);
    $productIDS =[];
    for ($i = 0 ; $i < count($myrows) ; $i++)
    {
           array_push($productIDS,$myrows[$i]->object_id);
        
    }  
        $this->productIDS = $productIDS;
        $this->minArea = $minArea;
        $this->maxArea = $maxArea;
        $this->minPrice = $minPrice;
        $this->maxPrice = $maxPrice;
    ?>
<!-- property listing: START -->
         <?php
//Insert the product ID to the database based on user ID.
        
        if(isset($_POST['favorite'])){
        global $wpdb;
           $prodid = $_POST['prodid']; 
        $wpdb->insert($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
        
//Delete the product id from the database
         if(isset($_POST['deletefavorite'])){
         global $wpdb;
         $prodid = $_POST['prodid']; 
         $wpdb->delete($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
 ?>
       <div class="content">
<?php
// Set the arguments for getting the products
        if($this->productIDS ==""){
            $params = array(
                'post_per_page' => '1',
                'post_type' => 'product'
            );
        }
            else
            {
                $params = array(
                'post_per_page' => '1',
                'post_type' => 'product',
                    'post__in' => $this->productIDS
            );
            }
    
// Get the last X added products and form the loop
           $loop = new WP_Query($params);
			
//Define a custom $i variable
                    $i = 1;
    
    while ( $loop->have_posts()) : $loop->the_post(); 
    global $product; 
// Set the variables that will be used to display the product
    
//Get image URL
     $attachment_ids[0] = get_post_thumbnail_id( $product->id );
     $attachment = wp_get_attachment_image_src($attachment_ids[0], 'full' );
    
//Get and split the short description to 190 characters
    $shortDescription = str_split(get_the_excerpt(),190);
    
//Get the currency
    $currency = $_SESSION['currency'];
    $numberOfDecimals = 0;
			
//Get regular price
    $price = get_post_meta( get_the_ID(), '_regular_price', true);
    $price = $price  * $_SESSION['price'];
			
//Get sale price
    $salePrice = get_post_meta( get_the_ID(), '_sale_price', true);
    $salePrice = $salePrice  * $_SESSION['price'];
			
//Product SKU
    $sku = $product->get_sku();
    
//Product Attributes
    $bathroomsAtribute = $product->get_attribute($this->bathrooms);
    $bedroomsAttribute = $product->get_attribute($this->bedrooms);
    $rentAttribute = $product->get_attribute($this->rent);
    $saleAttribute = $product->get_attribute($this->sale);
    $soldAttribute = $product->get_attribute($this->sold);
    $neighbourAttribute = $product->get_attribute($this->neighbour);
    $areaSizeAttribute = $product->get_attribute($this->surface);
    global $post;

    $author_id=$post->post_author;
    $avatar = get_avatar_url2($author_id,64);
    $email = get_the_author_meta( 'email',$author_id);
    $phone = get_the_author_meta( 'billing_phone',$author_id);
			
     if($neighbourAttribute == $neighbour):
     if($bathroomsAtribute == $bathroom):
     if($bedroomsAttribute == $bedroom):
     if($price >= $minPrice && $price <= $maxPrice || $salePrice != "" && $salePrice >= $minPrice && $salePrice <= $maxPrice):
     if($areaSizeAttribute >= $minArea && $areaSizeAttribute <= $maxArea):?>
                
<!-- result-item: START -->
              <div class="col-xs-12 result-item list"> <!-- item -->
<!-- expand:START -->
                <div class="expand">
                  <a data-toggle="collapse" class="btn" data-target="#item-expand-<?php echo $i ; ?>">
                    <div class="triangle"></div>
                    <i class="fa fa-plus" aria-hidden="true"></i>
                  </a>
                  <div id="item-expand-<?php echo $i ; ?>" aria-expanded="false" class="collapse">
                    <div class="image">
                      <?php echo $avatar; ?>
                    </div>
                    <div class="phone">
                      <a href="tel:<?php echo $phone; ?>"><i class="fa fa-phone" aria-hidden="true"></i></a>
                    </div>
                    <div class="email">
                      <a href="mailto:<?php echo $email; ?>"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                    </div>
                  </div>
                </div>
<!-- expand: END -->
                <div class="col-xs-12 col-md-4 left">
                  <img src="<?php echo $attachment[0]; ?>" class="img-responsive" alt="">
                  <div class="ribbon <?php 
                    if($rentAttribute){
                        echo "rent";
                        $ribbon = _('For Rent' , 'real-estate');
                    }
                else if($soldAttribute){
                    echo "sold";
                    $ribbon = _('Sold' , 'real-estate');
                }
                else{
                    echo "sale";
                    $ribbon = _('For Sale' , 'real-estate');
                } ?>"><?php echo $ribbon; ?></div>
<!-- START favorites icon -->
                   <?php if(get_field('display_favorites', 'option')):
			
// Get products and display if in favorites or not
        global $wpdb;
        $table = $wpdb->prefix . 'citadelle_favorites'; 
        $user = $this->UserId;
        $result = $wpdb->get_results( "SELECT * FROM $table WHERE userid = '".$user."' " );
        for ($a = 0; $a < count($result) ; $a++){
        if($result[$a]->productid == get_the_ID())
        {
            $ico = "animated favorite bounceIn";
            $subname = "deletefavorite";
        }  
        }
?>
              <form method="post">
              <input type="hidden" name="prodid" value="<?php echo get_the_ID(); ?>">
              <button style="background:transparent" name="<?php if($subname != "0"){echo $subname;}else{echo "favorite";}?>" type="submit" class="fa fa-heart-o <?php if($ico != "0") {
                    echo $ico;
                } ?>" aria-hidden="true"></button></form>
                 
            <?php
            $ico = 0;
            $subname = 0;
						endif;?>
                    
<!-- END favorites icon -->
                </div>
                <div class="col-xs-12 col-md-8 right">
                  <div class="pad">
                   <a href="<?php echo get_permalink($product_id); ?>">
                    <h4>
                      <b>
                        <?php echo get_the_title(); ?>
                      </b>
                    </h4>
                      </a>
                    <p>
                     <?php echo $shortDescription[0]; ?>
                    </p>
                  </div>
                  <div class="specs-ribbon clearfix">
                    <div class="col-xs-4">
                      <span class="sprite-load ico-bedroom"></span> <br class="mobile-show">
                      <?php echo $bedroomsAttribute; ?> <?php _e('bedrooms' , 'real-estate'); ?>
                    </div>
                    <div class="col-xs-4">
                      <span class="sprite-load ico-bathroom"></span> <br class="mobile-show">
                      <?php echo $bathroomsAtribute; ?> <?php _e('bathrooms' , 'real-estate'); ?>
                    </div>
                    <div class="col-xs-4">
                      <span class="sprite-load ico-parking"></span> <br class="mobile-show">
                     <?php echo get_field('product_garage',$product->id);?> parking spaces
                    </div>
                  </div>
                  <div class="pad clearfix">
                    <div class="pull-left id">
                      <?php _e('ID:' , 'real-estate');?> <?php echo $sku ; ?>
                    </div>
                    <div class="pull-right price">
                      <b><?php echo $currency ; ?> <?php if($salePrice){
                        echo number_format($salePrice, $numberOfDecimals,".",",");
                    }
                      else {
                      echo number_format($price, $numberOfDecimals,".",",");
                      }
                      ?></b>
                    </div>
                  </div>
                </div>
              </div>
<!-- result-item: END -->

<!-- end 3 in a row -->
              <?php
			endif;
			endif; 
      endif;
      endif;
      endif;
			
//Increment the $i value and reset the query
    $i++;
    endwhile; 
    wp_reset_query(); 
            ?>
        

    

    </div>
  
<!-- property listing: END -->
  <?php
     }
        
        
   function displaySearchForm(){
       
    global $wpdb;
		 
//Get terms from attributes
            $neighbourhoods = get_terms($this->neighbour);
            $area = get_terms($this->surface);
            $property = get_terms($this->propertyType);
            $bedroom = get_terms($this->bedrooms);
            $bathroom = get_terms($this->bathrooms);

//Form a new array with values from surface
            $arrayArea[]="";
            for($i=0 ; $i < count($area) ; $i++){
                array_push($arrayArea,$area[$i]->name);

            }
//Set the min and max values of Surface and Round them
                $maxArea = round(max($arrayArea));
                $minArea = floor(min($arrayArea));
                $_SESSION['minAreaFinal'] = $minArea;
                $_SESSION['maxAreaFinal'] = $maxArea;
         
//Get price from database
            $results = $wpdb->get_results("SELECT `meta_value` FROM `wp_postmeta` WHERE `meta_key` = '_price'");
            
//Set the min and max values for Price
            $maxPrice = max($results);
            $minPrice = min($results);   
            
//Final price
            $maxFinalPrice = round($maxPrice->meta_value);
            $minFinalPrice = floor($minPrice->meta_value);
       
            $_SESSION['maxPriceFinal'] = $maxFinalPrice;
            $_SESSION['minPriceFinal'] = $minFinalPrice;
    
                ?>
      <div class="container advanced-search">
         <form method="get" action="index.php/search-results">
        <div class="col-xs-6 col-md-3">
          
<!-- location selector: START -->
          <select name="neighbour" class="selectpicker">
            
            <?php
//form the loop to cycle from 0
                
             for($i=0 ; $i<count($neighbourhoods) ; $i++ ) :?>
            <option value="<?php echo $neighbourhoods[$i]->name; ?>"><?php echo $neighbourhoods[$i]->name; ?></option>
            <?php 
                endfor;
                     ?>
          </select>
<!-- location selector: END -->

<!-- size slider range: START -->
          <input type="hidden" name="minAptSize" id="minAptSize" value="<?php echo $_SESSION['minAreaFinal']; ?>">
          <input type="hidden" name="maxAptSize" id="maxAptSize" value="<?php echo $_SESSION['maxAreaFinal']; ?>">
          
          <input type="hidden" name="minAptCost" id="maxPriceFinal" value="<?php echo $_SESSION['maxPriceFinal']; ?>">
          <input type="hidden" name="maxAptCost" id="minPriceFinal" value="<?php echo $_SESSION['minPriceFinal']; ?>">
          
          <input name="badge" type="hidden">
          <input name="area" type="hidden" id="size-amount"  style="border:0; color:#f6931f; font-weight:bold;">
          <div id="size-slider-range">
            <div class="text"></div>
            <div class="text last"></div>
          </div>
          <div class="pull-left">min.</div>
          <div class="pull-right">max.</div>
<!-- size slider range: END -->

        </div>


        <div class="col-xs-6 col-md-3">

<!-- type selector: START -->
          <select name="property" class="selectpicker">
            
            <?php for($i = 0; $i < count($property); $i++):?>
            <option value="<?php echo $property[$i]->name; ?>"><?php echo $property[$i]->name; ?></option>
                <?php endfor; ?>
          </select>
<!-- type selector: END -->

<!-- price slider range: START -->
          <input name="price" type="hidden" id="price-amount"  style="border:0; color:#f6931f; font-weight:bold;">
          <div id="price-slider-range">
            <div class="text"></div>
            <div class="text last"></div>
          </div>
          <div class="pull-left">min.</div>
          <div class="pull-right">max.</div>
<!-- size slider range: END -->

        </div>


        <div class="col-xs-12 col-md-4">
          <div class="row">
            <div class="col-xs-6">
<!-- bedroom selector: START -->
              <select name="bedroom" class="selectpicker">
               <?php
                for($i = 0 ; $i < count($bedroom); $i++) :?>
                
                <option value="<?php echo $bedroom[$i]->name; ?>"><?php echo $bedroom[$i]->name; ?> <?php if($bedroom[$i]->name == 1){
                    echo "bedroom";
                }
                    else{
                        echo "bedrooms";
                    }
                    
                    ?></option>
                
                <?php endfor; ?>
              </select>
<!-- bedroom selector: END -->
            </div>

            <div class="col-xs-6">
<!-- bathroom selector: START -->
              <select name="bathroom" class="selectpicker">
              <?php for($i = 0; $i < count($bathroom) ; $i ++): ?>
    					<option value="<?php echo $bathroom[$i]->name; ?>">
        <?php echo $bathroom[$i]->name; ?> <?php if($bathroom[$i]->name == 1){
                        echo "bathroom" ;
                    }
    else { 
        echo "bathrooms"; 
    } ?>
    </option>

    <?php endfor; ?>
              </select>
<!-- bathroom selector: END -->
            </div>
            <div class="col-xs-12">
              <input type="text" name="agent" id="agent" placeholder="ID or agent name">
            </div>
          </div>
        </div>
        <div class="col-xs-12 col-md-2">
          <button type="submit" id="advanced-submit" class="advanced-submit">
            <i class="fa fa-search" aria-hidden="true"></i> <br>
            <?php _e('SEARCH' , 'real-estate') ; ?>
          </button>
        </div>
         </form>
      </div>
     


<?php
}
    
    function displayMapByProduct($neighbour,$type,$bedroom,$bathroom,$area,$price,$badge="",$sku=""){
        //Get min and max area values
    $locat=[];
      $newArea = str_replace("sq ft","",$area);
    $arrayArea = explode(" ",$newArea);
    
    $minArea = $arrayArea[0];
    $maxArea = $arrayArea[1];

    //GEt min and max price values
    $newPrice = str_replace("$","",$price);
    $arrayPrice = explode(" - ",$newPrice);
    
    $minPrice = $arrayPrice[0];
    $maxPrice = $arrayPrice[1];
        
        if($badge != "" || $badge == "" && $sku == ""){
         
             global $wpdb;
        
//Insert the product ID to the database based on user ID.
        
        if(isset($_POST['favorite'])){
        global $wpdb;
           $prodid = $_POST['prodid']; 
        $wpdb->insert($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
        
//Delete the product id from the database
         if(isset($_POST['deletefavorite'])){
        global $wpdb;
           $prodid = $_POST['prodid']; 
        $wpdb->delete($wpdb->prefix . 'citadelle_favorites',array('userid' => $this->UserId,'productid' => $prodid ));
        }
        
        
                    
//Creating the sql for term_id
    $sql = "SELECT term_id FROM wp_terms WHERE name IN ('$neighbour','$type','$bedroom','$bathroom')";
    $myrows = $wpdb->get_results( $sql );
    $termIdString = "";
    $termIdString .= $myrows[0]->term_id.",";
    $termIdString .= $myrows[1]->term_id.",";
    $termIdString .= $myrows[2]->term_id.",";
    $termIdString .= $myrows[3]->term_id;
    
//Creating the sql for object_id
    $sql = "SELECT DISTINCT object_id FROM wp_term_relationships WHERE term_taxonomy_id IN($termIdString)";
     $myrows = $wpdb->get_results( $sql );
    $a = count($myrows);
    $productIDS =[];
    for ($i = 0 ; $i < count($myrows) ; $i++)
    {
        
           array_push($productIDS,$myrows[$i]->object_id);
        
    }  
        $this->productIDS = $productIDS;
        $this->minArea = $minArea;
        $this->maxArea = $maxArea;
        $this->minPrice = $minPrice;
        $this->maxPrice = $maxPrice;
        if($this->productIDS ==""){
        $params = array(
                'post_per_page' => '1',
                'post_type' => 'product'
            );
        }
            else
            {
                $params = array(
                'post_per_page' => '1',
                'post_type' => 'product',
                    'post__in' => $this->productIDS
            );
        
            }
        }
         else if($sku != "")
        {
             
   $productSku = wc_get_product_id_by_sku( $sku );
   $userID = get_user_id_by_display_name($sku);
             
    $args = array(
    'author' => $userID,
        'post_type' => 'product'
    );
    
    $productsByUser = get_posts( $args );
          
    if($productSku != ""){
            $posts = [];
            array_push($posts,$productSku);
    }
    else
    {
        $posts = [];
        
        for($i=0; $i < count($productsByUser); $i++)
        {
            array_push($posts,$productsByUser[$i]->ID);
        }
    }
            $params = array(
                'post_per_page' => '1',
                'post_type' => 'product',
                'post__in' => $posts
            );
            
        }
        $text = $sku;
          if($productSku == 0 && !$userID && $text !=""){
            $params = array(
                'post_per_page' => '1',
                'post_type' => 'product',
                's' => $sku
            );
        
        }  
   
// Get the last X added products and form the loop
           $loop = new WP_Query($params);
//Define a custom $i variable
                    $i = 1;
    $total=0;
             while ( $loop->have_posts() ) : $loop->the_post(); 
    global $product; 
             $rentAttribute = $product->get_attribute($this->rent);
             $saleAttribute = $product->get_attribute($this->sale);
             $soldAttribute = $product->get_attribute($this->sold);
              $areaSizeAttribute = $product->get_attribute($this->surface);
            if($rentAttribute)
    {
        $badgevalue = "rent";
    }
     else if($saleAttribute)
     {
         $badgevalue = "sale";
     }
     else if($soldAttribute)
     {
         $badgevalue = "sold";
     }
       
        if($sku !=""){
            global $post;

    $author_id=$post->post_author;
 $name = get_the_author_meta( 'display_name',$author_id);
        }
        
            if($badge == $badgevalue && get_field('product_address')!= "" && $product->get_attribute($this->neighbour) == $neighbour && $product->get_attribute($this->bathrooms) == $bathroom && $product->get_attribute($this->bedrooms) == $bedroom && $areaSizeAttribute >= $minArea && $areaSizeAttribute <= $maxArea ){
                
                $pu=array('lat' => get_field('product_address')['lat'], 'lng'=> get_field('product_address')['lng'], 'prodid' => $product->id  );
               
                
                array_push($locat,$pu);
                $total++;
               
            }
        else if($product->get_sku() == $sku && $sku !="" || $name == $sku && get_field('product_address') != "" && $sku !="" ){
            
             $pu=array('lat' => get_field('product_address')['lat'], 'lng'=> get_field('product_address')['lng'], 'prodid' => $product->id);
                array_push($locat,$pu);
            $total++;
          
        }
      
        else if($badge == "" && $sku == "" && get_field('product_address')!= "" && $product->get_attribute($this->neighbour) == $neighbour && $product->get_attribute($this->bathrooms) == $bathroom && $product->get_attribute($this->bedrooms) == $bedroom && $areaSizeAttribute >= $minArea && $areaSizeAttribute <= $maxArea ){
               $pu=array('lat' => get_field('product_address')['lat'], 'lng'=> get_field('product_address')['lng'], 'prodid' => $product->id);
                array_push($locat,$pu);
          $total++;
            
        }
          else if($productSku == 0 && !$userID  && $text !="")
        {
            $pu=array('lat' => get_field('product_address')['lat'], 'lng'=> get_field('product_address')['lng'], 'prodid' => $product->id);
                array_push($locat,$pu);
            $total++;
        }
            endwhile;

        $i=0;
        ?>
        <div class="acf-map mmap">
      <?php  foreach($locat as $location):
			if ($locat[$i]['lat'] !=""):?>
      			<div class="marker" data-lat="<?php echo $locat[$i]['lat']; ?>" data-lng="<?php echo $locat[$i]['lng']; ?>">
                    <a href="<?php echo get_permalink( $locat[$i]['prodid']); ?>"><h4><?php echo get_the_title($locat[$i]['prodid']); ?></h4></a>
				<p class="address"><?php echo $locat[$i]['address']; ?></p>
            </div>
<?php 
		endif; 
    $i++;
?>
      <?php endforeach;
			wp_reset_query();?>
</div>
    <?php    
        $_SESSION['total'] = $total;
    }
    
 function displaySearchByBadge($neighbour,$type,$bedroom,$bathroom,$area,$price,$badge){
    
//Get min and max area values
    $newArea = str_replace("sq ft","",$area);
    $arrayArea = explode(" ",$newArea);
    $minArea = $arrayArea[0];
    $maxArea = $arrayArea[1];

     
//GEt min and max price values
    $newPrice = str_replace("$","",$price);
    $arrayPrice = explode(" - ",$newPrice);
  
    $minPrice = $arrayPrice[0];
    $maxPrice = $arrayPrice[1];
    
        
     global $wpdb;
//Creating the sql for term_id
    $sql = "SELECT term_id FROM wp_terms WHERE name IN ('$neighbour','$type','$bedroom','$bathroom')";
    $myrows = $wpdb->get_results( $sql );
    $termIdString = "";
    $termIdString .= $myrows[0]->term_id.",";
    $termIdString .= $myrows[1]->term_id.",";
    $termIdString .= $myrows[2]->term_id.",";
    $termIdString .= $myrows[3]->term_id;
    
//Creating the sql for object_id
    $sql = "SELECT DISTINCT object_id FROM wp_term_relationships WHERE term_taxonomy_id IN($termIdString)";
     $myrows = $wpdb->get_results( $sql );
    $a = count($myrows);
    $productIDS =[];
    for ($i = 0 ; $i < count($myrows) ; $i++)
    {
           array_push($productIDS,$myrows[$i]->object_id);
        
    }  
        $this->productIDS = $productIDS;
        $this->minArea = $minArea;
        $this->maxArea = $maxArea;
        $this->minPrice = $minPrice;
        $this->maxPrice = $maxPrice;
    ?>
<!-- property listing: START -->
     
       <div class="content">
<?php
     
    
// Set the arguments for getting the products
        if($this->productIDS ==""){
            $params = array(
                'post_per_page' => '1',
                'post_type' => 'product'
            );
        }
            else
            {
                $params = array(
                'post_per_page' => '1',
                'post_type' => 'product',
                    'post__in' => $this->productIDS
            );
            }
    
// Get the last X added products and form the loop
           $loop = new WP_Query($params);
//Define a custom $i variable
           $i = 1;
    
    while ( $loop->have_posts() ) : $loop->the_post(); 
    global $product; 
// Set the variables that will be used to display the product
    
//Get image URL
     $attachment_ids[0] = get_post_thumbnail_id( $product->id );
     $attachment = wp_get_attachment_image_src($attachment_ids[0], 'full' );
    
//Get and split the short description to 190 characters
    $shortDescription = str_split(get_the_excerpt(),190);
    
//Get the currency
    $currency = $_SESSION['currency'];
    $numberOfDecimals = 0;
	 
//Get regular price
    $price = get_post_meta( get_the_ID(), '_regular_price', true);
    $price = $price  * $_SESSION['price'];
	 
//Get sale price
    $salePrice = get_post_meta( get_the_ID(), '_sale_price', true);
     $salePrice = $salePrice  * $_SESSION['price'];
	 
//Product SKU
    $sku = $product->get_sku();
    
//Product Attributes
    $bathroomsAtribute = $product->get_attribute($this->bathrooms);
    $bedroomsAttribute = $product->get_attribute($this->bedrooms);
    $rentAttribute = $product->get_attribute($this->rent);
    $saleAttribute = $product->get_attribute($this->sale);
    $soldAttribute = $product->get_attribute($this->sold);
    $neighbourAttribute = $product->get_attribute($this->neighbour);
    $areaSizeAttribute = $product->get_attribute($this->surface);
    global $post;
    $author_id=$post->post_author;
    $avatar = get_avatar_url2($author_id,64);
    $email = get_the_author_meta( 'email',$author_id);
    $phone = get_the_author_meta( 'billing_phone',$author_id);
  
     
    if($rentAttribute)
    {
        $badgevalue = "rent";
    }
     else if($saleAttribute)
     {
         $badgevalue = "sale";
     }
     else if($soldAttribute)
     {
         $badgevalue = "sold";
     }

  if($neighbourAttribute == $neighbour):
  if($bathroomsAtribute == $bathroom):
  if($bedroomsAttribute == $bedroom):
  if($price >=  $this->minPrice && $price <=  $this->maxPrice || $salePrice != "" && $salePrice >=  $this->minPrice && $salePrice <= $this->maxPrice):
  if ($areaSizeAttribute >= $minArea && $areaSizeAttribute <= $maxArea):
  if($badge == $badgevalue ): ?>
    
<!-- result-item: START -->
              <div class="col-xs-12 result-item list"> <!-- item -->
<!-- expand:START -->
                <div class="expand">
                  <a data-toggle="collapse" class="btn" data-target="#item-expand-<?php echo $i ; ?>">
                    <div class="triangle"></div>
                    <i class="fa fa-plus" aria-hidden="true"></i>
                  </a>
                  <div id="item-expand-<?php echo $i ; ?>" aria-expanded="false" class="collapse">
                    <div class="image">
                      <?php echo $avatar; ?>
                    </div>
                    <div class="phone">
                      <a href="tel:<?php echo $phone; ?>"><i class="fa fa-phone" aria-hidden="true"></i></a>
                    </div>
                    <div class="email">
                      <a href="mailto:<?php echo $email; ?>"><i class="fa fa-envelope-o" aria-hidden="true"></i></a>
                    </div>
                  </div>
                </div>
<!-- expand: END -->
                <div class="col-xs-12 col-md-4 left">
                  <img src="<?php echo $attachment[0]; ?>" class="img-responsive" alt="">
                  <div class="ribbon <?php 
                    if($rentAttribute){
                        echo "rent";
                        $ribbon = _('For Rent' , 'real-estate');
                    }
                else if($soldAttribute){
                    echo "sold";
                    $ribbon = _('Sold' , 'real-estate');
                }
                else{
                    echo "sale";
                    $ribbon = _('For Sale' , 'real-estate');
                } ?>"><?php echo $ribbon; ?></div>
<!-- START favorites icon -->
                   <?php if(get_field('display_favorites', 'option')):?>
                        <?php
// Get products and display if in favorites or not
                        global $wpdb;
        $table = $wpdb->prefix . 'citadelle_favorites'; 
        $user = $this->UserId;
       $result = $wpdb->get_results( "SELECT * FROM $table WHERE userid = '".$user."' " );
        for ($a = 0; $a < count($result) ; $a++){
        if($result[$a]->productid == get_the_ID())
        {
            
            $ico = "animated favorite bounceIn";
            $subname = "deletefavorite";
        }  
        }
?>
                     <form method="post">
                        <input type="hidden" name="prodid" value="<?php echo get_the_ID(); ?>">
              <button style="background:transparent" name="<?php if($subname != "0"){echo $subname;}else{echo "favorite";}?>" type="submit" class="fa fa-heart-o <?php if($ico != "0") {
                    echo $ico;
                } ?>" aria-hidden="true"></button></form>
                 
            <?php
            $ico = 0;
            $subname = 0;
	 					endif;?>
                    
<!-- END favorites icon -->
                </div>
                <div class="col-xs-12 col-md-8 right">
                  <div class="pad">
                   <a href="<?php echo get_permalink($product_id); ?>">
                    <h4>
                      <b>
                        <?php echo get_the_title(); ?>
                      </b>
                    </h4>
                      </a>
                    <p>
                     <?php echo $shortDescription[0]; ?>
                    </p>
                  </div>
                  <div class="specs-ribbon clearfix">
                    <div class="col-xs-4">
                      <span class="sprite-load ico-bedroom"></span> <br class="mobile-show">
                      <?php echo $bedroomsAttribute; ?> <?php _e('bedrooms' , 'real-estate'); ?>
                    </div>
                    <div class="col-xs-4">
                      <span class="sprite-load ico-bathroom"></span> <br class="mobile-show">
                      <?php echo $bathroomsAtribute; ?> <?php _e('bathrooms' , 'real-estate'); ?>
                    </div>
                    <div class="col-xs-4">
                      <span class="sprite-load ico-parking"></span> <br class="mobile-show">
                    <?php echo get_field('product_garage',$product->id);?> parking spaces
                    </div>
                  </div>
                  <div class="pad clearfix">
                    <div class="pull-left id">
                      <?php _e('ID:' , 'real-estate');?> <?php echo $sku ; ?>
                    </div>
                    <div class="pull-right price">
                      <b><?php echo $currency ; ?> <?php if($salePrice){
                        echo number_format($salePrice, $numberOfDecimals,".",",");
                    }
                      else {
                      echo number_format($price, $numberOfDecimals,".",",");
                      }
                      ?></b>
                    </div>
                  </div>
                </div>
              </div>
<!-- result-item: END -->
<!-- end 3 in a row -->
<?php           
	 endif;    
	 endif; 
	 endif; 
	 endif; 
	 endif; 
	 endif; 

//Increment the $i value and reset query
    $i++;
    endwhile; 
    wp_reset_query(); 
?>

    </div>
<!-- property listing: END -->
 
  <?php
     }

    function displaySearchForm2() {
         global $wpdb;

//Get terms from attributes
            $neighbourhoods = get_terms($this->neighbour);
            $area = get_terms($this->surface);
            $property = get_terms($this->propertyType);
            $bedroom = get_terms($this->bedrooms);
            $bathroom = get_terms($this->bathrooms);

//Form a new array with values from surface
            $arrayArea[]="";
            for($i=0 ; $i < count($area) ; $i++){
                array_push($arrayArea,$area[$i]->name);

            }
//Set the min and max values of Surface and Round them
                $maxArea = round(max($arrayArea));
                $minArea = floor(min($arrayArea));
                $_SESSION['minAreaFinal'] = $minArea;
                $_SESSION['maxAreaFinal'] = $maxArea;
         
//Get price from database
            $results = $wpdb->get_results("SELECT `meta_value` FROM `wp_postmeta` WHERE `meta_key` = '_price'");
            
//Set the min and max values for Price
            $maxPrice = max($results);
            $minPrice = min($results);   

//Final price
            $maxFinalPrice = round($maxPrice->meta_value);
            $minFinalPrice = floor($minPrice->meta_value);
       
            $_SESSION['maxPriceFinal'] = $maxFinalPrice;
            $_SESSION['minPriceFinal'] = $minFinalPrice;
            ?>
                <form action="" id="find-properties" method="get">
          <div class="form-inner">
<!-- searchbox input: START -->
            <input type="text" name="agent" placeholder="Search by ID or agent name...">
            <button type="submit" id="search-button" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
<!-- searchbox input: END -->

<!-- selectors: START -->
            <select name="neighbour" class="selectpicker">
            
            <?php
//form the loop to cycle from 0
                
            for($i=0 ; $i<count($neighbourhoods) ; $i++ ) :?>
            <option value="<?php echo $neighbourhoods[$i]->name; ?>"><?php echo $neighbourhoods[$i]->name; ?></option>
            <?php 
                endfor;
                     ?>
            </select>

            <select name="property" class="selectpicker">
              <?php for($i = 0; $i < count($property); $i++):?>
            <option value="<?php echo $property[$i]->name; ?>"><?php echo $property[$i]->name; ?></option>
                <?php endfor; ?>
            </select>

            <select name="bedroom" class="selectpicker">
                 <?php
                for($i = 0 ; $i < count($bedroom); $i++) :?>
                
                <option value="<?php echo $bedroom[$i]->name; ?>"><?php echo $bedroom[$i]->name; ?> <?php if($bedroom[$i]->name == 1){
                    echo "bedroom";
                }
                    else{
                        echo "bedrooms";
                    }
                    
                    ?></option>
                
                <?php endfor; ?>
            </select>

  <select name="bathroom" class="selectpicker">
  <?php for($i = 0; $i < count($bathroom) ; $i ++): ?>
    <option value="<?php echo $bathroom[$i]->name; ?>">
        <?php echo $bathroom[$i]->name; ?> <?php if($bathroom[$i]->name == 1){
                        echo "bathroom" ;
                    }
    else { 
        echo "bathrooms"; 
    } ?>
    </option>

    <?php endfor; ?>
            </select>
<!-- selectors: END -->

<!-- expandable area for type-->
            <div class="search-expand">
              <a data-toggle="collapse" class="btn" data-target="#property-status">Property status <span class="caret pull-right"></span></a>

              <div id="property-status" class="collapse in">

                <div class="form-row mt-10">
                  <span class="input-label">For Sale</span>
                  <div class="styled-checkbox">
                    <input type="checkbox" value="sale" id="sale" name="badge" />
                    <label for="sale"></label>
                  </div>
                </div>

                <div class="form-row">
                  <span class="input-label">For Rent</span>
                  <div class="styled-checkbox">
                    <input type="checkbox" value="rent" id="rent" name="badge" />
                    <label for="rent"></label>
                  </div>
                </div>

                <div class="form-row">
                  <span class="input-label">Sold</span>
                  <div class="styled-checkbox">
                    <input type="checkbox" value="sold" id="sold" name="badge" />
                    <label for="sold"></label>
                  </div>
                </div>

              </div>
            </div>

<!-- expandable area for range-->
            <div class="search-expand">
              <a data-toggle="collapse" class="btn collapsed" data-target="#area-price">Area & price range<span class="caret pull-right"></span></a>

              <div id="area-price" class="collapse">

<!-- area slider range: START -->
                <div class="form-row">
                  <input type="hidden" id="size-amount" name="area"  style="border:0; color:#f6931f; font-weight:bold;">
                  <div id="size-slider-range">
                    <div class="text"></div>
                    <div class="text last"></div>
                  </div>
                  <div class="pull-left">min.</div>
                  <div class="pull-right">max.</div>
                </div>
<!-- area slider range: END -->

<!-- area slider range: START -->
                <div class="form-row">
                  <input type="hidden" name="price"  id="price-amount"  style="border:0; color:#f6931f; font-weight:bold;">
                  <div id="price-slider-range">
                    <div class="text"></div>
                    <div class="text last"></div>
                  </div>
                  <div class="pull-left">min.</div>
                  <div class="pull-right">max.</div>
                </div>
<!-- area slider range: END -->
              </div>
            </div>
<!-- expandable area for range-->
          </div>
          <button type="submit" id="submit" class="submit"><i class="fa fa-search animated pulse infinite" aria-hidden="true"></i>Apply filters</button>
          
        </form>
                  
        
    <?php    
    }
    
}