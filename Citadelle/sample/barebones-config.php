<?php

    /**
     * ReduxFramework Barebones Sample Config File
     * For full documentation, please visit: http://docs.reduxframework.com/
     */

    if ( ! class_exists( 'Redux' ) ) {
        return;
    }

    // This is your option name where all the Redux data is stored.
    $opt_name = "redux_demo";

    /**
     * ---> SET ARGUMENTS
     * All the possible arguments for Redux.
     * For full documentation on arguments, please refer to: https://github.com/ReduxFramework/ReduxFramework/wiki/Arguments
     * */

    $theme = wp_get_theme(); // For use with some settings. Not necessary.

    $args = array(
        // TYPICAL -> Change these values as you need/desire
        'opt_name'             => $opt_name,
        // This is where your data is stored in the database and also becomes your global variable name.
        'display_name'         => $theme->get( 'Name' ),
        // Name that appears at the top of your panel
        'display_version'      => $theme->get( 'Version' ),
        // Version that appears at the top of your panel
        'menu_type'            => 'menu',
        //Specify if the admin menu should appear or not. Options: menu or submenu (Under appearance only)
        'allow_sub_menu'       => true,
        // Show the sections below the admin menu item or not
        'menu_title'           => __( 'Theme Options', 'redux-framework-demo' ),
        'page_title'           => __( 'Theme options', 'redux-framework-demo' ),
        // You will need to generate a Google API key to use this feature.
        // Please visit: https://developers.google.com/fonts/docs/developer_api#Auth
        'google_api_key'       => '',
        // Set it you want google fonts to update weekly. A google_api_key value is required.
        'google_update_weekly' => false,
        // Must be defined to add google fonts to the typography module
        'async_typography'     => true,
        // Use a asynchronous font on the front end or font string
        //'disable_google_fonts_link' => true,                    // Disable this in case you want to create your own google fonts loader
        'admin_bar'            => true,
        // Show the panel pages on the admin bar
        'admin_bar_icon'       => 'dashicons-portfolio',
        // Choose an icon for the admin bar menu
        'admin_bar_priority'   => 50,
        // Choose an priority for the admin bar menu
        'global_variable'      => '',
        // Set a different name for your global variable other than the opt_name
        'dev_mode'             => true,
        // Show the time the page took to load, etc
        'update_notice'        => true,
        // If dev_mode is enabled, will notify developer of updated versions available in the GitHub Repo
        'customizer'           => true,
        // Enable basic customizer support
        //'open_expanded'     => true,                    // Allow you to start the panel in an expanded way initially.
        //'disable_save_warn' => true,                    // Disable the save warning when a user changes a field

        // OPTIONAL -> Give you extra features
        'page_priority'        => null,
        // Order where the menu appears in the admin area. If there is any conflict, something will not show. Warning.
        'page_parent'          => 'themes.php',
        // For a full list of options, visit: http://codex.wordpress.org/Function_Reference/add_submenu_page#Parameters
        'page_permissions'     => 'manage_options',
        // Permissions needed to access the options panel.
        'menu_icon'            => '',
        // Specify a custom URL to an icon
        'last_tab'             => '',
        // Force your panel to always open to a specific tab (by id)
        'page_icon'            => 'icon-themes',
        // Icon displayed in the admin panel next to your menu_title
        'page_slug'            => '_options',
        // Page slug used to denote the panel
        'save_defaults'        => true,
        // On load save the defaults to DB before user clicks save or not
        'default_show'         => false,
        // If true, shows the default value next to each field that is not the default value.
        'default_mark'         => '',
        // What to print by the field's title if the value shown is default. Suggested: *
        'show_import_export'   => true,
        // Shows the Import/Export panel when not used as a field.

        // CAREFUL -> These options are for advanced use only
        'transient_time'       => 60 * MINUTE_IN_SECONDS,
        'output'               => true,
        // Global shut-off for dynamic CSS output by the framework. Will also disable google fonts output
        'output_tag'           => true,
        // Allows dynamic CSS to be generated for customizer and google fonts, but stops the dynamic CSS from going to the head
        // 'footer_credit'     => '',                   // Disable the footer credit of Redux. Please leave if you can help it.

        // FUTURE -> Not in use yet, but reserved or partially implemented. Use at your own risk.
        'database'             => '',
        // possible: options, theme_mods, theme_mods_expanded, transient. Not fully functional, warning!

        'use_cdn'              => true,
        // If you prefer not to use the CDN for Select2, Ace Editor, and others, you may download the Redux Vendor Support plugin yourself and run locally or embed it in your code.

        //'compiler'             => true,

        // HINTS
        'hints'                => array(
            'icon'          => 'el el-question-sign',
            'icon_position' => 'right',
            'icon_color'    => 'lightgray',
            'icon_size'     => 'normal',
            'tip_style'     => array(
                'color'   => 'light',
                'shadow'  => true,
                'rounded' => false,
                'style'   => '',
            ),
            'tip_position'  => array(
                'my' => 'top left',
                'at' => 'bottom right',
            ),
            'tip_effect'    => array(
                'show' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'mouseover',
                ),
                'hide' => array(
                    'effect'   => 'slide',
                    'duration' => '500',
                    'event'    => 'click mouseleave',
                ),
            ),
        )
    );

    // ADMIN BAR LINKS -> Setup custom links in the admin bar menu as external items.
    $args['admin_bar_links'][] = array(
        'id'    => 'redux-docs',
        'href'  => 'http://docs.reduxframework.com/',
        'title' => __( 'Documentation', 'redux-framework-demo' ),
    );

    $args['admin_bar_links'][] = array(
        //'id'    => 'redux-support',
        'href'  => 'https://github.com/ReduxFramework/redux-framework/issues',
        'title' => __( 'Support', 'redux-framework-demo' ),
    );

    $args['admin_bar_links'][] = array(
        'id'    => 'redux-extensions',
        'href'  => 'reduxframework.com/extensions',
        'title' => __( 'Extensions', 'redux-framework-demo' ),
    );

    // SOCIAL ICONS -> Setup custom links in the footer for quick links in your panel footer icons.
    $args['share_icons'][] = array(
        'url'   => 'https://github.com/ReduxFramework/ReduxFramework',
        'title' => 'Visit us on GitHub',
        'icon'  => 'el el-github'
        //'img'   => '', // You can use icon OR img. IMG needs to be a full URL.
    );
    $args['share_icons'][] = array(
        'url'   => 'https://www.facebook.com/pages/Redux-Framework/243141545850368',
        'title' => 'Like us on Facebook',
        'icon'  => 'el el-facebook'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://twitter.com/reduxframework',
        'title' => 'Follow us on Twitter',
        'icon'  => 'el el-twitter'
    );
    $args['share_icons'][] = array(
        'url'   => 'http://www.linkedin.com/company/redux-framework',
        'title' => 'Find us on LinkedIn',
        'icon'  => 'el el-linkedin'
    );

    // Panel Intro text -> before the form
    if ( ! isset( $args['global_variable'] ) || $args['global_variable'] !== false ) {
        if ( ! empty( $args['global_variable'] ) ) {
            $v = $args['global_variable'];
        } else {
            $v = str_replace( '-', '_', $args['opt_name'] );
        }
        $args['intro_text'] = sprintf( __( '<p>Did you know that Redux sets a global variable for you? To access any of your saved options from within your code you can use your global variable: <strong>$%1$s</strong></p>', 'redux-framework-demo' ), $v );
    } else {
        $args['intro_text'] = __( '<p>This text is displayed above the options panel. It isn\'t required, but more info is always better! The intro_text field accepts all HTML.</p>', 'redux-framework-demo' );
    }

    // Add content after the form.
    $args['footer_text'] = __( '<p>This text is displayed below the options panel. It isn\'t required, but more info is always better! The footer_text field accepts all HTML.</p>', 'redux-framework-demo' );

    Redux::setArgs( $opt_name, $args );

    /*
     * ---> END ARGUMENTS
     */

    /*
     * ---> START HELP TABS
     */

    $tabs = array(
        array(
            'id'      => 'redux-help-tab-1',
            'title'   => __( 'Theme Information 1', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        ),
        array(
            'id'      => 'redux-help-tab-2',
            'title'   => __( 'Theme Information 2', 'redux-framework-demo' ),
            'content' => __( '<p>This is the tab content, HTML is allowed.</p>', 'redux-framework-demo' )
        )
    );
    Redux::setHelpTab( $opt_name, $tabs );

    // Set the help sidebar
    $content = __( '<p>This is the sidebar content, HTML is allowed.</p>', 'redux-framework-demo' );
    Redux::setHelpSidebar( $opt_name, $content );


    /*
     * <--- END HELP TABS
     */


    /*
     *
     * ---> START SECTIONS
     *
     */

    /*

        As of Redux 3.5+, there is an extensive API. This API can be used in a mix/match mode allowing for


     */

    // -> START General Settings
    Redux::setSection( $opt_name, array(
        'title'  => __( 'General Settings', 'redux-framework-demo' ),
        'id'     => 'basic',
        'desc'   => __( 'Basic General Theme Settings.', 'redux-framework-demo' ),
        'icon'   => 'el el-home',
        'fields' => array(
            /*
            array(
                'id'       => 'opt-google',
                'type'     => 'text',
                'title'    => __( 'Google API', 'redux-framework-demo' ),
                'desc'     => __( 'Enter your GOOGLE API KEY', 'redux-framework-demo' ),
            ) */
        )
    ) );

  

        // Theme Logo Section

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Theme Logo', 'redux-framework-demo' ),
        'desc'       => __( 'Set your theme Logo images', 'redux-framework-demo' ),
        'id'         => 'theme-logo-section',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'desktop-logo',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Desktop Logo', 'redux-framework-demo' ),
                'desc'     => __( 'Select your desktop logo', 'redux-framework-demo' ),
                'default'  => array(
                'url'      => get_site_url().'/wp-content/themes/Citadelle/assets/images/desktop-logo.jpg'),
            ),
              array(
                'id'       => 'mobile-logo',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Mobile Logo', 'redux-framework-demo' ),
                'desc'     => __( 'Select your mobile logo', 'redux-framework-demo' ),
                'default'  => array(
                'url'      => get_site_url().'/wp-content/themes/Citadelle/assets/images/mobile-logo.jpg'),
            ),
             array(
                'id'       => 'footer-logo',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( 'Footer Logo', 'redux-framework-demo' ),
                'desc'     => __( 'Select your footer logo', 'redux-framework-demo' ),
                'default'  => array(
                'url'      => get_site_url().'/wp-content/themes/Citadelle/assets/images/footer-logo.jpg'),
            ),
              
        )
    ) );

    //Contact Section

    Redux::setSection( $opt_name, array(
        'title'      => __( 'Contact', 'redux-framework-demo' ),
        'desc'       => __( 'Set your contact details ', 'redux-framework-demo' ),
        'id'         => 'contact-section',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'phone-header',
                'type'     => 'text',
                'title'    => __( 'Phone', 'redux-framework-demo' ),
                'subtitle' => __( 'Phone Header', 'redux-framework-demo' ),
                'desc'     => __( 'Set the phone number you want to display in the header area', 'redux-framework-demo' ),
                'default'  => '0800 800 800',
            ),
            array(
                'id'       => 'email-header',
                'type'     => 'text',
                'title'    => __( 'Email', 'redux-framework-demo' ),
                'subtitle' => __( 'Email ', 'redux-framework-demo' ),
                'desc'     => __( 'Set the Email you want to display in the header area', 'redux-framework-demo' ),
                'default'  => 'contact@realestate.com',
            ),
        )
    ) );


    //Socials Section

        Redux::setSection( $opt_name, array(
        'title'      => __( 'Socials', 'redux-framework-demo' ),
        'desc'       => __( 'Set your socials details ', 'redux-framework-demo' ),
        'id'         => 'socials-section',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => 'social-facebook',
                'type'     => 'text',
                'title'    => __( 'Facebook', 'redux-framework-demo' ),
                'subtitle' => __( 'Facebook Social', 'redux-framework-demo' ),
                'desc'     => __( 'Set the facebook link you want to display in the header and footer area', 'redux-framework-demo' ),
                'default'  => '#',
            ),
              array(
                'id'       => 'social-linkedin',
                'type'     => 'text',
                'title'    => __( 'Linkedin', 'redux-framework-demo' ),
                'subtitle' => __( 'Linkedin Social', 'redux-framework-demo' ),
                'desc'     => __( 'Set the linkedin link you want to display in the header and footer area', 'redux-framework-demo' ),
                'default'  => '#',
            ),
              array(
                'id'       => 'social-twitter',
                'type'     => 'text',
                'title'    => __( 'Twitter', 'redux-framework-demo' ),
                'subtitle' => __( 'Twitter Social', 'redux-framework-demo' ),
                'desc'     => __( 'Set the twitter link you want to display in the header and footer area', 'redux-framework-demo' ),
                'default'  => '#',
            ),
        )
    ) );


      // 404 Page

        
    Redux::setSection( $opt_name, array(
        'title'      => __( '404 Page', 'redux-framework-demo' ),
        'desc'       => __( 'Set your theme 404 image', 'redux-framework-demo' ),
        'id'         => 'theme-error-section',
        'subsection' => true,
        'fields'     => array(
            array(
                'id'       => '404-image',
                'type'     => 'media',
                'url'      => true,
                'title'    => __( '404 Image', 'redux-framework-demo' ),
                'desc'     => __( 'Select your 404 image', 'redux-framework-demo' ),
                'default'  => array(
                'url'      => get_site_url().'/wp-content/themes/Citadelle/assets/images/404.png'),
            ),
              
              
        )
    ) );
    
        // Article Section

        Redux::setSection ( $opt_name , array(
            'title'      => __('Article Options', 'real-estate'),
            'desc'       => __('Set your general article header', 'real-estate'),
            'id'         => 'article-options',
            'subsection' => true,
            'fields'     => array(
                array(
                    'id'       => 'article-image',
                    'type'     => 'media',
                    'url'      => true,
                    'title'    => __('Article Header Image', 'real-estate'),
                    'desc'     => __('Set your default article header image', 'real-estate'),
                    'default'  => array(
                    'url'      => get_site_url().'wp-content/themes/Citadelle/assets/images/article-header.jpg'
                    ),
                ),
            )
        ));


    /*
     * Product General Settings
     */

        Redux::setSection( $opt_name, array(
        'title'      => __( 'Product Options', 'redux-framework-demo' ),
        'desc'       => __( 'Here you can define values for your products'),
        'id'         => 'product-options-area',
        'icon'       => 'el el-shopping-cart',
        'subsection' => false,
        'fields'     => array(
              array(
                'id'       => 'number-of-products',
                'type'     => 'text',
                'title'    => __( 'Number of Products', 'redux-framework-demo' ),
                'desc'     => __( 'Set the number of products you want to display on the main page', 'redux-framework-demo' ),
                'default'  => '6',
            ),
            array(
                'id'       => 'usd-value',
                'type'     => 'text',
                'title'    => __( 'USD Value', 'redux-framework-demo' ),
                'desc'     => __( 'This is the USD Value ratio', 'redux-framework-demo' ),
                'default'  => '1',
            ),
             array(
                'id'       => 'gbp-value',
                'type'     => 'text',
                'title'    => __( 'GBP Value', 'redux-framework-demo' ),
                'desc'     => __( 'Enter the USD - GBP Ratio Value', 'redux-framework-demo' ),
                'default'  => '1.3',
            ),
             array(
                'id'       => 'euro-value',
                'type'     => 'text',
                'title'    => __( 'Euro Value', 'redux-framework-demo' ),
                'desc'     => __( 'Enter the USD - EUR Ratio Value', 'redux-framework-demo' ),
                'default'  => '1.5',
            ),
        )
    ) );


  
        /*
         * Product Attributes Section\
         */

            Redux::setSection( $opt_name, array(
        'title'      => __( 'Product Attributes', 'redux-framework-demo' ),
        'desc'       => __( 'Set the Product Attributes', 'redux-framework-demo' ) ,
        'id'         => 'product-attributes',
       
        'subsection' => true,
        'fields'     => array(
            
            //neighbourhood area
            
            array(
                'id'       => 'product-neighbourhood',
                'type'     => 'text',
                'title'    => __( 'Neighbourhood Area', 'redux-framework-demo' ),
                'desc'     => __( 'Change this only if you have other category names defined', 'redux-framework-demo' ),
                'default'  => 'pa_neighborhood',
            ),
            
            //bedroom area
            
              array(
                'id'       => 'product-bedrooms',
                'type'     => 'text',
                'title'    => __( 'Bedrooms Area', 'redux-framework-demo' ),
                'desc'     => __( 'Change this only if you have other category names defined', 'redux-framework-demo' ),
                'default'  => 'pa_bedrooms',
            ),
            
            //bathroom area
            
              array(
                'id'       => 'product-bathroom',
                'type'     => 'text',
                'title'    => __( 'Bathroom Area', 'redux-framework-demo' ),
                'desc'     => __( 'Change this only if you have other category names defined', 'redux-framework-demo' ),
                'default'  => 'pa_bathrooms',
            ),
            
            //Rent Area
            
              array(
                'id'       => 'product-rent',
                'type'     => 'text',
                'title'    => __( 'Rent Area', 'redux-framework-demo' ),
                'desc'     => __( 'Change this only if you have other category names defined', 'redux-framework-demo' ),
                'default'  => 'pa_rent',
            ),
            
            //Sale Area
            
              array(
                'id'       => 'product-sale',
                'type'     => 'text',
                'title'    => __( 'Sale Area', 'redux-framework-demo' ),
                'desc'     => __( 'Change this only if you have other category names defined', 'redux-framework-demo' ),
                'default'  => 'pa_sale',
            ),
            
            //Sold Area
            
              array(
                'id'       => 'product-sold',
                'type'     => 'text',
                'title'    => __( 'Sold Area', 'redux-framework-demo' ),
                'desc'     => __( 'Change this only if you have other category names defined', 'redux-framework-demo' ),
                'default'  => 'pa_sold',
            ),
            
            //Property Type Area
            
              array(
                'id'       => 'product-property',
                'type'     => 'text',
                'title'    => __( 'Property Type Area', 'redux-framework-demo' ),
                'desc'     => __( 'Change this only if you have other category names defined', 'redux-framework-demo' ),
                'default'  => 'pa_property-type',
            ),
            
            //Product Surface Area
            
              array(
                'id'       => 'product-area',
                'type'     => 'text',
                'title'    => __( 'Surface Area', 'redux-framework-demo' ),
                'desc'     => __( 'Change this only if you have other category names defined', 'redux-framework-demo' ),
                'default'  => 'pa_area',
            ),
            
            //Ribbons Area
            
            
              array(
                'id'       => 'product-area',
                'type'     => 'text',
                'title'    => __( 'Surface Area', 'redux-framework-demo' ),
                'desc'     => __( 'Change this only if you have other category names defined', 'redux-framework-demo' ),
                'default'  => 'pa_area',
            ),
            
              array(
                'id'       => 'product-area',
                'type'     => 'text',
                'title'    => __( 'Surface Area', 'redux-framework-demo' ),
                'desc'     => __( 'Change this only if you have other category names defined', 'redux-framework-demo' ),
                'default'  => 'pa_area',
            ),
            
        )
    ) );

    /*
     * Maps Section
     */
            Redux::setSection( $opt_name, array(
        'title'      => __( 'Maps Options', 'redux-framework-demo' ),
        'desc'       => __( 'Define your Google Maps Options Here ', 'redux-framework-demo' ),
        'id'         => 'citadelle-maps-options',
        'subsection' => false,
        'fields'     => array(
            array(
                'id'       => 'maps-latitude',
                'type'     => 'text',
                'title'    => __( 'Latitude', 'redux-framework-demo' ),
                'desc'     => __( 'Enter your Latitude Values', 'redux-framework-demo' ),
                'default'  => '-37.8136',
            ),
             array(
                'id'       => 'maps-longitude',
                'type'     => 'text',
                'title'    => __( 'Longitude', 'redux-framework-demo' ),
                'desc'     => __( 'Enter your Longitude Values', 'redux-framework-demo' ),
                'default'  => '144.9631',
            ),
               array(
                'id'       => 'maps-city',
                'type'     => 'text',
                'title'    => __( 'City', 'redux-framework-demo' ),
                'desc'     => __( 'Enter your City Name', 'redux-framework-demo' ),
                'default'  => 'Melbourne',
            ),
             array(
                'id'       => 'opt-google',
                'type'     => 'text',
                'title'    => __( 'Google API', 'redux-framework-demo' ),
                'desc'     => __( 'Enter your GOOGLE API KEY', 'redux-framework-demo' ),
            ),
            array(
                'id'       => 'google-pointer',
                'type'     => 'media',
                'url'      => 'true',
                'title'    => __('Google Pointer', 'redux-framework-demo'),
                'desc'     => __('Select your desired default google pointer'),
                'default'  => array(
                    'url'      => get_site_url().'/wp-content/themes/Citadelle/assets/images/pointer.png'
                    ),
            ),
        )
    ) );

 /*
     * Footer Section
     */
            Redux::setSection( $opt_name, array(
        'title'      => __( 'Footer Options', 'redux-framework-demo' ),
        'desc'       => __( 'Define your footer Options Here ', 'redux-framework-demo' ),
        'id'         => 'citadelle-footer-options',
        'subsection' => false,
        'fields'     => array(
            array(
                'id'       => 'footer-copyright',
                'type'     => 'text',
                'title'    => __( 'Footer Copyright', 'redux-framework-demo' ),
                'desc'     => __( 'Enter your Copyright Values', 'redux-framework-demo' ),
                'default'  => 'Designed with passion by Goodwave',
            ),
        )
    ) );

    /*
     * <--- END SECTIONS
     */
