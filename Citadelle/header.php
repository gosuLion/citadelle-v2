<?php
session_start();

/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Real_Estate
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
  <meta name="viewport" content="width=device-width">

<?php wp_head(); ?>

<!-- Define No conflict JS -->

<script>
var $ = jQuery.noConflict();
</script>
</head>

<body <?php body_class(); ?>>
  <!-- header:START -->
  <header id="header" class="header">
 
    <!-- over header bar: START-->
    <div class="over-header container">
      <div class="container">
        <ul class="pull-right secondary-nav">
          <!-- metrics switch -->
          <li>
             <?php
              get_template_part('estate_class/class', 'currency');
            $currency = new Currency();
              $currency->displaySwitch();
              ?>
          </li>
          
          <!-- metrics switch: END -->

          <!-- language bar: START -->
          <li class="dropdown language">
         <?php dynamic_sidebar( 'language-widget-area' ); ?>
            </li>
          <!-- language bar: END -->
            <?php
           
            $currency->displayCurrency();
            ?>
                  <!-- login: START -->
          <li>
            <!-- Button trigger modal -->
             <?php if(!is_user_logged_in()) :?>
            <a href="account-page"> <button type="button" class="btn btn-lg" >Login</button></a>
            <?php endif;?>
            <?php if(is_user_logged_in()) :?>
              <a href="<?php echo wp_logout_url( get_permalink() ); ?>"> <button type="button" class="btn btn-lg" >Logout</button></a>
            <?php endif;?>
          </li>
          <!-- login END -->
        </ul>
      </div>
    </div>
    <!-- over header bar: END -->

      <!-- navigation:START-->
      <nav class="navbar navbar-default container">
        <div class="container">

          <!-- Brand and toggle get grouped for better mobile display -->
          
          <?php
            get_template_part('estate_class/class', 'navigation');
            $navigation = new Navigation();
            $navigation->displayLogo();
            ?>
        

          <!-- contact info <- hidden on mobile -->
             <?php 
            $navigation->displayContactInfo();
            ?>
 
            <!-- socials -->
            <?php
            $navigation->displaySocials();
            ?>

           
            <!-- search: START -->
            <form method="get" action="<?php echo get_site_url();?>/search-results" class="navbar-form navbar-right">
              <div class="form-group">
                <input type="text" name="agent" class="form-control" placeholder="Search...">
                <button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
              </div>
            </form>
            <!-- search: END -->
          </div>

          <!-- content for toggleing -->
          <div class="collapse navbar-collapse" id="bs-navbar-collapse-1">
          
          <?php if(has_nav_menu('primary')) :?>
          <?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>
         
          <?php else:?>
            <style>
              .menu {
                  display:none;
              }
              </style>



       

        <!-- content for toggleing -->
       
          <ul class="nav navbar-nav">
            <li class="phone-show close-menu"><i class="glyphicon glyphicon-arrow-right" aria-hidden="true"></i></li>

            <li class="dropdown">
              <a href=" <?php echo get_site_url() ;?>/search-page" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">FIND PROPERTIES<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li class="caret"></li>
                <li><a href=" <?php echo get_site_url() ;?>/search-results">Sales properties</a></li>
                <li><a href=" <?php echo get_site_url() ;?>/search-results">Rental Properties</a></li>
                <li><a href=" <?php echo get_site_url() ;?>/search-results">Property details</a></li>
              </ul>
            </li>

            <li><a href=" <?php echo get_site_url() ;?>/neighbourhoods">NEIGHBORHOODS</a></li>

            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">AGENTS<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li class="caret"></li>
                <li><a href=" <?php echo get_site_url() ;?>/our-agents">Our agents</a></li>
               
                <li><a href=" <?php echo get_site_url() ;?>/our-agencies">Agencies</a></li>
             
              </ul>
            </li>

            <li class="dropdown">
              <a href="new-developments" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">NEW DEVELOPMENTS<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li class="caret"></li>
                <li><a href="new-developments">Available Sales</a></li>
                <li><a href="new-developments">Available Rental</a></li>
              </ul>
            </li>

            <li><a href="commercial">COMMERCIAL</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">PAGES<span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li class="caret"></li>
                <li><a href=" <?php echo get_site_url() ;?>/about-us">About us</a></li>
                <li><a href="#">Article</a></li>
                <li><a href=" <?php echo get_site_url() ;?>/articles.html">Articles</a></li>
                <li><a href=" <?php echo get_site_url() ;?>/contact">Contact</a></li>
                <li><a href=" <?php echo get_site_url() ;?>/frequently-asked-questions">FAQ</a></li>
                <li><a href=" <?php echo get_site_url() ;?>/photo-gallery">Gallery</a></li>
                <li><a href=" <?php echo get_site_url() ;?>/testimonials">Testimonials</a></li>
              
                <li><a href="404">404 Page</a></li>
              </ul>
            </li>
          </ul>
       
        <!-- /.navbar-collapse -->

      
      <!-- /.container-fluid -->

         
           <?php endif; ?>
          </div>
          <!-- /.navbar-collapse -->

        </div>
        <!-- /.container-fluid -->
      </nav>
      <!-- navigation: END -->

      <!-- full width slider:START -->
      <?php
      displayCorrectCarousel();
      ?>
      <!-- full width slider:END -->

    </header>
    <!-- /header:END -->