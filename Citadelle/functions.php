<?php
   global $redux_demo;
/**
 * Real Estate functions and definitions.
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Real_Estate
 */

if ( ! function_exists( 'real_estate_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */

/**
 * Load all the plugins needed for this theme
 */
/**
 * This file represents an example of the code that themes would use to register
 * the required plugins.
 *
 * It is expected that theme authors would copy and paste this code into their
 * functions.php file, and amend to suit.
 *
 * @see http://tgmpluginactivation.com/configuration/ for detailed documentation.
 *
 * @package    TGM-Plugin-Activation
 * @subpackage Example
 * @version    2.6.1 for parent theme Citadelle
 * @author     Thomas Griffin, Gary Jones, Juliette Reinders Folmer
 * @copyright  Copyright (c) 2011, Thomas Griffin
 * @license    http://opensource.org/licenses/gpl-2.0.php GPL v2 or later
 * @link       https://github.com/TGMPA/TGM-Plugin-Activation
 */

/**
 * Include the TGM_Plugin_Activation class.
 *
 * Depending on your implementation, you may want to change the include call:
 *
 * Parent Theme:
 * require_once get_template_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Child Theme:
 * require_once get_stylesheet_directory() . '/path/to/class-tgm-plugin-activation.php';
 *
 * Plugin:
 * require_once dirname( __FILE__ ) . '/path/to/class-tgm-plugin-activation.php';
 */
ini_set('default_charset', 'utf-8');
    
require_once get_template_directory() . '/class-tgm-plugin-activation.php';

add_action( 'tgmpa_register', 'citadelle_register_required_plugins' );

/**
 * Register the required plugins for this theme.
 *
 * In this example, we register five plugins:
 * - one included with the TGMPA library
 * - two from an external source, one from an arbitrary source, one from a GitHub repository
 * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
 *
 * The variables passed to the `tgmpa()` function should be:
 * - an array of plugin arrays;
 * - optionally a configuration array.
 * If you are not changing anything in the configuration array, you can remove the array and remove the
 * variable from the function call: `tgmpa( $plugins );`.
 * In that case, the TGMPA default settings will be used.
 *
 * This function is hooked into `tgmpa_register`, which is fired on the WP `init` action on priority 10.
 */
function citadelle_register_required_plugins() {
	/*
	 * Array of plugin arrays. Required keys are name and slug.
	 * If the source is NOT from the .org repo, then source is also required.
	 */
    
	$plugins = array(
/*
		// This is an example of how to include a plugin bundled with a theme.
		array(
			'name'               => 'TGM Example Plugin', // The plugin name.
			'slug'               => 'tgm-example-plugin', // The plugin slug (typically the folder name).
			'source'             => get_template_directory() . '/plugins/tgm-example-plugin.zip', // The plugin source.
			'required'           => true, // If false, the plugin is only 'recommended' instead of required.
			'version'            => '', // E.g. 1.0.0. If set, the active plugin must be this version or higher. If the plugin version is higher than the plugin version installed, the user will be notified to update the plugin.
			'force_activation'   => false, // If true, plugin is activated upon theme activation and cannot be deactivated until theme switch.
			'force_deactivation' => false, // If true, plugin is deactivated upon theme switch, useful for theme-specific plugins.
			'external_url'       => '', // If set, overrides default API URL and points to an external URL.
			'is_callable'        => '', // If set, this callable will be be checked for availability to determine if a plugin is active.
		),

		// This is an example of how to include a plugin from an arbitrary external source in your theme.
		/*array(
			'name'         => 'TGM New Media Plugin', // The plugin name.
			'slug'         => 'tgm-new-media-plugin', // The plugin slug (typically the folder name).
			'source'       => 'https://s3.amazonaws.com/tgm/tgm-new-media-plugin.zip', // The plugin source.
			'required'     => true, // If false, the plugin is only 'recommended' instead of required.
			'external_url' => 'https://github.com/thomasgriffin/New-Media-Image-Uploader', // If set, overrides default API URL and points to an external URL.
		),

		// This is an example of how to include a plugin from a GitHub repository in your theme.
		// This presumes that the plugin code is based in the root of the GitHub repository
		// and not in a subdirectory ('/src') of the repository.
		array(
			'name'      => 'Adminbar Link Comments to Pending',
			'slug'      => 'adminbar-link-comments-to-pending',
			'source'    => 'https://github.com/jrfnl/WP-adminbar-comments-to-pending/archive/master.zip',
		),*/
        array('name' => 'Citadelle Extras',
              'slug' => 'citadelle-extras',
              'source'             => get_template_directory() . '/plugins/citadelle.zip'
             ), 
          array('name' => 'Citadelle Links',
              'slug' => 'citadelle-links',
              'source'             => get_template_directory() . '/plugins/citadelle-links.zip'
             ),
          array('name' => 'Citadelle Widget',
              'slug' => 'example-widget',
              'source'             => get_template_directory() . '/plugins/citadelle-widget.zip'
             ),
         array('name' => 'Citadelle Recent',
              'slug' => 'citadelle-recent',
              'source'             => get_template_directory() . '/plugins/citadelle-recent.zip'
             ),
         array('name' => 'Advanced Custom Fields PRO',
              'slug' => 'acf',
              'source'             => get_template_directory() . '/plugins/acf.zip'
             ),
        array('name' => 'WpGlobus',
              'slug' => 'wpglobus',
              'source'             => get_template_directory() . '/plugins/wpglobus.zip'
             ),

		// This is an example of how to include a plugin from the WordPress Plugin Repository.
		array(
			'name'      => 'Advanced Custom Fields: Font Awesome',
			'slug'      => 'advanced-custom-fields-font-awesome',
			'required'  => true,
		),array(
			'name'      => 'Contact Form 7',
			'slug'      => 'contact-form-7',
			'required'  => true,
		),array(
			'name'      => 'Profile Builder',
			'slug'      => 'profile-builder',
			'required'  => true,
		),array(
			'name'      => 'Max Mega Menu',
			'slug'      => 'megamenu',
			'required'  => true,
		),
        array(
			'name'      => 'Social Login',
			'slug'      => 'oa-social-login',
			'required'  => true,
		),array(
			'name'      => 'WooCommerce',
			'slug'      => 'woocommerce',
			'required'  => true,
		),array(
			'name'      => 'WP User Avatars',
			'slug'      => 'wp-user-avatars',
			'required'  => true,
		),
        array(
			'name'      => 'Redux Framework',
			'slug'      => 'redux-framework',
			'required'  => true,
		),  
        array(
			'name'      => 'One Click Demo Importk',
			'slug'      => 'one-click-demo-import',
			'required'  => true,
		),
        array(
            'name'      => 'Client Dash',
            'slug'      => 'client-dash',
            'required'  => 'true',
        ),

		// This is an example of the use of 'is_callable' functionality. A user could - for instance -
		// have WPSEO installed *or* WPSEO Premium. The slug would in that last case be different, i.e.
		// 'wordpress-seo-premium'.
		// By setting 'is_callable' to either a function from that plugin or a class method
		// `array( 'class', 'method' )` similar to how you hook in to actions and filters, TGMPA can still
		// recognize the plugin as being installed.
		array(
			'name'        => 'WordPress SEO by Yoast',
			'slug'        => 'wordpress-seo',
			'is_callable' => 'wpseo_init',
		),

	);

	/*
	 * Array of configuration settings. Amend each line as needed.
	 *
	 * TGMPA will start providing localized text strings soon. If you already have translations of our standard
	 * strings available, please help us make TGMPA even better by giving us access to these translations or by
	 * sending in a pull-request with .po file(s) with the translations.
	 *
	 * Only uncomment the strings in the config array if you want to customize the strings.
	 */
	$config = array(
		'id'           => 'Citadelle',                 // Unique ID for hashing notices for multiple instances of TGMPA.
		'default_path' => '',                      // Default absolute path to bundled plugins.
		'menu'         => 'tgmpa-install-plugins', // Menu slug.
		'parent_slug'  => 'themes.php',            // Parent menu slug.
		'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
		'has_notices'  => true,                    // Show admin notices or not.
		'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
		'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
		'is_automatic' => false,                   // Automatically activate plugins after installation or not.
		'message'      => '',                      // Message to output right before the plugins table.

		/*
		'strings'      => array(
			'page_title'                      => __( 'Install Required Plugins', 'Citadelle' ),
			'menu_title'                      => __( 'Install Plugins', 'Citadelle' ),
			/* translators: %s: plugin name. * /
			'installing'                      => __( 'Installing Plugin: %s', 'Citadelle' ),
			/* translators: %s: plugin name. * /
			'updating'                        => __( 'Updating Plugin: %s', 'Citadelle' ),
			'oops'                            => __( 'Something went wrong with the plugin API.', 'Citadelle' ),
			'notice_can_install_required'     => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme requires the following plugin: %1$s.',
				'This theme requires the following plugins: %1$s.',
				'Citadelle'
			),
			'notice_can_install_recommended'  => _n_noop(
				/* translators: 1: plugin name(s). * /
				'This theme recommends the following plugin: %1$s.',
				'This theme recommends the following plugins: %1$s.',
				'Citadelle'
			),
			'notice_ask_to_update'            => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.',
				'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.',
				'Citadelle'
			),
			'notice_ask_to_update_maybe'      => _n_noop(
				/* translators: 1: plugin name(s). * /
				'There is an update available for: %1$s.',
				'There are updates available for the following plugins: %1$s.',
				'Citadelle'
			),
			'notice_can_activate_required'    => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following required plugin is currently inactive: %1$s.',
				'The following required plugins are currently inactive: %1$s.',
				'Citadelle'
			),
			'notice_can_activate_recommended' => _n_noop(
				/* translators: 1: plugin name(s). * /
				'The following recommended plugin is currently inactive: %1$s.',
				'The following recommended plugins are currently inactive: %1$s.',
				'Citadelle'
			),
			'install_link'                    => _n_noop(
				'Begin installing plugin',
				'Begin installing plugins',
				'Citadelle'
			),
			'update_link' 					  => _n_noop(
				'Begin updating plugin',
				'Begin updating plugins',
				'Citadelle'
			),
			'activate_link'                   => _n_noop(
				'Begin activating plugin',
				'Begin activating plugins',
				'Citadelle'
			),
			'return'                          => __( 'Return to Required Plugins Installer', 'Citadelle' ),
			'plugin_activated'                => __( 'Plugin activated successfully.', 'Citadelle' ),
			'activated_successfully'          => __( 'The following plugin was activated successfully:', 'Citadelle' ),
			/* translators: 1: plugin name. * /
			'plugin_already_active'           => __( 'No action taken. Plugin %1$s was already active.', 'Citadelle' ),
			/* translators: 1: plugin name. * /
			'plugin_needs_higher_version'     => __( 'Plugin not activated. A higher version of %s is needed for this theme. Please update the plugin.', 'Citadelle' ),
			/* translators: 1: dashboard link. * /
			'complete'                        => __( 'All plugins installed and activated successfully. %1$s', 'Citadelle' ),
			'dismiss'                         => __( 'Dismiss this notice', 'Citadelle' ),
			'notice_cannot_install_activate'  => __( 'There are one or more required or recommended plugins to install, update or activate.', 'Citadelle' ),
			'contact_admin'                   => __( 'Please contact the administrator of this site for help.', 'Citadelle' ),

			'nag_type'                        => '', // Determines admin notice type - can only be one of the typical WP notice classes, such as 'updated', 'update-nag', 'notice-warning', 'notice-info' or 'error'. Some of which may not work as expected in older WP versions.
		),
		*/
	);

	tgmpa( $plugins, $config );
}

    /*
     * Require Redux Framework for Theme Options
     */

require_once (dirname(__FILE__) . '/sample/barebones-config.php');

function real_estate_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on Real Estate, use a find and replace
	 * to change 'real-estate' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'real-estate', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'real-estate' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form',
		'comment-form',
		'comment-list',
		'gallery',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'real_estate_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif;
add_action( 'after_setup_theme', 'real_estate_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function real_estate_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'real_estate_content_width', 640 );
}
add_action( 'after_setup_theme', 'real_estate_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function real_estate_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'real-estate' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'real-estate' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'real_estate_widgets_init' );

if ( !session_id() )

add_action( 'init', 'session_start' );

/**
 * Enqueue scripts and styles.
 */
function real_estate_scripts() {
   global $redux_demo;
   // wp_enqueue_script( 'jquery', esc_url_raw( 'https://code.jquery.com/jquery-1.12.4.min.js' ), array(), null );
   
    wp_enqueue_script( 'jquery-ui', esc_url_raw( 'https://code.jquery.com/ui/1.12.0/jquery-ui.min.js' ), array(), null );
   
    wp_enqueue_script( 'bootstrap', esc_url_raw( 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js' ), array(), null );
    
   $url = 'http://maps.google.com/maps/api/js?key='.$redux_demo['opt-google'].'';
    wp_enqueue_script( 'google-api', $url, array(), null,true );
    
    wp_enqueue_script( 'bootstrap-api', esc_url_raw( 'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.0/js/bootstrap-select.min.js' ), array(), null,true );
   
    wp_enqueue_script( 'jquery-cookie', esc_url_raw( 'https://cdnjs.cloudflare.com/ajax/libs/jquery-cookie/1.4.1/jquery.cookie.js' ), array(), null,true );
     
    wp_enqueue_script( 'jquery-touch', esc_url_raw( 'https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.18/jquery.touchSwipe.min.js' ), array(), null,true );
   
    wp_enqueue_script( 'gmaps', esc_url_raw( 'https://cdnjs.cloudflare.com/ajax/libs/gmaps.js/0.4.24/gmaps.min.js' ), array(), null,true );
    
	wp_enqueue_style( 'fonts',esc_url_raw('//netdna.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.css') );
    
	wp_enqueue_style( 'bootstrap', esc_url_raw('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css') );
    
	wp_enqueue_style( 'jquery-ui', esc_url_raw('//code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css') );
    
	wp_enqueue_style( 'real-estate-style', get_stylesheet_uri() );

	wp_enqueue_script( 'real-estate-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );

	wp_enqueue_script( 'real-estate-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );
   wp_enqueue_script('jquery-carousel', get_template_directory_uri(). '/js/jquery.jcarousel.min.js',array(),'',true);
   wp_enqueue_script('jquery-pajinate', get_template_directory_uri(). '/js/jquery.pajinate.min.js',array(),'',true);
   wp_enqueue_script('real-estate', get_template_directory_uri(). '/js/app.min.js',array(),'',true);
   //wp_enqueue_script('real-estate', get_template_directory_uri(). '/js/add.js',array(),'',true);

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'real_estate_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

/**
 * Add woocommerce support
 */
add_theme_support( 'woocommerce' );

/**
 * Add body class by page template.
 */

add_filter( 'body_class','class_by_template' );
function class_by_template( $classes ) {
 
    if ( is_page_template( 'template-parts/front-page.php' ) ) {
        $classes[] = 'home';
    }
    else if ( is_page_template( 'template-parts/search-page.php' ) ) {
        $classes[] = 'find-properties';
    }
      else if ( is_page_template( 'template-parts/faq-page.php' ) ) {
        $classes[] = 'faq';
    }
     else if ( is_page_template( 'template-parts/gallery-page.php' ) ) {
        $classes[] = 'gallery';
    }
    else if ( is_page_template( 'template-parts/testimonials-page.php' ) ) {
        $classes[] = 'testimonials';
    }
     else if ( is_page_template( 'template-parts/developments-page.php' ) ) {
        $classes[] = 'new-developments';
    }
    else if ( is_page_template( 'template-parts/commercial-page.php' ) ) {
        $classes[] = 'commercial';
    }  
    else if ( is_page_template( 'template-parts/neighbourhoods-page.php' ) ) {
        $classes[] = 'neighbourhoods';
    }
      else if ( is_page_template( 'template-parts/ouragents-page.php' ) ) {
        $classes[] = 'our-agents';
    }  
    else if ( is_page_template( 'template-parts/ouragencies-page.php' ) ) {
        $classes[] = 'our-agents';
    } 
    else if ( is_page_template( 'template-parts/contact-page.php' ) ) {
        $classes[] = 'contact-page';
    }
    else if ( is_page_template( 'template-parts/listings-page.php' ) ) {
        $classes[] = 'our-agents agent-details';
    }
     else if ( is_page_template( 'template-parts/aboutus-page.php' ) ) {
        $classes[] = 'about-us';
    }
      else if(is_product())
    {
        $classes[]= 'property-details';
    }
    else if ( is_single() ) {
        $classes[] = 'article';
    }
     else if ( is_archive() ) {
        $classes[] = 'article articles';
    }
     else if ( is_page_template( '404.php' ) ) {
        $classes[] = 'error-404';
    }
    else if (is_page_template('template-parts/typografy-page.php')) {
        $classes[] = 'typography';
    }

    
     
    return $classes;
     
}

/**
 * Get user Avatar URL
 */
function get_avatar_url2($user_id, $size) {
    $avatar_url = get_avatar($user_id, $size);
    $regex = '/(^.*src="|" w.*$)/';
    return preg_replace($regex, '', $avatar_url);
}

/**
 * Make href for tabbed content
 */
function get_href($string_content) {
    $lowerString = strtolower($string_content);
    $newString = explode(' ',$lowerString);
    $string = implode('-',$newString);
    return $string;
}

add_action('init', 'myStartSession', 1);
function myStartSession() {
    if(!session_id()) {
        session_start();
    }
}

/**
 * Get User by ID
 */
function get_user_id_by_display_name( $display_name ) {
    global $wpdb;

    if ( ! $user = $wpdb->get_row( $wpdb->prepare(
        "SELECT `ID` FROM $wpdb->users WHERE `display_name` = %s", $display_name
    ) ) )
        return false;

    return $user->ID;
}

/**
 * Building the Theme Options

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Theme General Settings',
		'menu_title'	=> 'Theme Settings',
		'menu_slug' 	=> 'theme-general-settings',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Header Settings',
		'menu_title'	=> 'Header',
		'parent_slug'	=> 'theme-general-settings',
	));
	
	acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Footer Settings',
		'menu_title'	=> 'Footer',
		'parent_slug'	=> 'theme-general-settings',
	));
    acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Product Settings',
		'menu_title'	=> 'Product',
		'parent_slug'	=> 'theme-general-settings',
	));
       acf_add_options_sub_page(array(
		'page_title' 	=> 'Theme Search Engine Settings',
		'menu_title'	=> 'Search Engine',
		'parent_slug'	=> 'theme-general-settings',
	));
	
}
 */

function displayCorrectCarousel() {
    //Search and display the correct header for each page
     if ( is_page_template( 'template-parts/front-page.php' )  )  {
       get_template_part('estate_class/class', 'carousel');
      $carousel = new Carousel();
      $carousel->displayCarousel();
       }
      
      else if ( is_page_template( 'template-parts/contact-page.php')) {
          get_template_part('estate_class/class', 'ourmaps');
          $contactMap = new Maps();
          $contactMap->displayContactMap();
      }
      else if ( is_page_template( 'template-parts/faq-page.php') ) {
          get_template_part('estate_class/class', 'faq');
          $faq = new Faq();
          $faq->displayHeader(get_field('header_image'));
      }
       else if ( is_page_template( 'template-parts/testimonials-page.php')) {
          get_template_part('estate_class/class', 'testimonials');
          $testimonials = new Testimonials();
          $testimonials->displayHeader(get_field('header_image'),get_field('testimonial_image_header'),get_field('testimonial_name_header'),get_field('testimonial_position'),get_field('testimonial_description_header'));
      }
         else if ( is_page_template( 'template-parts/gallery-page.php') || is_product()) {
         get_template_part('estate_class/class', 'carousel');
      $carousel = new Carousel();
      $carousel->displayCarouselGallery(get_field('header_gallery'));
      }  
      else if ( is_page_template( 'template-parts/listings-page.php') ) {
         get_template_part('estate_class/class', 'carousel');
      $carousel = new Carousel();
      $carousel->displayCarouselAgent($_GET['id']);
      }
    else if ( is_page_template( 'template-parts/aboutus-page.php') ) {
             get_template_part('estate_class/class', 'carousel');
      $carousel = new Carousel();
      $carousel->displayCarouselCommercial(get_field('header_image'),get_field('header_subtitle'),get_field('header_title'));
      }
        else if ( is_page_template( 'template-parts/commercial-page.php') ||  is_page_template( 'template-parts/neighbourhoods-page.php') || is_page_template( 'template-parts/developments-page.php') || is_page_template( 'template-parts/ouragents-page.php') || is_page_template( 'template-parts/ouragencies-page.php')) {
         get_template_part('estate_class/class', 'carousel');
      $carousel = new Carousel();
      $carousel->displayCarouselCommercial(get_field('header_image'),get_field('header_subtitle'),get_field('header_title'));
      }
         else if ( is_archive() || is_page_template('template-parts/search-page.php') || is_page_template('template-parts/account-page.php') || is_page_template('template-parts/typografy-page.php')) {
         get_template_part('estate_class/class', 'carousel');
      $carousel = new Carousel();
      $carousel->displayCarouselGallery($redux_demo['article-image']);
      }
       else if ( is_single() ) {
         get_template_part('estate_class/class', 'carousel');
      $carousel = new Carousel();
      $carousel->displayCarouselArticle(get_field('header_image'),get_field('icon'),get_field('article_subtitle'));
      }
}


function tutsplus_widgets_init() {
 
    // First footer widget area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'First Footer Widget Area', 'tutsplus' ),
        'id' => 'first-footer-widget-area',
        'description' => __( 'The first footer widget area', 'tutsplus' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
 
    // Second Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Second Footer Widget Area', 'tutsplus' ),
        'id' => 'second-footer-widget-area',
        'description' => __( 'The second footer widget area', 'tutsplus' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
 
    // Third Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Third Footer Widget Area', 'tutsplus' ),
        'id' => 'third-footer-widget-area',
        'description' => __( 'The third footer widget area', 'tutsplus' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
 
    // Fourth Footer Widget Area, located in the footer. Empty by default.
    register_sidebar( array(
        'name' => __( 'Fourth Footer Widget Area', 'tutsplus' ),
        'id' => 'fourth-footer-widget-area',
        'description' => __( 'The fourth footer widget area', 'tutsplus' ),
        'before_widget' => '<div id="%1$s" class="widget-container %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ) );
    
    // Language Widget Area
    register_sidebar( array(
        'name' => __( 'Language Widget Area', 'tutsplus' ),
        'id' => 'language-widget-area',
        'description' => __( 'Language Widget Area', 'tutsplus' ),
        'before_widget' => '<li>',
        'after_widget' => '</li>'
    ) );
         
}
// Register sidebars by running tutsplus_widgets_init() on the widgets_init hook.
add_action( 'widgets_init', 'tutsplus_widgets_init' );

//Development function
function allplugin(){
    // Check if get_plugins() function exists. This is required on the front end of the
// site, since it is in a file that is normally only loaded in the admin.
if ( ! function_exists( 'get_plugins' ) ) {
	require_once ABSPATH . 'wp-admin/includes/plugin.php';
}

$all_plugins = get_plugins();
foreach($all_plugins as $plugin)
{
    echo $plugin['Name']." : ".$plugin['TextDomain']."</br>";
}
// Save the data to the error log so you can see what the array format is like.
//print_r( $all_plugins);
 }




// add default image setting to ACF image fields
  // let's you select a defualt image
  // this is simply taking advantage of a field setting that already exists
  
	add_action('acf/render_field_settings/type=image', 'add_default_value_to_image_field', 20);
	function add_default_value_to_image_field($field) {
		acf_render_field_setting( $field, array(
			'label'			=> 'Default Image',
			'instructions'		=> 'Appears when creating a new post',
			'type'			=> 'image',
			'name'			=> 'default_value',
		));
	}

add_action('after_switch_theme', 'citadelle_setup_activation');

function citadelle_setup_activation () {

    add_role( 'Agency', 'Agency', array( 'read' => true,
                                        'level_10' => true,
                                        'edit_dashboard' => 'true',
                                        'activate_plugins' => 'true',
                                        'delete_pages' => 'true',
                                        'delete_posts' => 'true',
                                        'delete_private_pages' => 'true',
                                        'delete_private_posts' => 'true',
                                        'delete_published_pages' => 'true',
                                        'delete_published_posts' => 'true',
                                        'edit_pages' => 'true',
                                        'edit_posts' => 'true',
                                        'edit_private_pages' => 'true',
                                        'edit_private_posts' => 'true',
                                        'edit_published_pages' => 'true',
                                        'edit_published_posts' => 'true',
                                        'edit_theme_options' => 'true',
                                        'export' => 'true',
                                        'import' => 'true',
                                        'list_users' => 'true',
                                        'manage_categories' => 'true',
                                        'manage_links' => 'true',
                                        'manage_options' => 'true',
                                        'moderate_comments' => 'true',
                                        'promote_users' => 'true',
                                        'publish_pages' => 'true',
                                        'publish_posts' => 'true',
                                        'read_private_pages' => 'true',
                                        'reade_private_posts' => 'true',
                                        
                                       ) );
      
    add_role( 'Agent', 'Agent', array( 'read' => true,
                                        'level_10' => true,
                                        'edit_dashboard' => 'true',
                                        'activate_plugins' => 'true',
                                        'delete_pages' => 'true',
                                        'delete_posts' => 'true',
                                        'delete_private_pages' => 'true',
                                        'delete_private_posts' => 'true',
                                        'delete_published_pages' => 'true',
                                        'delete_published_posts' => 'true',
                                        'edit_pages' => 'true',
                                        'edit_posts' => 'true',
                                        'edit_private_pages' => 'true',
                                        'edit_private_posts' => 'true',
                                        'edit_published_pages' => 'true',
                                        'edit_published_posts' => 'true',
                                        'edit_theme_options' => 'true',
                                        'export' => 'true',
                                        'import' => 'true',
                                        'list_users' => 'true',
                                        'manage_categories' => 'true',
                                        'manage_links' => 'true',
                                        'manage_options' => 'true',
                                        'moderate_comments' => 'true',
                                        'promote_users' => 'true',
                                        'publish_pages' => 'true',
                                        'publish_posts' => 'true',
                                        'read_private_pages' => 'true',
                                        'reade_private_posts' => 'true',
                                        
                                       ) );
    
  
      
   
}
//Create Product attributes 

function ocdi_before_content_import( $selected_import ) {
    if ( 'Demo Import 1' === $selected_import['import_file_name'] ) {

$insert = process_add_attribute(array('attribute_name' => 'area', 'attribute_label' => 'Area', 'attribute_type' => 'text', 'attribute_orderby' => 'menu_order', 'attribute_public' => false));

$insert = process_add_attribute(array('attribute_name' => 'bathrooms', 'attribute_label' => 'Bathrooms', 'attribute_type' => 'text', 'attribute_orderby' => 'menu_order', 'attribute_public' => false));

$insert = process_add_attribute(array('attribute_name' => 'bedrooms', 'attribute_label' => 'Bedrooms', 'attribute_type' => 'text', 'attribute_orderby' => 'menu_order', 'attribute_public' => false));

$insert = process_add_attribute(array('attribute_name' => 'neighborhood', 'attribute_label' => 'Neighborhood', 'attribute_type' => 'text', 'attribute_orderby' => 'menu_order', 'attribute_public' => false));

$insert = process_add_attribute(array('attribute_name' => 'property-type', 'attribute_label' => 'Property Type', 'attribute_type' => 'text', 'attribute_orderby' => 'menu_order', 'attribute_public' => false));

$insert = process_add_attribute(array('attribute_name' => 'rent', 'attribute_label' => 'Rent', 'attribute_type' => 'text', 'attribute_orderby' => 'menu_order', 'attribute_public' => false));

$insert = process_add_attribute(array('attribute_name' => 'sale', 'attribute_label' => 'Sale', 'attribute_type' => 'text', 'attribute_orderby' => 'menu_order', 'attribute_public' => false));


$insert = process_add_attribute(array('attribute_name' => 'sold', 'attribute_label' => 'Sold', 'attribute_type' => 'text', 'attribute_orderby' => 'menu_order', 'attribute_public' => false));

if (is_wp_error($insert)) { do_something_for_error($insert); }
        
    }
    else {
        // Here you can do stuff for all other imports before the content import starts.
        echo "before import 2";
    }
}
add_action( 'pt-ocdi/before_content_import', 'ocdi_before_content_import' );


function ocdi_after_import_setup() {
    // Assign menus to their locations.
    $main_menu = get_term_by( 'name', 'Main Menu', 'nav_menu' );

    set_theme_mod( 'nav_menu_locations', array(
            'main-menu' => $main_menu->term_id,
        )
    );

    // Assign front page and posts page (blog page).
    $front_page_id = get_page_by_title( 'Homepage' );
    

    update_option( 'show_on_front', 'page' );
    update_option( 'page_on_front', $front_page_id->ID );
   

}
add_action( 'pt-ocdi/after_import', 'ocdi_after_import_setup' );


 /*
  * Enter Google Map API KEY
  */

function my_acf_init() {
	
    global $redux_demo;
    
	acf_update_setting('google_api_key', $redux_demo['opt-google']);
}

add_action('acf/init', 'my_acf_init');

  /*
   * Demo Import
   */
function ocdi_import_files() {
    return array(
        array(
            'import_file_name'           => 'Demo Import 1',
            'categories'                 => array( 'Category 1', 'Category 2' ),
            'import_file_url'            => 'http://dev.dandelions.ro/citadelle/wp-content/themes/Citadelle/demo/default/content.xml',
            'import_preview_image_url'   => 'http://dev.dandelions.ro/citadelle/wp-content/themes/Citadelle/demo/default/screenshot.png',
            'import_notice'              => __( 'After you import this demo you have to import Products.xml using the Wordpress Importer', 'your-textdomain' ),
        ),
        
    );
}
add_filter( 'pt-ocdi/import_files', 'ocdi_import_files' );



if ( class_exists( 'WooCommerce' ) ) {
//add caps to editor role
    $role = get_role("Agency");
    $role = get_role("Agent");

    //for woocommerce
    $role->add_cap("manage_woocommerce");
    $role->add_cap("view_woocommerce_reports");
    $role->add_cap("edit_product");
    $role->add_cap("read_product");
    $role->add_cap("delete_product");
    $role->add_cap("edit_products");
    $role->add_cap("edit_others_products");
    $role->add_cap("publish_products");
    $role->add_cap("read_private_products");
    $role->add_cap("delete_products");
    $role->add_cap("delete_private_products");
    $role->add_cap("delete_published_products");
    $role->add_cap("delete_others_products");
    $role->add_cap("edit_private_products");
    $role->add_cap("edit_published_products");
    $role->add_cap("manage_product_terms");
    $role->add_cap("edit_product_terms");
    $role->add_cap("delete_product_terms");
    $role->add_cap("assign_product_terms");
    $role->add_cap("edit_shop_order");
    $role->add_cap("read_shop_order");
    $role->add_cap("delete_shop_order");
    $role->add_cap("edit_shop_orders");
    $role->add_cap("edit_others_shop_orders");
    $role->add_cap("publish_shop_orders");
    $role->add_cap("read_private_shop_orders");
    $role->add_cap("delete_shop_orders");
    $role->add_cap("delete_private_shop_orders");
    $role->add_cap("delete_published_shop_orders");
    $role->add_cap("delete_others_shop_orders");
    $role->add_cap("edit_private_shop_orders");
    $role->add_cap("edit_published_shop_orders");
    $role->add_cap("manage_shop_order_terms");
    $role->add_cap("edit_shop_order_terms");
    $role->add_cap("delete_shop_order_terms");
    $role->add_cap("assign_shop_order_terms");
    $role->add_cap("edit_shop_coupon");
    $role->add_cap("read_shop_coupon");
    $role->add_cap("delete_shop_coupon");
    $role->add_cap("edit_shop_coupons");
    $role->add_cap("edit_others_shop_coupons");
    $role->add_cap("publish_shop_coupons");
    $role->add_cap("read_private_shop_coupons");
    $role->add_cap("delete_shop_coupons");
    $role->add_cap("delete_private_shop_coupons");
    $role->add_cap("delete_published_shop_coupons");
    $role->add_cap("delete_others_shop_coupons");
    $role->add_cap("edit_private_shop_coupons");
    $role->add_cap("edit_published_shop_coupons");
    $role->add_cap("manage_shop_coupon_terms");
    $role->add_cap("edit_shop_coupon_terms");
    $role->add_cap("delete_shop_coupon_terms");
    $role->add_cap("assign_shop_coupon_terms");
    $role->add_cap("edit_shop_webhook");
    $role->add_cap("read_shop_webhook");
    $role->add_cap("delete_shop_webhook");
    $role->add_cap("edit_shop_webhooks");
    $role->add_cap("edit_others_shop_webhooks");
    $role->add_cap("publish_shop_webhooks");
    $role->add_cap("read_private_shop_webhooks");
    $role->add_cap("delete_shop_webhooks");
    $role->add_cap("delete_private_shop_webhooks");
    $role->add_cap("delete_published_shop_webhooks");
    $role->add_cap("delete_others_shop_webhooks");
    $role->add_cap("edit_private_shop_webhooks");
    $role->add_cap("edit_published_shop_webhooks");
    $role->add_cap("manage_shop_webhook_terms");
    $role->add_cap("edit_shop_webhook_terms");
    $role->add_cap("delete_shop_webhook_terms");
    $role->add_cap("assign_shop_webhook_terms");

}

function process_add_attribute($attribute)
{
    global $wpdb;
//      check_admin_referer( 'woocommerce-add-new_attribute' );

    if (empty($attribute['attribute_type'])) { $attribute['attribute_type'] = 'text';}
    if (empty($attribute['attribute_orderby'])) { $attribute['attribute_orderby'] = 'menu_order';}
    if (empty($attribute['attribute_public'])) { $attribute['attribute_public'] = 0;}

    if ( empty( $attribute['attribute_name'] ) || empty( $attribute['attribute_label'] ) ) {
            return new WP_Error( 'error', __( 'Please, provide an attribute name and slug.', 'woocommerce' ) );
    } elseif ( ( $valid_attribute_name = valid_attribute_name( $attribute['attribute_name'] ) ) && is_wp_error( $valid_attribute_name ) ) {
            return $valid_attribute_name;
    } elseif ( taxonomy_exists( wc_attribute_taxonomy_name( $attribute['attribute_name'] ) ) ) {
            return new WP_Error( 'error', sprintf( __( 'Slug "%s" is already in use. Change it, please.', 'woocommerce' ), sanitize_title( $attribute['attribute_name'] ) ) );
    }

    $wpdb->insert( $wpdb->prefix . 'woocommerce_attribute_taxonomies', $attribute );

    do_action( 'woocommerce_attribute_added', $wpdb->insert_id, $attribute );

    flush_rewrite_rules();
    delete_transient( 'wc_attribute_taxonomies' );

    return true;
}

function valid_attribute_name( $attribute_name ) {
    if ( strlen( $attribute_name ) >= 28 ) {
            return new WP_Error( 'error', sprintf( __( 'Slug "%s" is too long (28 characters max). Shorten it, please.', 'woocommerce' ), sanitize_title( $attribute_name ) ) );
    } elseif ( wc_check_if_attribute_name_is_reserved( $attribute_name ) ) {
            return new WP_Error( 'error', sprintf( __( 'Slug "%s" is not allowed because it is a reserved term. Change it, please.', 'woocommerce' ), sanitize_title( $attribute_name ) ) );
    }

    return true;
}
