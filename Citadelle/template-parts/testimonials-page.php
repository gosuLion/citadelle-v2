<?php

/**
 * Template Name: Testimonials Page
 *
 * @package Real Estate
 * @subpackage Goodwave
 * @since Goodwave 
 */

get_header(); ?>
        <?php
            $testimonials = new Testimonials();
            $testimonials->displayTestimonials();

get_footer();