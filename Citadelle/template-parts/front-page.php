<?php

/**
 * Template Name: Front Page
 *
 * @package Real Estate
 * @subpackage Goodwave
 * @since Goodwave 
 */

get_header(); ?>
 <div class="bg-f5f5f5"> <!-- start bg f5f5f5 -->
<!-- advanced search: START -->
   <?php 
     get_template_part('estate_class/class', 'searchmodule');
     $search = new SearchModule();
     $search->displaySearchForm(); 
     ?>
<?php
     global $redux_demo;
     
     
     ?>
<!-- advanced search: END -->

<!-- property listing: START -->
    <?php
     get_template_part('estate_class/class', 'realestate');
     $property = new RealEstate();
     $property->displayPropertyListings();
     ?>
     
<!-- property listing: END -->
</div> <!-- end f5f5f5 bg -->

<!-- our agents: START -->
  <?php

     get_template_part('estate_class/class', 'ouragents');
     $agents = new OurAgents();
     $agents->displayAgents();
     ?>
<!-- our agents: END -->

<!-- photo gallery -->
 <?php
     get_template_part('estate_class/class', 'photogallery');
     $gallery = new PhotoGallery();
     $gallery->displayGallery();
     ?>
<!-- end photo gallery -->

<!-- partners: START -->
 <?php
     get_template_part('estate_class/class', 'ourpartners');
     $partners = new OurPartners();
     $partners->displayOurPartners();
     ?>
<!-- partners: END -->
<!-- achievements: START -->
<?php
     get_template_part('estate_class/class', 'ourachievements');
     $achievements = new OurAchievements();
     $achievements->displayAchievements();
     ?>
<!-- achievements: END -->

<!-- latest news: START -->
<?php 
     get_template_part('estate_class/class', 'articles');
     $articles = new Articles();
     $articles->displayCategoryArticles(); 
     ?>
<!-- latest news: END -->
<!-- awards: START -->
<?php
     get_template_part('estate_class/class', 'ourawards');
     $awards = new OurAwards();
     $awards->displayAwards();
     ?>
<!-- awards: START -->

<!-- location: START -->


<div class="acf-map mmap">
<?php
    global $redux_demo;
    ?>
	<div class="marker" data-lat="<?php echo $redux_demo['maps-latitude'] ; ?>" data-lng="<?php echo $redux_demo['maps-longitude'] ; ?>"></div>
</div>

<!-- location: END -->




<?php

get_footer();