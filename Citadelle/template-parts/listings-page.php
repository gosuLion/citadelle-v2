<?php

/**
 * Template Name: Listings Page
 *
 * @package Real Estate
 * @subpackage Goodwave
 * @since Goodwave 
 */

get_header(); ?>
<?php
$users = get_user_by('id',$_GET['id']);
           $bio = get_the_author_meta('description',$users->ID);
?>


<div class="container" id="agents-paginate">
      <div class="row details-content">
        <div class="col-xs-12">
          <div class="favorites-share clearfix">
            <div class="fav">
              <i class="fa fa-heart-o"></i>Add to favorites
            </div>
            <div class="share">
              <a href="#"><i class="fa fa-share-alt" aria-hidden="true"></i>Share</a>
            </div>
          </div>
        </div>
        <div class="col-xs-12">
          <p><?php echo $bio; ?>
          </p>
        </div>
      </div>
      <!-- results head: START -->
      <div class="row">
        <div class="col-xs-12 result-head">
          <!-- sort: START -->
          <div class="col-xs-6 col-md-3 col-lg-2">
            Sort by: 
            <span class="dropdown">
              <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><br class="mobile-show"> Default Order <i class="fa fa-angle-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu">
                <li><a href="javascript: void(0)">Price (Low to High)</a></li>
                <li><a href="javascript: void(0)">Price (High to Low)</a></li>
                <li><a href="javascript: void(0)">Featured</a></li>
                <li><a href="javascript: void(0)">Date Old to New</a></li>
                <li><a href="javascript: void(0)">Date New to Old</a></li>
              </ul>
            </span>
          </div>
<!-- sort: START -->

<!-- order: START -->
          <div class="col-xs-6 col-md-3 col-lg-2">
            Order by: 
            <span class="dropdown">
              <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><br class="mobile-show"> Default Order <i class="fa fa-angle-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu">
                <li><a href="javascript: void(0)">Price (Low to High)</a></li>
                <li><a href="javascript: void(0)">Price (High to Low)</a></li>
                <li><a href="javascript: void(0)">Featured</a></li>
                <li><a href="javascript: void(0)">Date Old to New</a></li>
                <li><a href="javascript: void(0)">Date New to Old</a></li>
              </ul>
            </span>
          </div>
<!-- order: END -->

          <div class="col-xs-6 col-sm-2 mobile-hide">
            <a href="javascript: void(0)"><i class="fa fa-th-large" aria-hidden="true"></i></a>
            <a href="javascript: void(0)"><i class="fa fa-list selected" aria-hidden="true"></i></a>
          </div>

<!-- pagination:START -->
          <div class="col-xs-6 col-md-3 pull-right text-right">
            <div class="pagination page_navigation pagination-sm"></div>
          </div>
<!-- pagination:END -->
        </div>
<!-- result container: START -->
        <div class="content col-xs-12">
       
       <?php 
     get_template_part('estate_class/class', 'listings');
     $search = new ListingsPage();
     $search->displayListings($_GET['id']); 
     ?>
       
        </div>
<!-- result container: END -->
      </div>
<!-- results head: END -->
      <div class="row list content">
      </div>

<!-- results head: START -->
      <div class="row">
        <div class="col-xs-12 result-head">
<!-- sort: START -->
          <div class="col-xs-6 col-md-3 col-lg-2">
            Sort by: 
            <span class="dropdown">
              <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><br class="mobile-show"> Default Order <i class="fa fa-angle-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu">
                <li><a href="javascript: void(0)">Price (Low to High)</a></li>
                <li><a href="javascript: void(0)">Price (High to Low)</a></li>
                <li><a href="javascript: void(0)">Featured</a></li>
                <li><a href="javascript: void(0)">Date Old to New</a></li>
                <li><a href="javascript: void(0)">Date New to Old</a></li>
              </ul>
            </span>
          </div>
<!-- sort: START -->

<!-- order: START -->
          <div class="col-xs-6 col-md-3 col-lg-2">
            Order by: 
            <span class="dropdown">
              <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><br class="mobile-show"> Default Order <i class="fa fa-angle-down" aria-hidden="true"></i></a>
              <ul class="dropdown-menu">
                <li><a href="javascript: void(0)">Price (Low to High)</a></li>
                <li><a href="javascript: void(0)">Price (High to Low)</a></li>
                <li><a href="javascript: void(0)">Featured</a></li>
                <li><a href="javascript: void(0)">Date Old to New</a></li>
                <li><a href="javascript: void(0)">Date New to Old</a></li>
              </ul>
            </span>
          </div>
<!-- order: END -->

          <div class="col-xs-6 col-sm-2 mobile-hide">
          </div>

<!-- pagination:START -->
          <div class="col-xs-6 col-md-3 pull-right text-right">
            <div class="pagination page_navigation pagination-sm"></div>
<!-- pagination:END -->
        </div>
      </div>
<!-- results head: END -->
    </div>
<?php
get_footer();