<?php

/**
 * Template Name: Commercial Page
 *
 * @package Real Estate
 * @subpackage Goodwave
 * @since Goodwave 
 */

get_header(); ?>


<?php 

     get_template_part('estate_class/class', 'commercial');
     $commercial = new Commercial();
     $commercial->displayCategories('commercial'); 

get_footer();