<?php

/**
 * Template Name: Search Results
 *
 * @package Real Estate
 * @subpackage Goodwave
 * @since Goodwave 
 */

get_header(); ?>


     <!-- find properties: START -->
  <div class="container search">
    <div class="row">
      <div class="col-xs-12 title">
        <h2><b>Find properties</b></h2>
        <p>
          Be the first to get our top rated property list <br>
          and contact out best agents!
        </p>
        <div class="line"></div>
      </div>
      
<!-- sidebar: START -->
      <div class="col-xs-12 col-sm-3 sidebar">
<!-- map results cta: START -->
          
       
<!-- map results cta: START -->

<!-- search form: START -->
       <?php 
     get_template_part('estate_class/class', 'searchmodule');
     $search = new SearchModule();
     $search->displaySearchForm2(); 
     ?>
<!-- search form: START -->
<!-- valued properties: START -->
        <div class="best-valued">
          <div class="inner">
            <h4>RECENT NEWS</h4>

        <?php 
     get_template_part('estate_class/class', 'articles');
     $articles = new Articles();
     $articles->displaySearchArticles(); 
     ?>

            <p class="text-center">
              <a href="javascript: void(0)" class="btn">View more ...</a>
            </p>

          </div>
        </div>
<!-- valued properties: END -->
      </div>
<!-- sidebar: END -->
<div class="col-xs-12 col-sm-9 map-modal animated slideInLeft">
        <?php 
        $search->displayMapByProduct( $_GET['neighbour'],
         $_GET['property'],
         $_GET['bedroom'],
         $_GET['bathroom'],
         $_GET['area'],
         $_GET['price'],
         $_GET['badge'],
         $_GET['agent']
                           );
$location = get_field('homepage_map','option');
if( !empty($location) ):
?>



<?php endif; ?>
        </div>
<!-- search-results: START -->
      <div class="col-xs-12 col-sm-9">
        <div class="search-results clearfix" id="search-results"><!-- start search-results container -->
          <p class="nr-of-results">
            Your search has <b><span class="number"><?php echo  $_SESSION['total']; ?></span> results</b>
          </p>
<!-- search results head: START -->
          <div class="col-xs-12 result-head">
<!-- sort: START -->
            <div class="col-xs-6 col-md-3">
              Sort by: 
              <span class="dropdown">
                <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><br class="mobile-show"> Default Order <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <ul class="dropdown-menu">
                  <li><a href="javascript: void(0)">Price (Low to High)</a></li>
                  <li><a href="javascript: void(0)">Price (High to Low)</a></li>
                  <li><a href="javascript: void(0)">Featured</a></li>
                  <li><a href="javascript: void(0)">Date Old to New</a></li>
                  <li><a href="javascript: void(0)">Date New to Old</a></li>
                </ul>
              </span>
            </div>
<!-- sort: START -->
<!-- order: START -->
            <div class="col-xs-6 col-md-4">
              Order by: 
              <span class="dropdown">
                <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><br class="mobile-show"> Default Order <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <ul class="dropdown-menu">
                  <li><a href="javascript: void(0)">Price (Low to High)</a></li>
                  <li><a href="javascript: void(0)">Price (High to Low)</a></li>
                  <li><a href="javascript: void(0)">Featured</a></li>
                  <li><a href="javascript: void(0)">Date Old to New</a></li>
                  <li><a href="javascript: void(0)">Date New to Old</a></li>
                </ul>
              </span>
            </div>
<!-- order: END -->
            <div class="col-xs-6 col-md-2">
              <a href="javascript: void(0)"><i class="fa fa-th-large" aria-hidden="true"></i></a>
              <a href="javascript: void(0)"><i class="fa fa-list selected" aria-hidden="true"></i></a>
              
            </div>
<!-- pagination:START -->
            <div class="col-xs-6 col-md-3 text-right">
              <div class="pagination page_navigation pagination-sm"></div>
            </div>
<!-- pagination:END -->
          </div>
<!-- search results head: END -->
<!-- result container: START -->
          <div class="content">
<!-- result-item: START -->
            <?php 
            
              
   if($_GET['agent'] == "" && $_GET['badge'] == ""){
     $search->displaySearchResults(
         $_GET['neighbour'],
         $_GET['property'],
         $_GET['bedroom'],
         $_GET['bathroom'],
         $_GET['area'],
         $_GET['price'],
         $_GET['agent']
     ); 
  
    }
    else if($_GET['agent'] != "") {
        $search->displaySearchBySku($_GET['agent']);
   
    }
    else if($_GET['badge'] !="") {
        $search->displaySearchByBadge( $_GET['neighbour'],
         $_GET['property'],
         $_GET['bedroom'],
         $_GET['bathroom'],
         $_GET['area'],
         $_GET['price'],
        $_GET['badge']);
    }

     ?>
<!-- result-item: END -->
          </div>
<!-- result container: END -->
<!-- search results bottom head: START -->
          <div class="col-xs-12 result-head">
<!-- sort: START -->
            <div class="col-xs-6 col-md-3">
              Sort by: 
              <span class="dropdown">
                <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><br class="mobile-show"> Default Order <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <ul class="dropdown-menu">
                  <li><a href="javascript: void(0)">Price (Low to High)</a></li>
                  <li><a href="javascript: void(0)">Price (High to Low)</a></li>
                  <li><a href="javascript: void(0)">Featured</a></li>
                  <li><a href="javascript: void(0)">Date Old to New</a></li>
                  <li><a href="javascript: void(0)">Date New to Old</a></li>
                </ul>
              </span>
            </div>
<!-- sort: START -->
<!-- order: START -->
            <div class="col-xs-6 col-md-4">
              Order by: 
              <span class="dropdown">
                <a href="javascript: void(0)" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="true"><br class="mobile-show"> Default Order <i class="fa fa-angle-down" aria-hidden="true"></i></a>
                <ul class="dropdown-menu">
                  <li><a href="javascript: void(0)">Price (Low to High)</a></li>
                  <li><a href="javascript: void(0)">Price (High to Low)</a></li>
                  <li><a href="javascript: void(0)">Featured</a></li>
                  <li><a href="javascript: void(0)">Date Old to New</a></li>
                  <li><a href="javascript: void(0)">Date New to Old</a></li>
                </ul>
              </span>
            </div>
<!-- order: END -->
<!-- pagination:START -->
            <div class="col-xs-6 col-md-3 text-right">
              <div class="pagination page_navigation pagination-sm"></div>
            </div>
            <!-- pagination:END -->
          </div>
<!-- search results bottom head: END -->

        </div> <!-- end search-results container -->
      </div>
<!-- search-results: END -->
    </div>
  </div>
<!-- find properties: END -->
<!-- recently added: START -->
     <?php
     get_template_part('estate_class/class', 'realestate');
     $recently = new RealEstate();
     $recently->displayRecently();
     ?>
<!-- recently added: END -->
<?php
get_footer();