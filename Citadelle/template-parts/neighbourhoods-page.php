<?php

/**
 * Template Name: Neighbourhoods Page
 *
 * @package Real Estate
 * @subpackage Goodwave
 * @since Goodwave 
 */

get_header(); ?>

  <!-- content: START -->

  <div class="content container">
               <?php

// Adding Breadcrumbs by Yoast


if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<div class="breadcrumbs"><p>','</p></div>
');
}
?>
    <div class="row featured"> <!-- featured: START -->
      <div class="col-xs-12 title">
        <h2><b><?php echo get_field('featured_title');?></b></h2>
        <p>
         <?php echo get_field('featured_description');?>
        </p>
        <div class="line"></div>
      </div>
  <?php 
     get_template_part('estate_class/class', 'neighbourhoods');
     $neighbourhoods = new Neighbourhoods();
        $category = get_field('featured_category');
     $neighbourhoods->displayCategoriesFeatured($category->name,3); 
        
     ?>
    
<!-- list: START -->
    <div class="row">
      <div class="col-xs-12 title" id="other-areas">
        <h2><b><?php echo get_field('other_areas_title');?></b></h2>
        <p>
          <?php echo get_field('other_areas_description');?>
        </p>
        <div class="line"></div>
      </div>
    </div>
<?php 

     $neighbourhoods = new Neighbourhoods();
     $category = get_field('other_areas_category');
     $neighbourhoods->displayOtherAreas($category->name); 
     ?>
    
      
 <!-- list: END -->
    
 <!-- more button -->
    <a id="loadMore" class="more btn">Load more areas ...</a>
  <script type="text/javascript">
    $(document).ready(function(){
        $(function(){
            
    $(".ajax").slice(0, 3).show(); // select the first 3
    $("#loadMore").click(function(e){ // click event for load more
        e.preventDefault();
        $(".ajax:hidden").slice(3, 4).show(); // select next 4 hidden divs and show them
        if($(".ajax:hidden").length == 0){ // check if any hidden divs still exist
            $("#loadMore").hide();
        }
    });
});
});
</script>
  </div>
<!-- content: END -->
</div>
<?php
get_footer();