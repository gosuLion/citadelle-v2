<?php

/**
 * Template Name: About Us
 *
 * @package Real Estate
 * @subpackage Goodwave
 * @since Goodwave 
 */

get_header(); ?>


<!-- content: START -->
<!-- simple text container: START -->
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <p class="description">
                     <?php

// Adding Breadcrumbs by Yoast


if ( function_exists('yoast_breadcrumb') ) {
yoast_breadcrumb('
<div class="breadcrumbs"><p>','</p></div>
');
}
?>
         <?php if( have_rows('description') ): ?>


	<?php while( have_rows('description') ): the_row(); 

// vars to get description
		$text = get_sub_field('add_description');
            
            echo $text."<br><br>";
		?>


	<?php 
					endwhile; 
					endif; 
					?>
        </p>
      </div>
    </div>
  </div>
  
<!-- simple text container: END -->

<!-- tabbed content: START -->
<!-- global markup -->
<!-- may be nested according to layout -->
  <div class="real-estate-tabbed">
    <div class="container">
      <div class="row">
<!-- tab head START -->
        <ul class="nav nav-pills">
          <li class="active" role="presentation">
            <a data-toggle="tab" href="#offices">OFFICES</a>
          </li>
          <li class="" role="presentation">
            <a data-toggle="tab" href="#management-team">MANAGEMENT TEAM</a>
          </li>
          <li class="" role="presentation">
            <a data-toggle="tab" href="#careers">CAREERS</a>
          </li>
          <li class="" role="presentation">
            <a data-toggle="tab" href="#press">PRESS</a>
          </li>
          <li class="" role="presentation">
            <a data-toggle="tab" href="#market-reports">MARKET REPORTS</a>
          </li>
        </ul>
<!-- tab head END -->

<!-- tabs content: START -->
        <div class="tab-content clearfix">

          <!-- 1st tab -->
          <div class="tab-pane active clearfix offices" id="offices">
            <p>
             <?php echo get_field('offices'); ?>
            </p>

             <?php 
     get_template_part('estate_class/class', 'offices');
     $neighbourhoods = new Offices();
     $category = get_field('featured_category');
     $neighbourhoods->displayOtherAreas('offices'); 
     ?>
          </div>
<!-- 2nd tab -->
          <div class="tab-pane management-team" id="management-team">

           <?php if( have_rows('management_team') ): ?>

<div class="row">  <!-- item: START -->
	<?php while( have_rows('management_team') ): the_row(); 

// vars
		$image = get_sub_field('picture');
		$name = get_sub_field('name');
		$position = get_sub_field('position');
		$facebook = get_sub_field('facebook');
		$linkedin = get_sub_field('linkedin');
		$twitter = get_sub_field('twitter');
		$description = get_sub_field('description');
		$email = get_sub_field('email');
		$phone = get_sub_field('phone');
		

		?>
           <div class="left">
                <img src="<?php echo $image['url']; ?>" class="img-responsive center-block" alt=""> <!-- left side image -->
              </div>
              <div class="right"> <!-- right side content (titles and description) --> 
                <div class="title">
                  <h4>
                    <b><?php echo $name;?></b><br class="mobile-show">
                    <a href="<?php echo $linkedin;?>">
                      <i class="fa fa-linkedin" aria-hidden="true"></i>
                    </a>
                    <a href="<?php echo $facebook;?>">
                      <i class="fa fa-facebook" aria-hidden="true"></i>
                    </a>
                    <a href="<?php echo $twitter;?>">
                      <i class="fa fa-twitter" aria-hidden="true"></i>
                    </a>
                  </h4>
                  <p><?php echo $position;?></p>
                  <div class="line"></div>
                </div>
                <p>
                  <?php echo $description;?>
                </p>
                <div class="contact"> <!-- bottom contact section -->
                  <span class="btn-fourth"><i class="fa fa-phone" aria-hidden="true"></i> <a  href="tel:<?php echo $phone;?>" ><?php echo $phone;?></a></span>
                  <span class="btn-fourth"><i class="fa fa-envelope-o" aria-hidden="true"></i>  <a href="mailto:<?php echo $email;?>" ><?php echo $email;?></a></span>
                </div>
              </div>
	<?php endwhile; ?>
   </div><!-- item: END -->
<?php endif; ?>
          </div>
<!-- 3rd tab -->
          <div class="tab-pane careers" id="careers">
<!-- simple text section -->
           <?php echo get_field('add_information') ; ?>
          </div>
<!-- 4th tab -->
          <div class="tab-pane press" id="press">
<!-- responsive table data --> 
<!-- will collapse in two columns on mobile devices -->
            <div class="head clearfix"> <!-- table head -->
              <div class="col-xs-6 col-sm-2">
                <div class="dropdown">
                  <a href="javascript: void(0)" class="active">Date<span class="caret"></span></a>
                </div>
              </div>

              <div class="col-xs-6 col-sm-2">
                <div class="dropdown">
                  <a href="javascript: void(0)" class="">Publication<span class="caret"></span></a>
                </div>
              </div>

              <div class="col-xs-6 col-sm-6">
                <div class="dropdown">
                  <a href="javascript: void(0)" class="">Article title<span class="caret"></span></a>
                </div>
              </div>

              <div class="col-xs-6 col-sm-2">
                <div class="dropdown">
                  <a href="javascript: void(0)" class="">Author<span class="caret"></span></a>
                </div>
              </div>

            </div>
<!-- table content -->
            
            <?php
//compose the posts
              
            $args = array( 'posts_per_page' => 10, 'order'=> 'ASC', 'orderby' => 'title' );
            $postsList = get_posts( $args );
              ?>
            <?php foreach($postsList as $post) :?>
            <div class="press-row clearfix"> <!-- press row -->
              <div class="col-xs-8 col-sm-2">
                <div class="date">
                 <?php echo $post->post_date; ?>
                </div>
              </div>

              <div class="col-xs-4 col-sm-2">
                <div class="publication">
                  <?php 
// get the category of a post
                    
                    $category = get_the_category($post->ID);

                   echo $category[0]->name;
                    ?>
                </div>
              </div>

              <div class="col-xs-8 col-sm-6">
                <div class="article">
                  <a href="<?php echo $post->guid; ?>"><?php echo strtoupper($post->post_title); ?></a>
                </div>
              </div>

              <div class="col-xs-4 col-sm-2">
                <div class="author">
                  <?php
                    $user = get_user_by('id', $post->post_author);
                    echo $user->user_nicename;
                    ?>
                </div>
              </div>
            </div>
            
    <?php endforeach; ?>
            <?php wp_reset_query(); ?>
          </div>

<!-- 5th tab -->
<!-- two columns layout -->
          <div class="tab-pane market-reports" id="market-reports">
            <div class="col-xs-12 col-sm-7"> <!-- text content section -->
              <h4><b>Report Summary</b></h4>
              <p>
               <?php echo get_field('add_information_market'); ?>
              </p>
              <a href="<?php echo get_field('add_pdf') ; ?>" class="btn-secondary">DOWNLOAD PDF</a> <!-- call to action -->
            </div>

            <div class="col-xs-12 col-sm-5">
              <img src="<?php echo get_field('add_image')['url'] ; ?>" class="img-responsive" alt=""> <!-- right side image -->
            </div>
          </div> 
          
        </div>
<!-- tabs content: START -->
      </div>
    </div>
  </div>
<!-- tabbed: END -->
<!-- content: END -->
<?php
get_footer();