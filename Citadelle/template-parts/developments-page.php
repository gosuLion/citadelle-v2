<?php

/**
 * Template Name: Developments Page
 *
 * @package Real Estate
 * @subpackage Goodwave
 * @since Goodwave 
 */

get_header(); ?>

     <?php 
     get_template_part('estate_class/class', 'developments');
     $newDevelopments = new Developments();
     $newDevelopments->displayNewDevelopments(); 
    
get_footer();