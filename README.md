

Citadelle – Real Estate

		Summary : 

I. Introduction / General
1. Wordpress Information
2. Requirements for Citadelle
3. Hosting
II. Instalation
1. Wordpress Instalation
2. FTP Instalation
3. Plugin Instalation
III. Demo Import
1. Import Citadelle Demos
IV. Options
1. How Options Work
2. Theme Logo
3. Socials
4. Article Options
5. Product Attributes
6. Google Maps Options
7. Sliders
8. Footer Copyright
	V. Widgets
1. How to add Widgets
2. Widget customization
VI. Extras
1. Create Agents and Agencies









I . Introduction / General


1. WordPress Information

	To install this theme you must have a working version of WordPress already installed. If you need help installing WordPress, follow the instructions in WordPress Codex . Below are all the useful links for WordPress information.

	> WordPress Codex – General info about WordPress and how to install on your server.
	> First Steps With Wordpress – General info about multiple topics about WordPress.
	> FAQ New to WordPress – The most Popular FAQ's reading WordPress.


2. Requirements for Citadelle

	To use Citadelle, please make sure you are running WordPress 4.6 or higher, PHP 5.6 or higher , and MySQL 5.6 or higher.

	> Check to ensure that your web host has the minimum requirements to run WordPress.
	> Always make sure they are running the latest version of WordPress.
	> You can download the latest release of WordPress from official WordPress website.
	> Always create secure passwords for FTP and Database.


3. Hosting

	Our Theme require that you have WordPress installed and runnning ona a web hosting account.







II. Instalation

		
1. WordPress Instalation

	You can install the theme in two ways : throught WordPress , or via FTP . The Citadelle.zip file is the installable WordPress Theme and that you need to use to get the theme installed. There are two ways to install Citadelle , via WordPress or via FTP. 
	First you need to download the Citadelle files from you account. Navigate to your downloads tab and find Citadelle. Click the download button to download the Installable WordPress Theme.
		a) Install Citadelle via WordPress
	
	Step 1 – Navigate to WordPress Dashboard > Appearance > Themes.
	
	Step 2 – Click the ' Add New' button on top of the page, then the  'Upload Theme ' button.

	Step 3 – Click 'Choose File' and find the theme files you've just downloaded. If you've downloaded the installable WordPress file, then you do NOT need to unzip the file. If you've downloaded the FULL THEME PACKAGE, you HAVE to unzip the Citadelle.zip file, therein you will see a secondary Citadelle.zip file, this is the one you select to install.

	Step 4 – Once the file has uploaded, to activate Citadelle , go to WordPress Dashboard > Appearance > Themes and click the Activate button.

	Step 5 – Once you activate Citadelle, you can return to WordPress Dashboard.

Troubleshooting 

1). 'ARE YOU SURE YOU WANT TO DO THIS?' ERROR

	If you get the "ARE YOU SURE YOU WANT TO DO THIS" message when installing Citadelle.zip file via WordPress, it means you have an upload file size limit that is set by your host. If this happens, please install the theme via FTP or contact your hosting company and ask them to increase the limit.


		2. FTP Installation

How to install Citadelle via FTP

	Step 1 – Log into your server installation via FTP. You can use software such as Filezila for this.
	
	Step 2 – If you have downloaded the Full Package, please unzip the master Citadelle.zip file and then unzip the secondary Citadelle.zip file which will give you a Citadelle folder.

	Step 3 – Upload the extracted Citadelle folder to the wp-content > themes folder on the server. Make sure the folder name is -> "Citadelle".

	Step 4 – Log in to your WordPress Dashboard and navigate to Appearance > Themes and clicking the 'Activate' button, for the Citadelle theme.

	Step 5 – Once activated, you will be redirected to the Welcome Screen. You'll see a prompt to install Citadell's required plugins.


3. Plugin Installation


		How to install Plugins

	Step 1 – Navigate to Citadelle > Plugins tab. Here you'll find Citadelle's required , included and recommended plugins.

	Step 2 – To install a plugin, simply click the "Install" button. Wait for it to finish installing and activating, then click the "Return to Required Plugins Installer" link to return to the Plugins page.
	Step 3 – Repeat steps 1 and 2 until you've installed and activated all the plugins.

		How to Update Plugins


	Step 1 – Navigate to the Citadelle > Plugins tab. Here you will see all the installed plugins.
	
	Step 2 – You'll see an update notification on plugins that have an update available. 

	Step 3 – Click the "Update" button to apply the update.

	Step 4. Repeat steps 1-3 until you've updated all your desired plugins.

	

III. Demo Import



	To import Citadelle Demos, you have to follow a few simple steps .

	Step 1 – Go to Dashboard > Appearance > Import Demo Data and click the "Import Demo Data" button. This might take a few minutes, so please, do not close the page.

	Step 2 – After the Import is completed , go to Dashboard > Tools > Import and Install the WordPress Importer plugin from the bottom of the plugin list. When the plugin is installed and activated, pres the "Run Importer" button.

	Step 3 – Click the "Choose File" button and select the "Products.xml" file from your master Citadelle folder that you downloaded. The exact path is : Citadelle / Import Files / Products.xml

	Step 4 – When the desired file is chosen, click the "Upload file and import" button in order to start the import.
	Step 5 – Click the "Submit" button in order to finish the final step of the import. Wait untill you see the "Import finished" message.



IV. Options


1. How Options Work & General Settings
	
	To access the Theme Options , go to Dashboard > Theme Options and a list of options will be available for your use.

	a) Theme Logo – In the "Theme Logo" section, you will have 3 fields that comes predefined with our default values.

	Desktop Logo – You can change the image by pressing the "Remove" button and then pressing the "Upload" button to choose the desired image you want to display as your desktop logo. Note that it can be a .png or .jpg image with values of  : width = '110px' ; height='85px';

	Mobile Logo - You can change the image by pressing the "Remove" button and then pressing the "Upload" button to choose the desired image you want to display as your mobile logo. Note that it can be a .png or .jpg image with values of  : width = '300px' ; height='120px';

	Footer Logo - ou can change the image by pressing the "Remove" button and then pressing the "Upload" button to choose the desired image you want to display as your footer logo. Note that it can be a .png or .jpg image with values of  : width = '170px' ; height='215px';

	b) Contact – In the "Contact" section you have 2 fields , Phone and Email.

	Phone – You can set the desired phone number you want to display in the header area.
	
	Email -  You can set the desired email address you want to display in the header area.

c) Socials – In the "Socials" section you have 3 fields , Facebook, Linkedin and Twitter.

		Facebook – To link your Facebook page / account, please enter the full link in the field as follows : https://facebook.com/YourAccountId.
		
		Linkedin – To link your Linkedin account, please enter the full link in the field as follows : https://www.linkedin.com/YourAcountId.
	
		Twitter – To link your Twitter account, please enter the full link in the field as follows : https://twitter.com/YourAccountId.

d) 404 Page – In the "404 Page" section you have 1 field that is predefined . You can change the image value by clicking the "Remove" button and then the "Upload" button in order to set your desired image.

	        e) Article Options - In the "Article Options" section you have 1 field that is predefined . You can change the image value by clicking the "Remove" button and then the "Upload" button in order to set your desired image.
	
		
2. Product Options 

	In the "Product Options" section , you can set the number of products you want to display on the main page and you can make the price conversion.
	
	a) Number of Products – You can enter the number of products you want to display in the main page. Notice that this value will also increase "Recently added" tab from the Product's page.

	b) USD Value – All the prices are based on USD, and so, the default value for USD is 1.
c). GBP Value – To set the value for GBP, you have to calculate the ratio of GBP and USD. Ex: if 1 GBP = 2 USD, you will enter 2 in the GBP Value Field.
d) EUR Value – To set the value for EUR, you have to calculate the ratio of EUR and USD. Ex: if 1 EUR = 1,5 USD, you will enter 1,5 in the EUR Value Field.
	
3. Product Attributes

	The "Product Attributes" section contains default values that are not recommended to be changed if you made the "Import Demo Data".The Search module from the Homepage needs these values in order to function properly. 
	
	If you didn't made the "Import Demo Data" , please go to Dashboard, Product Attributes and create the following product attributes :
		Neighbourhood Area ,
		Bedrooms Area ,
		Bathroom Area ,
		Rent Area ,
		Sale Area ,
		Sold Area ,
		Property Type Area ,
		Surface Area .
	Once created, enter their slugs in the coresponding fields as follows : pa_ProductAttributeSlug.

4. Maps Options

	In the "Maps Options" section you can set the values for the map on the main page.
	
	Latitude – Enter your latitude coordinates values for your map.
	Longitude - Enter your longitude coordinates values for your map.
	City – Enter the name of the city you want to display.

	Google API – For the map to be available, please enter your Google API key in the field. To get a Google API Key, please follow the instructions stated here.

5. Footer Opions
	
	Footer Copyright – Enter your copyright desired text.


V. Widgets


		The Citadelle theme has 3 Footer widget area and 1 Header Widget Area.
		1. To activate and use Widgets, go to Appearance > Widgets, and there you will see different Widget Areas as follows : 

	"Second Footer Widget Area"
	"Third Footer Widget Area"
	"Fourth Footer Widget Area"
	"Language Widget Area"

	In can add any widgets that you desire in these widgets area.

	To add a Widget, simply drag and drop the widget you want from the left side into the designated widget area.

		2. Widget Customization

	To customize any Widget, drag and drop the widget into a Widget Area and follow each widget's instructions.

3. Citadelle Specific Widgets

	a) Citadelle Contact – This widget is custom made for Citadelle Theme. You can set your custom Title, Company Name, Address, Phone Number, Email and has alse support for various social media contact links such as : Behance, Youtube, Linkedin, Facebook, Twitter.
	For social media, please enter the full link to your desired account.

	b) Citadelle Links – This widget is custom made for Citadelle Theme.
With this widget you are able to set and display custom Links in your Footer area. You can set up to 10 Links in total.

	c) Recent Posts Citadelle – This widget is customized for Citadelle Theme. You can choose the number of recent posts that you want to display in the Footer area.